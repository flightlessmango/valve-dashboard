unless ENV['DISABLE_SIMPLECOV'] == 1
  require 'simplecov'
  SimpleCov.start 'rails' do
    # Stuff that we don't care about
    add_filter "run.rb"
    add_filter "blob.rb"
    add_filter "direct_uploads_controller.rb"
    add_filter "devise/sessions_controller.rb"
    add_filter "app/controllers/frame_outputs_controller"
    add_filter "app/models/application_record.rb"
    add_filter "app/mailers/application_mailer.rb"
    add_filter "app/jobs/application_job.rb"
  end
end
ENV['RAILS_ENV'] ||= 'test'
require 'rails/test_help'
require_relative '../config/environment'

class ActiveSupport::TestCase
  # Don't run things in parallel because it messes up simplecov
  # Run tests in parallel with specified workers
  if ENV['DISABLE_SIMPLECOV'] == 1
    parallelize(workers: :number_of_processors)
  end

  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  # Add more helper methods to be used by all tests here...
end
