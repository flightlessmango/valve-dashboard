class ApplicationControllerTest < ActionDispatch::IntegrationTest
    include Devise::Test::IntegrationHelpers
    setup do
        sign_in User.first
    end

    test "access" do
        # @user = User.first
        # sign_out @user
        # # Not signed in
        # get root_path
        # assert_response :success

        # # No login access
        # get games_path
        # assert_response :success
        # get game_path(Game.first)
        # assert_response :success

        # # Signed in with and without can_download
        # sign_in @user
        # get new_game_path
        # # @user.update(can_create_game)
        # assert response.body == "Not permitted"
        # assert_response 401

        # # @user.update(can_download: true)
        # get games_path
        # assert_response :success

        # # Signed in with and without can_upload
        # @game = Game.first
        # get edit_game_path(@game)
        # assert response.body == "Not permitted"
        # assert_response 401

        # @user.update(can_upload: true)
        # get edit_game_path(@game)
        # assert_response :success

        # # Signed in with and without admin
        # get users_path
        # assert response.body == "Not permitted"
        # assert_response 401

        # @user.update(admin: true)
        # get users_path
        # assert_response :success
    end

    test "execColumn" do
        a = ApplicationController::ExecColumn.new("NAVI10", 2, 3)
        b = ApplicationController::ExecColumn.new("NAVI20", 1, 2)
        assert (a <=> b) == -1
        assert (b <=> a) == 1

        a = ApplicationController::ExecColumn.new("NAVI10", 1, 3)
        b = ApplicationController::ExecColumn.new("NAVI10", 2, 2)
        assert (a <=> b) == -1
        assert (b <=> a) == 1

        a = ApplicationController::ExecColumn.new("NAVI10", 1, 2)
        b = ApplicationController::ExecColumn.new("NAVI10", 1, 3)
        assert (a <=> b) == -1
        assert (b <=> a) == 1

        a = ApplicationController::ExecColumn.new("NAVI10", 1, 2)
        assert (a <=> a) == 0
    end
end