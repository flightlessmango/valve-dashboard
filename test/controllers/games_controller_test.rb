class GamesControllerTest < ActionDispatch::IntegrationTest
    include Devise::Test::IntegrationHelpers

    setup do
        sign_in User.first
    end

    test "create game" do
        User.first.update(can_create_game: false)
        sign_in User.first
        # without permissions
        get new_game_path
        assert_response 401
        get games_path
        assert_response :success

        # with permissions
        User.first.update(can_create_game: true)
        post games_path, params: {game: {name: "test game 1"}}
        assert_not_nil Game.find_by(name: "test game 1")
        post games_path, params: {game: {name: "test game 1"}}
        assert response.body == '{"name":["has already been taken"]}'
        get new_game_path
        assert_response :success
        get games_path
        assert_response :success

        # without login
        sign_out User.first
        get new_game_path
        assert_response 401
        get games_path
        assert_response :success
    end

    test "show game" do
        # with login
        get game_path(Game.first)
        assert_response :success

        # without login
        get game_path(Game.first)
        assert_response :success
    end

    test "update game" do
        # without permissions
        User.first.update(can_edit_game: false)
        sign_in User.first
        patch game_path(Game.first), params: {game: {name: "A different name"}}
        assert_response :unauthorized
        get edit_game_path(Game.first)
        assert_response :unauthorized

        # with permissions
        User.first.update(can_edit_game: true)
        patch game_path(Game.first), params: {game: {name: "A different name"}}
        assert_redirected_to game_path(Game.first)
        get edit_game_path(Game.first)
        assert_response :success

        # with login
        sign_out User.first
        patch game_path(Game.first), params: {game: {name: "A different name"}}
        assert_response :unauthorized
        get edit_game_path(Game.first)
        assert_response :unauthorized
    end

    test "destroy game" do
        # without permissions
        assert_no_difference 'Game.count' do
            delete game_path(Game.first)
            assert_response 401
        end

        # with permissions
        User.first.update(can_destroy_game: true)
        assert_difference 'Game.count', -1 do
            delete game_path(Game.first)
        end

        # without login
        sign_out User.first
        assert_no_difference 'Game.count' do
            delete game_path(Game.first)
            assert_response :unauthorized
        end
    end
end