class TraceExecsControllerTest < ActionDispatch::IntegrationTest
    include Devise::Test::IntegrationHelpers
    setup do
        sign_in User.first
    end

    test "show" do
        # without permissions
        get trace_exec_path(TraceExec.first)
        assert_response 401

        # with permissions
        User.first.update(admin: true)
        get trace_exec_path(TraceExec.first)
        assert_response :success

        # without login
        sign_out User.first
        get trace_exec_path(TraceExec.first)
        assert_response :unauthorized
    end
end