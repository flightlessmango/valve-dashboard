class RunsControllerTest < ActionDispatch::IntegrationTest
    include Devise::Test::IntegrationHelpers
    setup do
        sign_in User.first
        assert @run = Run.create!(game_id: 1, bench_id: 1, name: "Test")
    end

    test "Get index/show" do
        # without permissions
        get runs_path
        assert_response 401
        post runs_path, params: {run: {game_id: 1, bench_id: 1, name: "Test"}}
        assert_response 401
        get run_path(@run), as: :json
        assert_response 401

        # with permissions
        User.first.update(admin: true)
        get runs_path
        assert_response :success
        get run_path(@run), as: :json
        assert_response :success

        # without login
        sign_out User.first
        get runs_path
        assert_redirected_to new_user_session_path
        post runs_path, params: {run: {game_id: 1, bench_id: 1, name: "Test"}}
        assert_redirected_to new_user_session_path
        get run_path(@run), as: :json
        assert_response 401
    end

    test "create/update/destroy" do
        # without permissions
        post runs_path, params: {file_type: "HML", run: {game_id: 1, bench_id: 1, upload: fixture_file_upload("afterburner.hml")}}
        assert_response 401
        patch run_path(Run.last), params: {attachment: true, file_type: "HML", run: {name: "test123"}}, as: :json
        assert_response 401
        post runs_path, params: {file_type: "MANGO", run: {game_id: 1, bench_id: 1, upload: fixture_file_upload("mangohud.csv")}}
        assert_response 401
        patch run_path(Run.last), params: {attachment: true, file_type: "MANGO", run: {name: "test123"}}, as: :json
        assert_response 401
        post runs_path, params: {run: {game_id: nil, bench_id: 1, name: "test"}}, as: :json
        assert_response 401
        delete run_path(@run)
        assert_response 401

        # with permissions
        User.first.update(admin: true)
        post runs_path, params: {file_type: "HML", run: {game_id: 1, bench_id: 1, upload: fixture_file_upload("afterburner.hml")}}
        assert_response :redirect
        patch run_path(Run.last), params: {attachment: true, file_type: "HML", run: {name: "test123"}}, as: :json
        assert_equal "test123", Run.last.name
        post runs_path, params: {file_type: "MANGO", run: {game_id: 1, bench_id: 1, upload: fixture_file_upload("mangohud.csv")}}
        assert_response :redirect
        patch run_path(Run.last), params: {attachment: true, file_type: "MANGO", run: {name: "test123"}}, as: :json
        assert_equal "test123", Run.last.name
        post runs_path, params: {run: {game_id: nil, bench_id: 1, name: "test"}}, as: :json
        assert_response 422
        @run = Run.last
        delete run_path(@run)
        assert_empty Run.where(id: @run.id)

        # with login
        sign_out User.first
        post runs_path, params: {file_type: "HML", run: {game_id: 1, bench_id: 1, upload: fixture_file_upload("afterburner.hml")}}
        assert_redirected_to new_user_session_path
        patch run_path(Run.last), params: {attachment: true, file_type: "HML", run: {name: "test123"}}, as: :json
        assert_response 401
        post runs_path, params: {file_type: "MANGO", run: {game_id: 1, bench_id: 1, upload: fixture_file_upload("mangohud.csv")}}
        assert_redirected_to new_user_session_path
        patch run_path(Run.last), params: {attachment: true, file_type: "MANGO", run: {name: "test123"}}, as: :json
        assert_response 401
        post runs_path, params: {run: {game_id: nil, bench_id: 1, name: "test"}}, as: :json
        assert_response 401
        delete run_path(Run.last)
        assert_redirected_to new_user_session_path
    end

end