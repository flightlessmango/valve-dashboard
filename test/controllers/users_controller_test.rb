class UsersControllerTest < ActionDispatch::IntegrationTest
    include Devise::Test::IntegrationHelpers
    setup do
        sign_in User.first
    end

    test "user methods" do
        @user = User.first
        sign_out @user
        @user.update(admin: false)
        sign_in @user
        @user2 = User.last
        get edit_user_path(@user2)
        assert response.body == "Not permitted"
        assert_response 401

        get user_path(@user2)
        assert response.body == "Not permitted"
        assert_response 401

        get edit_permissions_user_path(@user2)
        assert response.body == "Not permitted"
        assert_response 401
        
        # non-admin try to change self to admin
        patch user_path(@user), params: {user: {admin: true}}
        assert_not @user.admin
        assert_redirected_to root_path
        
        @user.update(admin: true)
        patch user_path(@user), params: {user: {online: false}}
        # update error
        patch user_path(@user), params: {user: {username: "steve"}}
        assert_equal response.body, '{"username":["has already been taken"]}'

        get user_path(@user2)
        assert_response :success
        get edit_user_path(@user2)
        assert_response :success
        get edit_permissions_user_path(@user2)
        assert_response :success

        # non admin try to access other user
        @user2.update(admin: false)
        sign_out @user
        sign_in @user2

        get user_path(@user)
        assert response.body == "Not permitted"
        assert_response 401

        get edit_user_path(@user)
        assert response.body == "Not permitted"
        assert_response 401

        get edit_permissions_user_path(@user)
        assert response.body == "Not permitted"
        assert_response 401

        # non admin trying to access index
        get users_path
        assert_response 401

        # access index
        get users_path
        assert_response 401
        @user2.update(admin: true)
        get users_path
        assert_response :success

        # not logged in trying to access user things
        sign_out @user
        sign_out @user2
        get user_path(@user)
        assert_redirected_to new_user_session_path

        get edit_user_path(@user)
        assert_redirected_to new_user_session_path

        get edit_permissions_user_path(@user)
        assert_redirected_to new_user_session_path

        get users_path
        assert_redirected_to new_user_session_path
    end
end