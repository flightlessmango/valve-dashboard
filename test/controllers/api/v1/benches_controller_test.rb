module Api
    module V1
        class BenchesControllerTest < ActionDispatch::IntegrationTest
            include Devise::Test::IntegrationHelpers

            setup do
                sign_in User.first
            end

            test "api benches methods" do
                # without permissions
                get api_v1_benches_path, as: :json
                assert_response 401
                get new_api_v1_bench_path
                assert_response 401
                assert_no_difference "Bench.count" do
                    post api_v1_benches_path, params: {bench: {name: "test"}}
                end
                @bench = Bench.first
                get edit_api_v1_bench_path(@bench)
                assert_response 401
                get edit_api_v1_bench_path(@bench)
                assert_response 401
                get api_v1_bench_path(@bench)
                assert_response 401
                patch api_v1_bench_path(@bench), params: {bench: {name: "test1"}}, as: :html
                @bench.reload
                assert_not_equal @bench.name, "test1"
                assert_no_difference "Bench.count" do
                    delete api_v1_bench_path(@bench)
                end

                # with permissions
                User.first.update(admin: true)
                get api_v1_benches_path, as: :json
                assert_response :success
                get new_api_v1_bench_path
                assert_response :success
                assert_difference "Bench.count" do
                    post api_v1_benches_path, params: {bench: {name: "test"}}
                end
                @bench = Bench.first
                get edit_api_v1_bench_path(@bench)
                assert_response :success
                get edit_api_v1_bench_path(@bench)
                assert_response :success
                get api_v1_bench_path(@bench)
                assert_response :success
                patch api_v1_bench_path(@bench), params: {bench: {name: "test1"}}, as: :html
                @bench.reload
                assert_equal @bench.name, "test1"
                assert_difference "Bench.count", -1 do
                    delete api_v1_bench_path(@bench)
                end
                
                # without login
                sign_out User.first
                get api_v1_benches_path, as: :json
                assert_response 401
                get new_api_v1_bench_path
                assert_response 401
                assert_no_difference "Bench.count" do
                    post api_v1_benches_path, params: {bench: {name: "test"}}
                end
                @bench = Bench.first
                get edit_api_v1_bench_path(@bench)
                assert_response 401
                get edit_api_v1_bench_path(@bench)
                assert_response 401
                get api_v1_bench_path(@bench)
                assert_response 401
                patch api_v1_bench_path(@bench), params: {bench: {name: "test1"}}, as: :html
                @bench.reload
                assert_not_equal @bench.name, "test1"
                assert_no_difference "Bench.count" do
                    delete api_v1_bench_path(@bench)
                end
            end

        end
    end
end