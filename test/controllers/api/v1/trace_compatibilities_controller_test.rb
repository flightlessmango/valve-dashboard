module Api
    module V1
        class TraceCompatibilitiesControllerTest < ActionDispatch::IntegrationTest
            include Devise::Test::IntegrationHelpers

            setup do
                sign_in User.first
            end

            test "index" do
                # without permissions
                get api_v1_trace_compatibilities_path, params: {pciid: "0x1002:0x67df"}
                assert_response 401

                # with permissions
                User.first.update(admin: true)
                get api_v1_trace_compatibilities_path, params: {pciid: "0x1002:0x67df"}
                assert_response :success

                # without login
                sign_out User.first
                get api_v1_trace_compatibilities_path, params: {pciid: "0x1002:0x67df"}
                assert_response 401
            end

            test "create" do
                # without permissions
                assert_no_difference 'TraceCompatibility.count' do
                    post api_v1_trace_compatibilities_path, params: {trace_compatibility: {commit_id: 1, trace_id: 1, gpu_id: 1, is_working: true, agreement_count: 1}}
                    assert_response 401
                end

                # with permissions
                User.first.update(admin: true)
                assert post api_v1_trace_compatibilities_path, params: {trace_compatibility: {commit_id: 1, trace_id: 1, gpu_id: 1, is_working: true, agreement_count: 1}}
                assert post api_v1_trace_compatibilities_path, params: {trace_compatibility: {commit_id: 1, trace_id: 1, gpu_id: 1, is_working: true, agreement_count: 1}}
                assert post api_v1_trace_compatibilities_path, params: {trace_compatibility: {commit_id: nil, trace_id: 1, gpu_id: 1, is_working: true, agreement_count: 1}}

                # without permissions
                sign_out User.first
                assert_no_difference 'TraceCompatibility.count' do
                    post api_v1_trace_compatibilities_path, params: {trace_compatibility: {commit_id: 1, trace_id: 1, gpu_id: 1, is_working: true, agreement_count: 1}}
                    assert_response 401
                end
            end
        end
    end
end