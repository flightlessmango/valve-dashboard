module Api
    module V1
        class StatsControllerTest < ActionDispatch::IntegrationTest
            include Devise::Test::IntegrationHelpers

            setup do
                sign_in User.first
            end

            test "frame_outputs" do
                # without permissions
                get frame_outputs_api_v1_stats_path, as: :json
                assert_response 401

                # with permissions
                User.first.update(admin: true)
                get frame_outputs_api_v1_stats_path, as: :json
                assert_response :success

                # without login
                sign_out User.first
                get frame_outputs_api_v1_stats_path, as: :json
                assert_response 401
            end

            test "trace_frames" do
                # without permissions
                get trace_frames_api_v1_stats_path(TraceFrame.first), as: :json
                assert_response 401

                # with permissions
                User.first.update(admin: true)
                get trace_frames_api_v1_stats_path(TraceFrame.first), as: :json
                assert_response :success

                # without login
                sign_out User.first
                get trace_frames_api_v1_stats_path(TraceFrame.first), as: :json
                assert_response 401
            end

            test "blobs" do
                # without permissions
                get blobs_api_v1_stats_path(ActiveStorage::Blob.first)
                assert_response :success

                # with permissions
                User.first.update(admin: true)
                get blobs_api_v1_stats_path(ActiveStorage::Blob.first)
                assert_response :success

                # without login
                sign_out User.first
                get blobs_api_v1_stats_path(ActiveStorage::Blob.first)
                assert_response :success
            end
        end
    end
end