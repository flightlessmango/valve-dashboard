module Api
    module V1
        class StaticControllerTest < ActionDispatch::IntegrationTest
            include Devise::Test::IntegrationHelpers

            setup do
                sign_in User.first
            end

            test "checksum" do
                # without permissions
                get api_v1_checksum_path, params: {checksum: "test"}
                assert_response 401
                get api_v1_checksum_path, params: {checksum: "fdUivZUf74Y6pjAiemuvlg=="}
                assert_response 401
                get api_v1_image_checksum_path, params: {checksum: "test"}
                assert_response 401
                get api_v1_image_checksum_path, params: {checksum: "fdUivZUf74Y6pjAiemuvlG==1"}
                assert_response 401

                # with permissions
                User.first.update(admin: true)
                get api_v1_checksum_path, params: {checksum: "test"}
                assert_response :success
                get api_v1_checksum_path, params: {checksum: "fdUivZUf74Y6pjAiemuvlg=="}
                assert_response :success
                get api_v1_image_checksum_path, params: {checksum: "test"}
                assert_response :success
                get api_v1_image_checksum_path, params: {checksum: "fdUivZUf74Y6pjAiemuvlG==1"}
                assert_response :success

                # without login
                sign_out User.first
                get api_v1_checksum_path, params: {checksum: "test"}
                assert_response 401
                get api_v1_checksum_path, params: {checksum: "fdUivZUf74Y6pjAiemuvlg=="}
                assert_response 401
                get api_v1_image_checksum_path, params: {checksum: "test"}
                assert_response 401
                get api_v1_image_checksum_path, params: {checksum: "fdUivZUf74Y6pjAiemuvlG==1"}
                assert_response 401
            end
        end
    end
end