module Api
    module V1
        class ProjectsControllerTest < ActionDispatch::IntegrationTest
            include Devise::Test::IntegrationHelpers

            setup do
                sign_in User.first
            end

            test "api project methods" do
                # without permissions
                get api_v1_projects_path, as: :json
                assert_response 401

                post api_v1_projects_path, params: {project: {name: "AMDVLK"}}
                assert_response 401
                assert_nil Project.find_by(name: "AMDVLK")

                # with permissions
                User.first.update(admin: true)
                get api_v1_projects_path, as: :json
                assert_response :success

                post api_v1_projects_path, params: {project: {name: "AMDVLK"}}
                assert_not_nil Project.find_by(name: "AMDVLK")
                post api_v1_projects_path, params: {project: {name: "AMDVLK"}}
                assert response.body.include? '{"name":["has already been taken"]}'

                # without login
                sign_out User.first
                get api_v1_projects_path, as: :json
                assert_response 401
                post api_v1_projects_path, params: {project: {name: "AMDGPU"}}
                assert_response 401
                assert_nil Project.find_by(name: "AMDGPU")
            end

        end
    end
end