module Api
    module V1
        class TraceExecsControllerTest < ActionDispatch::IntegrationTest
            include Devise::Test::IntegrationHelpers
            setup do
                sign_in User.first
            end

            test "Get index" do
                # without permissions
                get api_v1_trace_execs_path, as: :json
                assert_response 401

                # with permissions
                User.first.update(admin: true)
                get api_v1_trace_execs_path, as: :json
                assert_response :success

                # without login
                sign_out User.first
                get api_v1_trace_execs_path, as: :json
                assert_response 401
            end

            test "Get show" do
                # with permissions
                User.first.update(admin: true)
                get api_v1_trace_exec_path(TraceExec.first), as: :json
                assert_response :success

                # without login
                sign_out User.first
                get api_v1_trace_exec_path(TraceExec.first), as: :json
                assert_response 401
            end

            test "Create trace_exec" do
                pciid = "0x1002:0x67df"
                frame_blobs = {"100": ActiveStorage::Blob.first.signed_id}
                params = {trace_exec: {job_id: 1, trace_id: 1}, driver: {name: "RADV"},
                          frame_blobs: frame_blobs, pciid: pciid}

                # without permissions
                assert_no_difference("TraceExec.count") do
                    post api_v1_trace_execs_path, params: params, as: :json
                    assert_response 401
                end

                # with permissions
                User.first.update(can_upload_frames: true)
                assert_difference("TraceExec.count") do
                    post api_v1_trace_execs_path, params: params, as: :json
                end
                assert_response :success
                resp = JSON.parse(response.body)
                assert_equal params[:trace_exec][:job_id], resp["job"]["id"]
                assert_equal params[:trace_exec][:trace_id], resp["trace"]["id"]
                assert_equal params[:pciid], resp["gpu"]["pciid"]
                assert_equal frame_blobs.keys.first.to_s, resp["trace_frames"].first.first

                # If no pciid
                assert_difference("TraceExec.count") do
                    post api_v1_trace_execs_path, params: params.except(:pciid), as: :json
                end

                # If trace exec already exists
                assert_no_difference("TraceExec.count") do
                    post api_v1_trace_execs_path, params: params, as: :json
                end

                # If error (no job)
                assert_no_difference("TraceExec.count") do
                    params[:trace_exec][:job_id] = nil
                    post api_v1_trace_execs_path, params: params, as: :json
                end

                # without login
                sign_out User.first
                assert_no_difference("TraceExec.count") do
                    post api_v1_trace_execs_path, params: params, as: :json
                    assert_response 401
                end
            end

            test "Update trace_exec" do
                # without permissions
                assert_no_difference("TraceExec.last.status") do
                    patch api_v1_trace_exec_path(TraceExec.last), params: {trace_exec: {status: 1}}, as: :json
                end

                # with permissions
                User.first.update(can_upload_frames: true)
                assert_changes("TraceExec.last.status") do
                    patch api_v1_trace_exec_path(TraceExec.last), params: {trace_exec: {status: 1}}, as: :json
                end

                # without login
                sign_out User.first
                assert_no_difference("TraceExec.last.status") do
                    patch api_v1_trace_exec_path(TraceExec.last), params: {trace_exec: {status: 1}}, as: :json
                end
            end

        end
    end
end