module Api
    module V1
        class JobsControllerTest < ActionDispatch::IntegrationTest
            include Devise::Test::IntegrationHelpers
            setup do
                sign_in User.first
            end

            test "jobs index" do
                # with login
                get api_v1_jobs_path, as: :json
                assert_response :success
                get api_v1_jobs_path, params: {name: "test"}, as: :json
                assert_response :success

                # without login
                sign_out User.first
                get api_v1_jobs_path, as: :json
                assert_response :success
                get api_v1_jobs_path, params: {name: "test"}, as: :json
                assert_response :success
            end

            test "Create job" do
                job_timeline = {branch: "ci_test", project: "dxvk-ci",
                                project_url: "https://gitlab.freedesktop.org/mupuf/dxvk-ci//-/tree/ci_test"}
                params = {job: {name: "dxvk-ci-test", is_released_code: true, commit_version: "1c8021e"}, job_timeline: job_timeline }

                # without permissions
                assert_no_difference("Job.count") do
                    post api_v1_jobs_path, params: params, as: :json
                    assert_response 401
                end

                # with permissions
                User.first.update(admin: true)
                assert_difference("Job.count") do
                    post api_v1_jobs_path, params: params, as: :json
                    assert_response :success
                end
                resp = JSON.parse(response.body)
                assert_equal params[:job][:name], resp["name"]
                assert_equal params[:job][:is_released_code], resp["is_released_code"]
                assert_equal params[:job][:commit_version], resp["commit_version"]

                # When job already exists
                params[:job][:name] = "DXVK"
                post api_v1_jobs_path, params: params, as: :json
                assert_response :success

                # When error
                params[:job][:name] = "DXVK-TEST"
                params.except!(:job_timeline)
                post api_v1_jobs_path, params: params, as: :json
                assert_response :success

                # without login
                sign_out User.first
                assert_no_difference("Job.count") do
                    post api_v1_jobs_path, params: params, as: :json
                    assert_response 401
                end

            end

        end
    end
end