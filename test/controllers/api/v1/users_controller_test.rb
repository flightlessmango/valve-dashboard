module Api
    module V1
        class UsersControllerTest < ActionDispatch::IntegrationTest
            include Devise::Test::IntegrationHelpers
            setup do
                sign_in User.first
            end

            test "users index" do
                # without permissions
                get api_v1_users_path, as: :json
                assert_response 401

                # with permissions
                User.first.update(admin: true)
                get api_v1_users_path, as: :json
                assert_response :success

                # without login
                sign_out User.first
                get api_v1_users_path, as: :json
                assert_response 401
            end

        end
    end
end