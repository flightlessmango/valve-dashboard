module Api
    module V1
        class GamesControllerTest < ActionDispatch::IntegrationTest
            include Devise::Test::IntegrationHelpers

            setup do
                sign_in User.first
            end

            test "api game index" do
                # with login
                get api_v1_games_path
                assert_response :success

                # without login
                sign_out User.first
                get api_v1_games_path
                assert_response :success
            end
            
            test "api create game" do
                # without permissions
                post api_v1_games_path, params: {game: {name: "test game 1"}}
                assert_response 401
                assert_nil Game.find_by(name: "test game 1")

                # with permission
                User.first.update(can_create_game: true)
                post api_v1_games_path, params: {game: {name: "test game 1"}}
                assert_not_nil Game.find_by(name: "test game 1")
                post api_v1_games_path, params: {game: {name: "test game 1"}}
                assert_equal response.body, '{"name":["has already been taken"]}'

                # without login
                sign_out User.first
                post api_v1_games_path, params: {game: {name: "test game 1"}}
                assert_response 401
            end

        end
    end
end