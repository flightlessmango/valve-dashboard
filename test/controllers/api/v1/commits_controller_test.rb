module Api
    module V1
        class CommitsControllerTest < ActionDispatch::IntegrationTest
            include Devise::Test::IntegrationHelpers

            setup do
                sign_in User.first
            end

            test "api commit methods" do
                # without permissions
                get api_v1_commits_path, as: :json
                assert_response 401
                assert_no_difference "Commit.count" do
                    post api_v1_commits_path, params: {commit: {version: "git hash", project_id: 1}}
                end
                assert_nil Commit.find_by(version: "git hash")

                # with permissions
                User.first.update(admin: true)
                get api_v1_commits_path, as: :json
                assert_response :success
                assert_difference "Commit.count" do
                    post api_v1_commits_path, params: {commit: {version: "git hash", project_id: 1}}
                end
                assert_not_nil Commit.find_by(version: "git hash")
                post api_v1_commits_path, params: {commit: {version: "git hash", project_id: 1}}
                assert response.body.include? '{"version":["has already been taken"]}'

                # without login
                sign_out User.first
                get api_v1_commits_path, as: :json
                assert_response 401
                assert_no_difference "Commit.count" do
                    post api_v1_commits_path, params: {commit: {version: "git hash no login", project_id: 1}}
                end
                assert_nil Commit.find_by(version: "git hash no login")
            end

        end
    end
end