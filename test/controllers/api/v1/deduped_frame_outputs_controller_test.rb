module Api
    module V1
        class DedupedFrameOutputControllerTest < ActionDispatch::IntegrationTest
            include Devise::Test::IntegrationHelpers
            setup do
                sign_in User.first
            end

            test "api v1 dedup show" do
                # with login
                get api_v1_deduped_frame_output_path(DedupedFrameOutput.find(1)), as: :json
                assert_response :success

                # without login
                sign_out User.first
                get api_v1_deduped_frame_output_path(DedupedFrameOutput.find(1)), as: :json
                assert_response :success
            end

        end
    end
end