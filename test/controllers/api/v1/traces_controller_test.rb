module Api
    module V1
        class TracesControllerTest < ActionDispatch::IntegrationTest
            include Devise::Test::IntegrationHelpers
            setup do
                sign_in User.first
            end

            test "index" do
                # make sure the traces have trace frames
                # without permissions
                Trace.all.each do |trace|
                    patch trace_path(trace), params: {trace: {user_id: 1}}
                end
                get api_v1_traces_path, params: {target_gpu_pciid: "0x1002:0x67df"}, as: :json
                assert_response :success

                # without login
                sign_out User.first
                get api_v1_traces_path, params: {target_gpu_pciid: "0x1002:0x67df"}, as: :json
                assert_response :success
            end

            test "create" do
                # without permissions
                assert_no_difference "Trace.count" do
                    post api_v1_traces_path, params: {trace: {game_id: 1, frames_to_capture: [15,16,17]}}
                    assert_response 401
                end

                # with permissions
                User.first.update(can_upload_trace: true)
                assert_difference "Trace.count" do
                    post api_v1_traces_path, params: {trace: {game_id: 1, frames_to_capture: [15,16,17]}}
                    assert_response :success
                end

                # without login
                sign_out User.first
                assert_no_difference "Trace.count" do
                    post api_v1_traces_path, params: {trace: {game_id: 1, frames_to_capture: [15,16,17]}}
                    assert_response 401
                end
            end

            test "show" do
                # with login
                get api_v1_trace_path(Trace.first), as: :json
                assert_response :success

                # without login
                sign_out User.first
                get api_v1_trace_path(Trace.first), as: :json
                assert_response :success
            end

            test "update" do
                # without permissions
                patch api_v1_trace_path(Trace.first), params: {trace: {user_id: 2}}
                assert_response 401

                # with permissions
                User.first.update(can_upload_trace: true)
                patch api_v1_trace_path(Trace.first), params: {trace: {user_id: 2}}
                assert_response :success
                patch api_v1_trace_path(Trace.first), params: {trace: {user_id: 2, game_id: nil}}
                assert_response :success

                # without login
                sign_out User.first
                patch api_v1_trace_path(Trace.first), params: {trace: {user_id: 2}}
                assert_response 401
            end

            test "enrollment" do
                # make sure the traces have trace frames
                Trace.all.each do |trace|
                    patch trace_path(trace), params: {trace: {user_id: 1}}
                end

                # without permissions
                get enrollment_api_v1_traces_path
                assert_response 401

                # with permissions
                User.first.update(admin: true)
                get enrollment_api_v1_traces_path
                assert_response :success

                # without login
                sign_out User.first
                get enrollment_api_v1_traces_path
                assert_response 401
            end

        end
    end
end