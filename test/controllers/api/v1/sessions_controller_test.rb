module Api
    module V1
        class SessionsControllerTest < ActionDispatch::IntegrationTest
            include Devise::Test::IntegrationHelpers

            test "session methods" do
                post "/api/v1/login", as: :json
                assert_response 401
                post "/api/v1/login", as: :json, params: {user: {username: "bob", password: "1234567"}}
                assert_response :success
            end
        end
    end
end