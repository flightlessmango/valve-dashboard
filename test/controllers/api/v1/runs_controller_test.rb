module Api
    module V1
        class RunsControllerTest < ActionDispatch::IntegrationTest
            include Devise::Test::IntegrationHelpers
            setup do
                sign_in User.first
            end

            test "api v1 runs index" do
                # without permissions
                get api_v1_runs_path
                assert_response 401

                # with permissions
                User.first.update(admin: true)
                get api_v1_runs_path
                assert_response :success

                # without login
                sign_out User.first
                get api_v1_runs_path
                assert_response 401
            end

            test "api v1 runs create" do
                # without permissions
                assert_no_difference "Run.count" do
                    post api_v1_runs_path, params: {run: {game_id: 1, upload: fixture_file_upload("mangohud.csv")}}
                    assert_response 401
                end

                #with permissions
                User.first.update(admin: true)
                assert_difference "Run.count" do
                    post api_v1_runs_path, params: {run: {game_id: 1, upload: fixture_file_upload("mangohud.csv")}}
                end
                assert_equal Run.last.upload.filename, "mangohud.csv"

                # without login
                sign_out User.first
                assert_no_difference "Run.count" do
                    post api_v1_runs_path, params: {run: {game_id: 1, upload: fixture_file_upload("mangohud.csv")}}
                    assert_response 401
                end
            end

        end
    end
end