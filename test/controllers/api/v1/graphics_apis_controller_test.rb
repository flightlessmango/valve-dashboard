module Api
    module V1
        class GraphicsApisControllerTest < ActionDispatch::IntegrationTest
            include Devise::Test::IntegrationHelpers

            setup do
                sign_in User.first
            end

            test "api gfxapi methods" do
                # without permissions
                get api_v1_graphics_apis_path, as: :json
                assert_response 401
                assert_no_difference "GraphicsApi.count" do
                    post api_v1_graphics_apis_path, params: {graphics_api: {name: "DX12"}}
                end
                post api_v1_graphics_apis_path, params: {graphics_api: {name: "DX12"}}
                assert_nil GraphicsApi.find_by(name: "DX12")

                # with permissions
                User.first.update(admin: true)
                get api_v1_graphics_apis_path, as: :json
                assert_response :success
                assert_difference "GraphicsApi.count" do
                    post api_v1_graphics_apis_path, params: {graphics_api: {name: "DX12"}}
                end
                post api_v1_graphics_apis_path, params: {graphics_api: {name: "DX12"}}
                assert_not_nil GraphicsApi.find_by(name: "DX12")

                # without login
                sign_out User.first
                get api_v1_graphics_apis_path, as: :json
                assert_response 401
                assert_no_difference "GraphicsApi.count" do
                    post api_v1_graphics_apis_path, params: {graphics_api: {name: "DX11"}}
                    assert_response 401
                end
                post api_v1_graphics_apis_path, params: {graphics_api: {name: "DX11"}}
                assert_nil GraphicsApi.find_by(name: "DX11")
            end

        end
    end
end