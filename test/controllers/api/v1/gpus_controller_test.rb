module Api
    module V1
        class GpusControllerTest < ActionDispatch::IntegrationTest
            include Devise::Test::IntegrationHelpers

            setup do
                sign_in User.first
            end

            test "api commit methods" do
                # without permissions
                get api_v1_gpus_path, as: :json
                assert_response 401
                assert_no_difference "Gpu.count" do
                    post api_v1_gpus_path, params: {gpu: {pciid: "0x1002:0x1235"}}
                end
                assert_nil Gpu.find_by(pciid: "0x1002:0x1235")

                # with permissions
                User.first.update(admin: true)
                get api_v1_gpus_path, as: :json
                assert_response :success
                assert_difference "Gpu.count" do
                    post api_v1_gpus_path, params: {gpu: {pciid: "0x1002:0x1235"}}
                end
                assert_not_nil Gpu.find_by(pciid: "0x1002:0x1235")
                post api_v1_gpus_path, params: {gpu: {pciid: "0x1002:0x1235"}}
                assert response.body.include? "0x1002:0x1235"

                # without login
                sign_out User.first
                get api_v1_gpus_path, as: :json
                assert_response 401
                assert_no_difference "Gpu.count" do
                    post api_v1_gpus_path, params: {gpu: {pciid: "0x1002:0x1"}}
                    assert_response 401
                end
                assert_nil Gpu.find_by(pciid: "0x1002:0x1")
            end

        end
    end
end