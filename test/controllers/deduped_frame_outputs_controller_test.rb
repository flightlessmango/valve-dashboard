class DedupedFrameOutputControllerTest < ActionDispatch::IntegrationTest
    include Devise::Test::IntegrationHelpers
    setup do
        sign_in User.first
    end

    test "Get deduped show" do
        # without q params and permissions
        get deduped_frame_output_path(DedupedFrameOutput.first)
        assert_response :success

        # with q param
        get deduped_frame_output_path(DedupedFrameOutput.first), params: {q: {id_eq: 1}}
        assert_response :success

        # without login
        sign_out User.first
        get deduped_frame_output_path(DedupedFrameOutput.first), params: {q: {id_eq: 1}}
        assert_response :success
    end

    test "set frame as acceptable" do
        frame = DedupedFrameOutput.find(2)
        assert_not frame.is_acceptable

        # without permissions
        User.first.update(can_review_frames: false)
        sign_in User.first
        post acceptable_frame_deduped_frame_output_path(frame)
        assert_response 401

        # with permissions
        User.first.update(can_review_frames: true)
        post acceptable_frame_deduped_frame_output_path(frame)
        frame.reload
        assert frame.is_acceptable
        assert_redirected_to next_frame_deduped_frame_output_path(frame)

        # without login
        sign_out User.first
        post acceptable_frame_deduped_frame_output_path(frame)
        assert_redirected_to new_user_session_path
    end

    test "next frame" do
        # without permissions
        User.first.update(can_review_frames: false)
        sign_in User.first
        DedupedFrameOutputsController.stub_any_instance(:find_next_frame, DedupedFrameOutput.first) do
            get next_frame_deduped_frame_output_path(DedupedFrameOutput.first)
            assert_response 401
        end

        # with permissions
        User.first.update(can_review_frames: true)
        # Test when we have a next frame but no job_id
        DedupedFrameOutputsController.stub_any_instance(:find_next_frame, DedupedFrameOutput.first) do
            get next_frame_deduped_frame_output_path(DedupedFrameOutput.first)
            assert_redirected_to deduped_frame_output_path(DedupedFrameOutput.first)
        end

        # Test when we have a next frame and a job_id
        DedupedFrameOutputsController.stub_any_instance(:find_next_frame, DedupedFrameOutput.first) do
            get next_frame_deduped_frame_output_path(DedupedFrameOutput.first), params: {job: 1}
            assert_redirected_to deduped_frame_output_path(DedupedFrameOutput.first, q: {job_id: 1})
        end

        # Test when we have a no next frames and no job_id
        DedupedFrameOutputsController.stub_any_instance(:find_next_frame, nil) do
            get next_frame_deduped_frame_output_path(DedupedFrameOutput.first)
            assert_redirected_to frames_path
        end

        # Test when we have no next frames but a job_id
        DedupedFrameOutputsController.stub_any_instance(:find_next_frame, nil) do
            get next_frame_deduped_frame_output_path(DedupedFrameOutput.first), params: {job: 1}
            assert_redirected_to job_path(Job.find(1))
        end

        # without login
        sign_out User.first
        DedupedFrameOutputsController.stub_any_instance(:find_next_frame, DedupedFrameOutput.first) do
            get next_frame_deduped_frame_output_path(DedupedFrameOutput.first)
            assert_redirected_to new_user_session_path
        end
    end

    test "find next frame" do
        # This test sucks. Rework it by creating dedups in the test and document what is what.

        # @resp = DedupedFrameOutputsController.new.find_next_frame(DedupedFrameOutput.find(1), DedupedFrameOutput.find(1).jobs.first.id)
        # assert_nil @resp
        # @resp = DedupedFrameOutputsController.new.find_next_frame(DedupedFrameOutput.find(2), DedupedFrameOutput.find(2).jobs.first.id)
        # assert_not_nil @resp
        # @resp = DedupedFrameOutputsController.new.find_next_frame(DedupedFrameOutput.first, nil)
        # assert_equal @resp, DedupedFrameOutput.find(3)
        # @resp = DedupedFrameOutputsController.new.find_next_frame(DedupedFrameOutput.find(2), nil)
        # assert_equal @resp, DedupedFrameOutput.find(3)
        # @resp = DedupedFrameOutputsController.new.find_next_frame(DedupedFrameOutput.find(3), nil)
        # assert_equal @resp, DedupedFrameOutput.find(3)
    end

    test "create issue" do
        frame = DedupedFrameOutput.first
        # without permissions
        User.first.update(can_review_frames: false)
        sign_in User.first
        get '/deduped_frame_outputs/' + frame.id.to_s + "/issue.js", params: {timeline: JobTimeLine.first.id}
        assert_response 401
        # with permissions
        User.first.update(can_review_frames: true)
        get '/deduped_frame_outputs/' + frame.id.to_s + "/issue.js", params: {timeline: JobTimeLine.first.id}
        assert_response :success

        resp = URI.decode_www_form_component(response.body)
        assert resp.include? "[frame](" + url_for(frame.blob) + ")"
        assert resp.include? "Game: " + frame.game.name
        assert resp.include? "Frame id: " + frame.trace_frame.frame_id.to_s

        # without login
        sign_out User.first
        get '/deduped_frame_outputs/' + frame.id.to_s + "/issue.js", params: {timeline: JobTimeLine.first.id}
        assert_response 401
    end

    test "deduped bugs" do
        @dedup = DedupedFrameOutput.first
        @bug = Bug.first
        # Associate bug with dedup
        @dedup.bugs << @bug
        assert_equal 1, @dedup.bugs.size
        # Disassociate all bugs
        # without permissions
        User.first.update(can_review_frames: false)
        sign_in User.first
        assert_no_difference '@dedup.bugs.size' do
            post '/deduped_frame_outputs/' + @dedup.id.to_s + "/dissociate.js", params: {bug: @bug.id}
            assert_response 401
        end

        # with permissions
        User.first.update(can_review_frames: true)
        post '/deduped_frame_outputs/' + @dedup.id.to_s + "/dissociate.js", params: {bug: @bug.id}
        assert_response :redirect
        assert_equal 0, @dedup.bugs.size

        # without login
        sign_out User.first
        assert_no_difference '@dedup.bugs.size' do
            post '/deduped_frame_outputs/' + @dedup.id.to_s + "/dissociate.js", params: {bug: @bug.id}
            assert_response 401
        end
    end

    test "dedup pre_merge_frame_bug" do
        # without permissions
        User.first.update(can_review_frames: false)
        sign_in User.first
        frame = DedupedFrameOutput.first
        get pre_merge_frame_bug_deduped_frame_output_path(frame)
        assert_response 401

        # with permissions
        User.first.update(can_review_frames: true)
        frame = DedupedFrameOutput.first
        get pre_merge_frame_bug_deduped_frame_output_path(frame)
        assert_response :redirect

        # without login
        sign_out User.first
        frame = DedupedFrameOutput.first
        get pre_merge_frame_bug_deduped_frame_output_path(frame)
        assert_redirected_to new_user_session_path
    end

end