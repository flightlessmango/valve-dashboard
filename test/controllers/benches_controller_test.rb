class BenchesControllerTest < ActionDispatch::IntegrationTest
    include Devise::Test::IntegrationHelpers

    setup do
        sign_in User.first
    end

    test "get index" do
        # without permissions
        get benches_path
        assert_response 401

        # with permissions
        User.first.update(admin: true)
        get benches_path
        assert_response :success

        # without login
        sign_out User.first
        get benches_path
        assert_response 302
    end

    test "get new/edit/create" do
        # without permissions
        get new_bench_path
        assert_response 401
        get edit_bench_path(Bench.first)
        assert_response 401
        post benches_path, params: {bench: {name: "Test"}}
        assert_response 401

        # with permissions
        user.first.update(admin: true)
        get new_bench_path
        assert_response :success
        get edit_bench_path(Bench.first)
        assert_response :success
        post benches_path, params: {bench: {name: "Test"}}
        assert_response :redirect

        # without login
        sign_out User.first
        get new_bench_path
        assert_redirected_to new_user_session_path
        get edit_bench_path(Bench.first)
        assert_redirected_to new_user_session_path
        post benches_path, params: {bench: {name: "Test"}}
        assert_redirected_to new_user_session_path
    end

    test "show" do
        # without permissions
        get bench_path(Bench.first)
        assert_response 401

        # with permissions
        User.first.update(admin: true)
        get bench_path(Bench.first)
        assert_response :success

        # without login
        sign_out User.first
        get bench_path(Bench.first)
        assert_redirected_to new_user_session_path
    end

    test "update" do
        # without permissions
        patch bench_path(Bench.first), params: {bench: {name: "test2"}}
        assert_response 401

        # with permissions
        User.first.update(admin: true)
        patch bench_path(Bench.first), params: {bench: {name: "test2"}}
        assert_redirected_to bench_path(Bench.first)
        assert_equal "test2", Bench.first.name

        # without login
        sign_out User.first
        patch bench_path(Bench.first), params: {bench: {name: "test2"}}
        assert_redirected_to new_user_session_path
    end

    test "destroy" do
        # without permissions
        assert_no_difference "Bench.count" do
            delete bench_path(Bench.first)
            assert_response 401
        end

        # with permissions
        User.first.update(admin: true)
        assert_difference "Bench.count", -1 do
            delete bench_path(Bench.first)
            assert_redirected_to root_path
        end

        # without login
        sign_out User.first
        Bench.create()
        assert_no_difference "Bench.count" do
            delete bench_path(Bench.first)
            assert_redirected_to new_user_session_path
        end
    end

    # test "permissions" do
    #     sign_out User.first
    #     get benches_path
    #     assert_response :success
    # end
end