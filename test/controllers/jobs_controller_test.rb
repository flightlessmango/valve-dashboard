class JobsControllerTest < ActionDispatch::IntegrationTest
    include Devise::Test::IntegrationHelpers
    setup do
        sign_in User.first
    end

    test "Get index" do
        # without permissions
        get jobs_path
        assert_response :success

        # without login
        sign_out User.first
        get jobs_path
        assert_response :success
    end

    test "Get show" do
        # create and extra trace_exec for this test
        TraceExec.create(status: 0, trace_id: 1, gpu_id: 1, driver_version_id: 1, job_id: 2)

        # with login
        get job_path(Job.find(2))
        assert_response :success

        # without login
        sign_out User.first
        get job_path(Job.find(2))
        assert_response :success

        # ExecTime
        execTime = JobsController::ExecTimeStats.new(10, 100, 50, 10)
        assert execTime.interval
        assert execTime.sample_difference_percentage(10)
        assert execTime.is_sample_within_variance(10)
        assert execTime.to_s
    end

    test "destroy job" do
        # without permissions
        assert_no_difference 'Job.count' do
            delete job_path(Job.first)
            assert_response 401
        end

        # with permissions
        User.first.update(admin: true)
        assert_difference 'Job.count', -1 do
            delete job_path(Job.first)
            assert_redirected_to jobs_path
        end

        # without login
        sign_out User.first
        assert_no_difference 'Job.count' do
            delete job_path(Job.first)
            assert_response :unauthorized
        end
    end
end