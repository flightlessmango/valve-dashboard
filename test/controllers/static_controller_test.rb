class StaticControllerTest < ActionDispatch::IntegrationTest
    include Devise::Test::IntegrationHelpers
    setup do
        sign_in User.first
    end

    test "static methods" do
        # with login
        get root_path
        assert_response :success

        get frames_path
        assert_response :success

        get '/static/get_url.js', params: {dedup: 1}
        assert_response :success

        get '/static/get_ref_frames.js', params: {q: {dedup_id: 1}}
        assert_response :success

        # without permissions
        get stats_path, as: :json
        assert_response 401

        # with permissions
        User.first.update(admin: true)
        get stats_path, as: :json
        assert_response :success
        
        # without login
        sign_out User.first
        get root_path
        assert_response :success

        get frames_path
        assert_response :success

        get '/static/get_url.js', params: {dedup: 1}
        assert_response :success

        get '/static/get_ref_frames.js', params: {q: {dedup_id: 1}}
        assert_response :success
    end

end