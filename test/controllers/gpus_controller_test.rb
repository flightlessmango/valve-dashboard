class GpusControllerTest < ActionDispatch::IntegrationTest
    include Devise::Test::IntegrationHelpers
    setup do
        sign_in User.first
    end

    test "get index" do
        # without permissions
        get gpus_path
        assert_response 401

        # with permissions
        User.first.update(admin: true)
        get gpus_path
        assert_response :success

        # without login
        sign_out User.first
        get gpus_path
        assert_redirected_to new_user_session_path
    end


    test "create/destroy/show" do
        # without permissions
        assert_no_difference "Gpu.count" do
            post gpus_path, params: {gpu: {name: "Test"}}
            assert_response 401
        end
        assert_no_difference "Gpu.count" do
            delete gpu_path(Gpu.last)
            assert_response 401
        end
        get gpu_path(Gpu.first)
        assert_response 401
        get gpus_path
        assert_response 401

        # with permissions
        User.first.update(admin: true)
        assert_difference "Gpu.count" do
            post gpus_path, params: {gpu: {name: "Test"}}
            assert_response :redirect
        end
        assert_difference "Gpu.count", -1 do
            delete gpu_path(Gpu.last)
            assert_response :redirect
        end
        get gpu_path(Gpu.first)
        assert_response :success
        get gpus_path
        assert_response :success

        # without login
        sign_out User.first
        assert_no_difference "Gpu.count" do
            post gpus_path, params: {gpu: {name: "Test"}}
            assert_redirected_to new_user_session_path
        end
        assert_no_difference "Gpu.count" do
            delete gpu_path(Gpu.last)
            assert_redirected_to new_user_session_path
        end
        get gpu_path(Gpu.first)
        assert_redirected_to new_user_session_path

        get gpus_path
        assert_redirected_to new_user_session_path
    end
end