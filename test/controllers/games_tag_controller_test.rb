class GamesTagsControllerTest < ActionDispatch::IntegrationTest
    include Devise::Test::IntegrationHelpers

    setup do
        sign_in User.first
    end
    
    test "destroy" do
        # without permissions
        delete games_tag_path(id: 1, game_id: 1, tag_id: 1)
        assert_response 401

        # with permissions
        User.first.update(can_edit_game: true)
        delete games_tag_path(id: 1, game_id: 1, tag_id: 1)
        assert_response :redirect

        # without login
        sign_out User.first
        delete games_tag_path(id: 1, game_id: 1, tag_id: 1)
        assert_redirected_to new_user_session_path
    end
end