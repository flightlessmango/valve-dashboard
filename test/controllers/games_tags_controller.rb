class GamesTagControllerTest < ActionDispatch::IntegrationTest
    include Devise::Test::IntegrationHelpers

    setup do
        sign_in User.first
    end

    test "destroy game" do
        @tag = Tag.create!(name: "test")
        @gamestag = GamesTag.create!(game_id: 1, tag_id: @tag.id)

        # without permissions
        assert_no_difference 'GamesTag.count' do
            delete games_tag_path(@gamestag), params: {game_id: 1, tag_id: @tag.id}
            assert_response 401
        end

        # with permissions
        User.first.update(can_edit_game: true)
        assert_difference 'GamesTag.count', -1 do
            delete games_tag_path(@gamestag), params: {game_id: 1, tag_id: @tag.id}
        end

        # without login
        sign_out User.first
        assert_no_difference 'GamesTag.count' do
            delete games_tag_path(@gamestag), params: {game_id: 1, tag_id: @tag.id}
            assert_redirected_to new_user_session_path
        end
    end
end