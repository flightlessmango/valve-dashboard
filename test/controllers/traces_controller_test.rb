class TracesControllerTest < ActionDispatch::IntegrationTest
    include Devise::Test::IntegrationHelpers

    setup do
        sign_in User.first
    end

    test "get index" do
        # without permissions
        get traces_path
        assert :success

        # without login
        get traces_path
        assert :success
    end

    test "edit/create" do
        # without permissions
        User.first.update(can_upload_trace: false)
        sign_in User.first
        get edit_trace_path(Trace.first)
        assert 401
        assert_no_difference "Trace.count" do
            post traces_path, params: {trace: {game_id: 1}}
            assert_response 401
        end

        # with permissions
        User.first.update(can_upload_trace: true)
        get edit_trace_path(Trace.first)
        assert :success
        assert_difference "Trace.count" do
            post traces_path, params: {trace: {game_id: 1}}
        end
        assert :success
        post traces_path, params: {trace: {user_id: 1}}
        assert_redirected_to traces_path

        # without login
        sign_out User.first
        get edit_trace_path(Trace.first)
        assert 401
        assert_no_difference "Trace.count" do
            post traces_path, params: {trace: {game_id: 1}}
            assert_redirected_to new_user_session_path
        end
    end

    test "get show" do
        # with login
        get trace_path(Trace.first)
        assert_response :success

        # without login
        sign_out User.first
        get trace_path(Trace.first)
        assert_response :success
    end

    test "update" do
        # without permissions
        assert_no_difference "Trace.first.game_id" do
            patch trace_path(Trace.first), params: {trace: {game_id: 2}}
            assert_response 401
        end
        get edit_trace_path(Trace.first)
        assert_response 401

        # with permissions
        User.first.update(can_upload_trace: true)
        patch trace_path(Trace.first), params: {trace: {game_id: 2}}
        assert_response :redirect
        assert_equal 2, Trace.first.game_id
        get edit_trace_path(Trace.first)
        assert_response :success

        # without login
        sign_out User.first
        assert_no_difference "Trace.first.game_id" do
            patch trace_path(Trace.first), params: {trace: {game_id: 2}}
            assert_redirected_to new_user_session_path
        end
        get edit_trace_path(Trace.first)
        assert_redirected_to new_user_session_path
    end

    test "destroy" do
        # without permissions
        assert_no_difference "Trace.count" do
            delete trace_path(Trace.first)
            assert_response 401
        end

        # with permissions
        User.first.update(admin: true)
        Trace.first.update(deleted: false)
        delete trace_path(Trace.first)
        assert Trace.first.deleted

        # without login
        sign_out User.first
        assert_no_difference "Trace.count" do
            delete trace_path(Trace.first)
            assert_redirected_to new_user_session_path
        end
    end

    test "execution_time" do
        # with login
        get execution_time_trace_path(Trace.first), as: :json
        assert_response :success
        get execution_time_trace_path(Trace.first), params: {timeline: 1}, as: :json
        assert_response :success

        # without login
        sign_out User.first
        get execution_time_trace_path(Trace.first), as: :json
        assert_response :success
        get execution_time_trace_path(Trace.first), params: {timeline: 1}, as: :json
        assert_response :success
    end
end