class BugsControllerTest < ActionDispatch::IntegrationTest
    include Devise::Test::IntegrationHelpers

    setup do
        sign_in User.first
    end

    test "get index" do
        # without permissions
        get bugs_path
        assert_response :success

        # without login
        get bugs_path
        assert_response :success
    end

    test "get show" do
        # without permissions
        get bug_path(Bug.find(1))
        assert_response :success

        # without login
        get bug_path(Bug.find(1))
        assert_response :success
    end

    test "pick job" do
        set_ = SortedSet.new
        set_ << BugsController::ReproductionRate.new(0, 0, nil, Job.first)
        assert set_.first.pick_job(Job.first, nil)
        assert set_.first.repro_rate
        set_.first.pick_job(Job.first, Job.find(2))
        set_.first.pick_job(Job.first, Job.find(2), false)
        FrameOutput.first.ignore_for_repro = true
    end

    test "create bug" do
        # without permissions
        User.first.update(can_review_frames: false)
        sign_in User.first
        assert_no_difference 'Bug.count' do
            post "/bugs.js", params: {bug: {issue_url: "http://someproject.com/issues"}, trace_frame_id: 1, deduped_frame_output_id: 1}
            assert_response 401
        end

        # with permissions
        User.first.update(can_review_frames: true)
        assert_difference 'Bug.count' do
            post "/bugs.js", params: {bug: {issue_url: "http://someproject.com/issues"}, trace_frame_id: 1, deduped_frame_output_id: 1}
            assert_response :success
        end

        # without login
        sign_out User.first
        assert_no_difference 'Bug.count' do
            post bugs_path, params: {bug: {issue_url: "http://someproject.com/issues"}, trace_frame_id: 1, deduped_frame_output_id: 1}
            assert_redirected_to new_user_session_path
        end
    end

    test "bug associate" do
        @dedup = DedupedFrameOutput.find(1)
        @dedup.update(is_acceptable: true)
        assert @dedup.is_acceptable
        # without permissions
        User.first.update(can_review_frames: false)
        sign_in User.first
        assert_no_difference '@dedup.bugs.count' do
            post associate_bug_path(Bug.find(2)), params: {dedup_id: @dedup.id}
            assert_response 401
        end

        # with permissions
        User.first.update(can_review_frames: true)
        assert_difference '@dedup.bugs.count' do
            post associate_bug_path(Bug.find(2)), params: {dedup_id: @dedup.id}
        end
        @dedup.reload
        assert_not @dedup.is_acceptable

        # without login
        sign_out User.first
        assert_no_difference '@dedup.bugs.count' do
            post associate_bug_path(Bug.find(2)), params: {dedup_id: @dedup.id}
            assert_redirected_to new_user_session_path
        end
    end

    test "bug mark fixed" do
        @bug = Bug.find(2)
        User.first.update(can_review_frames: false)
        sign_in User.first
        # without permissions
        post mark_fixed_bug_path(@bug)
        assert_response 401

        # with permissions
        User.first.update(can_review_frames: true)
        post mark_fixed_bug_path(@bug)
        assert_response :redirect
        @bug.reload
        assert @bug.fixed_on

        # without login
        sign_out User.first
        post mark_fixed_bug_path(@bug)
        assert_redirected_to new_user_session_path
    end
end