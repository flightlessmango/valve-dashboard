class UsersChannelTest < ActionCable::Channel::TestCase
    test "subscribe" do
        assert subscribe users_channel: 1
    end
end

class ApplicationCable::ConnectionTest < ActionCable::Connection::TestCase
    include ActionCable::TestHelper
    include Devise::Test::IntegrationHelpers
    setup do
        sign_in User.first
    end

    test "connect" do
        connect env: { 'warden' => FakeEnv.new(User.first) }
        assert_equal connection.current_user, User.first
    end

    class FakeEnv
        attr_reader :user
  
        def initialize(user)
          @user = user
        end
    end
end