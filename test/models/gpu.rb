require "test_helper"
class GpuTest < ActiveSupport::TestCase
    test "create gpu" do
        assert Gpu.create!(name: "NAVI10")
    end
    
    test "set codename" do
        @gpu = Gpu.first
        @gpu.update(name: nil)
        assert_nil @gpu.name
        @gpu.set_codename
        assert_not_nil @gpu.name
        # if no pciid
        @gpu.update(pciid: nil)
        @gpu.trace_execs.destroy_all
        @gpu.codename
    end
end