require "test_helper"
class JobTest < ActiveSupport::TestCase
    test "create trace" do
        assert Trace.create!(game: Game.first)
    end

    test "Trace filenames" do
        assert Trace.filenames
    end
end