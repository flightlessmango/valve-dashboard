require "test_helper"
class TraceFrameTest < ActiveSupport::TestCase
  test "latest_generated_frames returns the expected FrameOutput's" do
    outputs = TraceFrame.find(1).latest_generated_frames
    assert_equal 3, outputs.count
    assert_equal outputs[0], 1
    assert_equal outputs[1], 4
    assert_equal outputs[2], 5
  end
end
