require "test_helper"
class DriverVersionTest < ActiveSupport::TestCase
    test "driver version to_s" do
        assert DriverVersion.last.to_s
    end
end