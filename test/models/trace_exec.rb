require "test_helper"
class TraceExecTest < ActiveSupport::TestCase
    test "create trace_exec" do
        traceExec = TraceExec.new(job: Job.first, trace: Trace.first,
                                  gpu: Gpu.first, execution_time: 270, driver_version: DriverVersion.first)
        assert traceExec.exec_log.attach(io: File.open(Rails.root.join('test', 'fixtures', 'files', 'test_file.log')),
                                  filename: 'test_file')
        assert traceExec.save!
    end
end