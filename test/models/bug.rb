require "test_helper"
class BugTest < ActiveSupport::TestCase
    test "bug probs" do
        Prob = Struct.new(:is_matched, :matched, :description)
        probs = []
        probs << Prob.new(false, [], "prob stuff")
        assert_equal Bug::probability_tooltip(probs), "prob stuff: Different\n"

        probs = []
        probs << Prob.new(true, ["GPU"], "prob stuff")
        assert_equal Bug::probability_tooltip(probs), "prob stuff: Matched (GPU)\n"
    end
end