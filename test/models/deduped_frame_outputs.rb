require "test_helper"
class DedupedFrameOutputTest < ActiveSupport::TestCase
    test "deduped model methods" do
        assert DedupedFrameOutput.find(1).next
        assert DedupedFrameOutput.find(2).prev
        assert_equal false, DedupedFrameOutput.find(2).seen_in_released_job
        assert_equal true, DedupedFrameOutput.find(1).seen_in_released_job
        assert DedupedFrameOutput.new(is_acceptable: nil).acceptability
        assert DedupedFrameOutput.new(is_acceptable: true).acceptability
        assert DedupedFrameOutput.new(is_acceptable: false).acceptability
        assert DedupedFrameOutput.first.to_s
        assert DedupedFrameOutput.first.codenames
        assert DedupedFrameOutput.first.codenames_prob
        assert DedupedFrameOutput.first.pciids
    end
end
