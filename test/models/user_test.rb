require "test_helper"
class Usertest < ActiveSupport::TestCase

    test "create user" do
        assert User.create!(email: "test2@email.com", username: "test", password: "test123")
    end

    test "user methods" do
        raw_info_struct = Struct.new(:url)
        raw_info = raw_info_struct.new("http://test.com")
        extra_struct = Struct.new(:raw_info)
        extra = extra_struct.new(raw_info)
        info_struct = Struct.new(:nickname, :email)
        info = info_struct.new("test", "test@auth.com")
        credentials_struct = Struct.new(:token, :expires_at, :refresh_token)
        credentials = credentials_struct.new("1234", 1234, "1234123")
        auth_struct = Struct.new(:provider, :uid, :info, :extra, :credentials)
        auth = auth_struct.new("discord", 123, info, extra, credentials)
        assert User::from_omniauth(auth)
        assert User.find(1).to_s
        assert User.find(3).to_s
        assert User.find(1).is_offline
        assert User.find(1).is_online
        assert User.first.list_permissions
    end
end