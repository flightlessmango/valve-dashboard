require "test_helper"
class DedupedFrameComparisonsTest < ActiveSupport::TestCase
    test "validations" do
        # test dedup1 and dedup2 has the same id
        comparison = DedupedFrameComparison.new
        comparison.dedup1 = 1
        comparison.dedup2 = 1
        comparison.save
        assert_not_empty comparison.errors[:base]

        # test that dedup2 is greater than dedup1
        comparison = DedupedFrameComparison.new
        comparison.dedup1 = 2
        comparison.dedup2 = 1
        comparison.save
        assert_not_empty comparison.errors[:base]

        # test a normal commit
        comparison = DedupedFrameComparison.new
        comparison.dedup1 = 1
        comparison.dedup2 = 2
        assert comparison.save

        # test uniqueness of dedup1 and dedup2
        comparison = DedupedFrameComparison.new
        comparison.dedup1 = 1
        comparison.dedup2 = 2
        assert_raises ActiveRecord::RecordNotUnique do
            assert_raises comparison.save
        end
    end
end
