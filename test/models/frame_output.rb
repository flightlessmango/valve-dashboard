require "test_helper"
class FrameOutputTest < ActiveSupport::TestCase
  test "history method returns the correct records in jobs id descending order" do
    outputs = FrameOutput.last.history
    # based on the fixtures, we expect to find frame output id 4 and 5
    assert_equal outputs.count, 2
    assert_equal outputs[0].id, 4
    assert_equal outputs[1].id, 5
  end
end
