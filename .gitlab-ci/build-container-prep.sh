#!/bin/bash

set -ex

curl -L get.rvm.io | bash
echo "source /usr/local/rvm/scripts/rvm" >> ~/.bashrc
source /usr/local/rvm/scripts/rvm
rvm install 3.1.3
su - postgres -c "initdb -D /var/lib/postgres/data"
mkdir /run/postgresql
chown postgres:postgres /run/postgresql
chmod 2775 /run/postgresql
rvm use 3.1.3
bundle
npm install --global yarn
yarn
NODE_OPTIONS=--openssl-legacy-provider rails webpacker:compile
RAILS_ENV=test NODE_OPTIONS=--openssl-legacy-provider rails webpacker:compile
su postgres -c "pg_ctl -D /var/lib/postgres/data start > /dev/null 2>&1"
su - postgres -c "createuser crz --superuser --no-password"
su - postgres -c "createuser root --superuser --no-password"
su postgres -c "pg_ctl -D /var/lib/postgres/data stop"
