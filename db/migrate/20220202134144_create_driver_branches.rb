class CreateDriverBranches < ActiveRecord::Migration[7.0]
  def change
    create_table :driver_branches do |t|
      t.string :name
      t.integer :driver_id

      t.timestamps
    end
  end
end
