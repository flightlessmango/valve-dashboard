class CreateGamesTags < ActiveRecord::Migration[6.0]
  def change
    create_table :games_tags do |t|
      t.integer :game_id
      t.integer :tag_id

      t.timestamps
    end
  end
end
