class CreateBenches < ActiveRecord::Migration[6.0]
  def change
    create_table :benches do |t|
      t.string  :name
      t.string  :type_name
      t.timestamps
    end
  end
end
