class AddIsReleasedCodeToJob < ActiveRecord::Migration[6.1]
  def change
    add_column :jobs, :is_released_code, :boolean
  end
end
