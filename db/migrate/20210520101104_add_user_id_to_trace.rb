class AddUserIdToTrace < ActiveRecord::Migration[6.0]
  def change
    add_column :traces, :user_id, :integer
  end
end
