# This migration comes from active_storage_resumable (originally 20190825001409)
class AddResumableUrlToActiveStorageBlobs < ActiveRecord::Migration[6.0]
  def change
    add_column :active_storage_blobs, :resumable_url, :text
  end
end
