class CreateFrameOutputs < ActiveRecord::Migration[6.0]
  def change
    create_table :frame_outputs do |t|
      t.integer :user_id
      t.integer :trace_id
      t.integer :trace_frame_id
      t.jsonb   :metadata, null: false, default: {}
      t.boolean :is_looking_acceptable, default: nil
      t.timestamps
    end
  end
end
