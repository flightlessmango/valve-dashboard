class AddIndexToDedup1And2inComparisons < ActiveRecord::Migration[7.0]
  def change
    add_index :deduped_frame_comparisons, [:dedup1, :dedup2], unique: true
  end
end
