class CreateTraceExecs < ActiveRecord::Migration[6.0]
  def change
    create_table :trace_execs do |t|
      t.integer :job_id
      t.integer :trace_id
      t.integer :execution_time

      t.timestamps
    end
  end
end
