class CreateSysinfos < ActiveRecord::Migration[6.0]
  def change
    create_table :sysinfos do |t|
      t.integer :bench_id
      t.string  :label, limit: 15
      t.string  :info
      t.timestamps
    end
  end
end
