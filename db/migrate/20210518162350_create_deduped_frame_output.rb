class CreateDedupedFrameOutput < ActiveRecord::Migration[6.0]
  def change
    create_table :deduped_frame_outputs do |t|
      t.boolean :is_acceptable
      t.integer :trace_frame_id
      t.integer :blob_id
    end
  end
end
