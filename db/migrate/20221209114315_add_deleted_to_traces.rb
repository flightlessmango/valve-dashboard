class AddDeletedToTraces < ActiveRecord::Migration[7.0]
  def change
    add_column :traces, :deleted, :boolean
  end
end
