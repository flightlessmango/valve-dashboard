class RemoveGpuIdFromTraceFrames < ActiveRecord::Migration[6.0]
  def change
    remove_column :trace_frames, :gpu_id, :integer
  end
end
