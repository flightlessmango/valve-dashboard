class AddCompareUrlToTimeline < ActiveRecord::Migration[7.0]
  def change
    add_column :job_time_lines, :url_for_comparing_commits, :string
  end
end
