class AddBaseUrlForIssuesToJobTimeLines < ActiveRecord::Migration[7.0]
  def change
    add_column :job_time_lines, :url_pattern_for_filing_issue, :string
  end
end
