class AddCompleteAtToJobs < ActiveRecord::Migration[6.0]
  def change
    add_column :jobs, :complete_at, :datetime
  end
end
