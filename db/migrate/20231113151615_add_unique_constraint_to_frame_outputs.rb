class AddUniqueConstraintToFrameOutputs < ActiveRecord::Migration[7.0]
  def change
    add_index :frame_outputs, [:trace_exec_id, :trace_frame_id], unique: true
  end
end
