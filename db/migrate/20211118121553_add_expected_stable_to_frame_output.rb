class AddExpectedStableToFrameOutput < ActiveRecord::Migration[6.1]
  def change
    add_column :frame_outputs, :expected_stable, :boolean
  end
end
