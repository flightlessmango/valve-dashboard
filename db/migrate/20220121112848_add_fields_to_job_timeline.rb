class AddFieldsToJobTimeline < ActiveRecord::Migration[7.0]
  def change
    add_column :job_time_lines, :project, :string
    add_column :job_time_lines, :branch, :string
    add_column :job_time_lines, :project_url, :string
    add_column :job_time_lines, :base_url_for_commits, :string
    add_column :jobs, :commit_version, :string
  end
end
