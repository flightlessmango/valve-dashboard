class AddOnlineBoolToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :online, :boolean
    add_column :users, :frame_lock, :integer
  end
end
