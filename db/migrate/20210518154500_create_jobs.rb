class CreateJobs < ActiveRecord::Migration[6.0]
  def change
    create_table :jobs do |t|
      t.jsonb :metadata
      t.integer :gpu_id
      t.integer :user_id

      t.timestamps
    end
  end
end
