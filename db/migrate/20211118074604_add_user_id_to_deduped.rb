class AddUserIdToDeduped < ActiveRecord::Migration[6.1]
  def change
    add_column :deduped_frame_outputs, :user_id, :integer
  end
end
