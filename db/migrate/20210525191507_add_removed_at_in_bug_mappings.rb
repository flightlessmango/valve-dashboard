class AddRemovedAtInBugMappings < ActiveRecord::Migration[6.0]
  def change
    add_column :bug_mappings, :removed_at, :datetime
  end
end
