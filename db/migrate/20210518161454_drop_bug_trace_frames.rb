class DropBugTraceFrames < ActiveRecord::Migration[6.0]
  def change
    drop_table :bugs_trace_frames
  end
end
