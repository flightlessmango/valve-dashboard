class AddUsernameAndBaseUrlToProvider < ActiveRecord::Migration[7.0]
  def change
    add_column :providers, :username, :string
    add_column :providers, :base_url, :string
    add_column :providers, :email, :string
  end
end
