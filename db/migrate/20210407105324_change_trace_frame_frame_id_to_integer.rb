class ChangeTraceFrameFrameIdToInteger < ActiveRecord::Migration[6.0]
  def change
    change_column :trace_frames, :frame_id, :integer, using: 'frame_id::integer'
  end
end
