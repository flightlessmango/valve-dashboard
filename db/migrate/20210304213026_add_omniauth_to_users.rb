class AddOmniauthToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :provider, :string
    add_column :users, :uid, :string
    add_column :users, :username, :string
    add_column :users, :can_download, :boolean, default: false
    add_column :users, :can_upload, :boolean, default: false
    add_column :users, :can_modify_metadata, :boolean, default: false
    add_column :users, :admin, :boolean, default: false
  end
end
