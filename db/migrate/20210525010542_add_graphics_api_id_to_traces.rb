class AddGraphicsApiIdToTraces < ActiveRecord::Migration[6.0]
  def change
    add_column :traces, :graphics_api_id, :integer
  end
end
