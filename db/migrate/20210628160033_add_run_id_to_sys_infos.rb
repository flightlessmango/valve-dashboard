class AddRunIdToSysInfos < ActiveRecord::Migration[6.0]
  def change
    add_column :sysinfos, :run_id, :integer
  end
end
