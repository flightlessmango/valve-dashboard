class CreateTraceFrames < ActiveRecord::Migration[6.0]
  def change
    create_table :trace_frames do |t|
      t.integer :trace_id
      t.string  :frame_id
      t.timestamps
    end
  end
end