class AddTraceStatusToGames < ActiveRecord::Migration[6.0]
  def change
    add_column :games, :trace_status, :string
  end
end
