class AddImageChecksumToBlob < ActiveRecord::Migration[6.1]
  def change
    add_column :active_storage_blobs, :image_checksum, :string
  end
end
