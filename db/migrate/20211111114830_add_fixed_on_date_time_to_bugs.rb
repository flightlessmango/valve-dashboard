class AddFixedOnDateTimeToBugs < ActiveRecord::Migration[6.1]
  def change
    add_column :bugs, :fixed_on, :datetime
  end
end
