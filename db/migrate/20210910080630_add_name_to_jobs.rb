class AddNameToJobs < ActiveRecord::Migration[6.1]
  def change
    add_column :jobs, :name, :string
  end
end
