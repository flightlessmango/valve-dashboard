class AddStatusToTraceExec < ActiveRecord::Migration[6.1]
  def change
    add_column :trace_execs, :status, :integer
  end
end
