class AddDriverIdtoTraceExec < ActiveRecord::Migration[7.0]
  def change
    add_column :trace_execs, :driver_version_id, :integer
  end
end
