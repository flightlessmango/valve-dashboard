class CreateRuns < ActiveRecord::Migration[6.0]
  def change
    create_table :runs do |t|
      t.integer :game_id
      t.integer :bench_id
      t.string  :name
      t.timestamps
    end
  end
end
