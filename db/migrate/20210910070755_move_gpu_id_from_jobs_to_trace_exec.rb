class MoveGpuIdFromJobsToTraceExec < ActiveRecord::Migration[6.1]
  def change
    remove_column :jobs, :gpu_id, :integer
    add_column :trace_execs, :gpu_id, :integer
    add_column :trace_execs, :metadata, :jsonb, null: false, default: {}
  end
end
