class CreateCommits < ActiveRecord::Migration[6.0]
  def change
    create_table :commits do |t|
      t.integer :project_id
      t.string :version

      t.timestamps
    end
  end
end
