class AddGpuIdToTraceFrame < ActiveRecord::Migration[6.0]
  def change
    add_column :trace_frames, :gpu_id, :integer
  end
end
