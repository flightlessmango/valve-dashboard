class CreateDriverVersions < ActiveRecord::Migration[7.0]
  def change
    create_table :driver_versions do |t|
      t.string :name
      t.integer :driver_branch_id

      t.timestamps
    end
  end
end
