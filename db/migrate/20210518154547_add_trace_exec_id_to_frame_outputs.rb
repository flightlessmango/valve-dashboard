class AddTraceExecIdToFrameOutputs < ActiveRecord::Migration[6.0]
  def change
    add_column :frame_outputs, :trace_exec_id, :integer
  end
end
