class CreateMetrics < ActiveRecord::Migration[6.0]
  def change
    create_table :metrics do |t|
      t.integer :run_id
      t.string  :label, limit: 15
      t.float   :data, array: true
      t.float   :avg
      t.float   :onepercent
      t.float   :ninetyseventh
      t.timestamps
    end
  end
end
