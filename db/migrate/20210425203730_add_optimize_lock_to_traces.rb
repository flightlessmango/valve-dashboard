class AddOptimizeLockToTraces < ActiveRecord::Migration[6.0]
  def change
    add_column :traces, :optimize_lock, :boolean
  end
end
