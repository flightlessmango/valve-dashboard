class AddAgreementCountToCompat < ActiveRecord::Migration[6.1]
  def change
    add_column :trace_compatibilities, :agreement_count, :integer
  end
end
