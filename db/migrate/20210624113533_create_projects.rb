class CreateProjects < ActiveRecord::Migration[6.0]
  def change
    create_table :projects do |t|
      t.string :name
      t.string :repo_url
      t.string :project_url
      t.string :base_url_for_commits

      t.timestamps
    end
  end
end
