class AddChecksumToFrameOutput < ActiveRecord::Migration[6.0]
  def change
    add_column :frame_outputs, :checksum, :string
  end
end
