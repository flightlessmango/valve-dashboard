class CreateProviders < ActiveRecord::Migration[7.0]
  def change
    create_table :providers do |t|
      t.integer :user_id
      t.string :provider
      t.integer :uid
      t.string :token
      t.string :refresh_token
      t.datetime :expires_at
      t.string :name
      t.timestamps
    end
  end
end
