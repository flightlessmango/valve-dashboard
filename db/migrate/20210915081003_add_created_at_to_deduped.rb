class AddCreatedAtToDeduped < ActiveRecord::Migration[6.1]
  def change
    add_column :deduped_frame_outputs, :created_at, :datetime, precision: 6
  end
end
