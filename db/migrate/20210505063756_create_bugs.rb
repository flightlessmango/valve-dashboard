class CreateBugs < ActiveRecord::Migration[6.0]
  def change
    create_table :bugs do |t|
      t.string :issue_url
      t.string :title

      t.timestamps
    end
    create_table :bugs_trace_frames, id: false do |t|
      t.belongs_to :bug
      t.belongs_to :trace_frame
    end
  end
end
