class AddAutoAcceptCriteriaToDedupedFrameOutputs < ActiveRecord::Migration[7.0]
  def change
    add_column :deduped_frame_outputs, :auto_acceptance_criteria, :jsonb
  end
end
