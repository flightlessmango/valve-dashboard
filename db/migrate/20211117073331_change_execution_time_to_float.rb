class ChangeExecutionTimeToFloat < ActiveRecord::Migration[6.1]
  def change
    change_column :trace_execs, :execution_time, :float
  end
end
