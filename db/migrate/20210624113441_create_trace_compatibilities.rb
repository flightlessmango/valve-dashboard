class CreateTraceCompatibilities < ActiveRecord::Migration[6.0]
  def change
    create_table :trace_compatibilities do |t|
      t.integer :commit_id
      t.integer :trace_id
      t.integer :gpu_id
      t.boolean :is_working
      t.integer :bug_id

      t.timestamps
    end
  end
end
