class CreateBugMapping < ActiveRecord::Migration[6.0]
  def change
    create_table :bug_mappings, :id => false do |t|
      t.integer :bug_id
      t.integer :deduped_frame_output_id
    end
  end
end
