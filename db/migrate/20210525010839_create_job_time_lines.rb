class CreateJobTimeLines < ActiveRecord::Migration[6.0]
  def change
    create_table :job_time_lines do |t|
      t.jsonb :metadata

      t.timestamps
    end
  end
end
