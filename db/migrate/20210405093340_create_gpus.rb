class CreateGpus < ActiveRecord::Migration[6.0]
  def change
    create_table :gpus do |t|
      t.string :name
      t.jsonb  :metadata
      t.timestamps
    end
  end
end
