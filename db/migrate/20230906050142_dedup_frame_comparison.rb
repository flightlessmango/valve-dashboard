class DedupFrameComparison < ActiveRecord::Migration[7.0]
  def change
    create_table :deduped_frame_comparisons do |t|
      t.integer :dedup1
      t.integer :dedup2
      t.integer :version
      t.jsonb :result
      # Add more columns as needed
      t.timestamps
    end
  end
end
