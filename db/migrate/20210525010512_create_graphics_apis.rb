class CreateGraphicsApis < ActiveRecord::Migration[6.0]
  def change
    create_table :graphics_apis do |t|
      t.string :name
      t.string :version

      t.timestamps
    end
  end
end
