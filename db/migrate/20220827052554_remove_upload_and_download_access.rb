class RemoveUploadAndDownloadAccess < ActiveRecord::Migration[7.0]
  def change
    remove_column :users, :can_download, :boolean
    remove_column :users, :can_upload, :boolean
  end
end
