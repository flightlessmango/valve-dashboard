class CreateTraces < ActiveRecord::Migration[6.0]
  def change
    create_table :traces do |t|
      t.integer :game_id
      t.jsonb   :metadata, null: false, default: {}
      t.boolean :obsolete, default: false
      t.integer :frames_to_capture, array: true, default: []
      t.integer :gpu_id
      t.timestamps
    end
  end
end