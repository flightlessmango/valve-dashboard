class AddDedupedIdtoFrameOutputs < ActiveRecord::Migration[6.0]
  def change
    add_column :frame_outputs, :deduped_frame_output_id, :integer
  end
end
