class AddIndexToForeignKeys < ActiveRecord::Migration[7.0]
  def change
    add_index :bug_mappings, :bug_id
    add_index :bug_mappings, :deduped_frame_output_id

    add_index :commits, :project_id
    add_index :deduped_frame_outputs, :trace_frame_id
    add_index :deduped_frame_outputs, :blob_id
    add_index :deduped_frame_outputs, :user_id

    add_index :driver_branches, :driver_id
    add_index :driver_versions, :driver_branch_id
    
    add_index :frame_outputs, :user_id
    add_index :frame_outputs, :trace_id
    add_index :frame_outputs, :trace_frame_id
    add_index :frame_outputs, :trace_exec_id
    add_index :frame_outputs, :deduped_frame_output_id

    add_index :games_tags, :game_id
    add_index :games_tags, :tag_id

    add_index :jobs, :user_id
    add_index :jobs, :timeline_id

    add_index :metrics, :run_id

    add_index :runs, :game_id
    add_index :runs, :bench_id

    add_index :sysinfos, :bench_id
    add_index :sysinfos, :run_id

    add_index :trace_compatibilities, :commit_id
    add_index :trace_compatibilities, :trace_id
    add_index :trace_compatibilities, :gpu_id
    add_index :trace_compatibilities, :bug_id

    add_index :trace_execs, :job_id
    add_index :trace_execs, :trace_id
    add_index :trace_execs, :gpu_id
    add_index :trace_execs, :driver_version_id

    add_index :trace_frames, :trace_id
    add_index :trace_frames, :frame_id

    add_index :traces, :game_id
    add_index :traces, :gpu_id
    add_index :traces, :user_id
    add_index :traces, :graphics_api_id
  end
end
