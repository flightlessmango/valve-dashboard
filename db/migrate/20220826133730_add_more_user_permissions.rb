class AddMoreUserPermissions < ActiveRecord::Migration[7.0]
  def change
    add_column :users, :can_upload_trace, :boolean
    add_column :users, :can_download_trace, :boolean
    add_column :users, :can_create_game, :boolean
    add_column :users, :can_destroy_game, :boolean
    add_column :users, :can_edit_game, :boolean
    add_column :users, :can_review_frames, :boolean
    add_column :users, :can_upload_frames, :boolean
  end
end
