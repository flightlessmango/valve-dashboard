class AddPciidToGpus < ActiveRecord::Migration[6.0]
  def change
    add_column :gpus, :pciid, :string
  end
end
