# Game.create!(name: "World of Warcraft")
# Game.create!(name: "Doom Eternal")
# Game.create!(name: "Grand Theft Auto 5")
# length = 500
# name_array = ["DXVK 1.4.4", "DXVK 1.5", "DXVK 1.6", "DXVK 1.7"]
# 10.times do |i|
#     @bench = Bench.create(name: name_array[Faker::Number.between(from: 0, to: 3)], type_name: "DXVK", game: Game.find(Faker::Number.between(from: 1, to: 3)),
#                           gpu: "2080 RTX", driver: "455.50.04")
#     fps_base = Faker::Number.between(from: 1, to: 10) * 100
#     length.times do |x|
#         fps_rand = Faker::Number.between(from: 1, to: 900) / 100
#         Input.create(bench: @bench, gpu: ((fps_base / 10) * 1.13) / 2, cpu: ((fps_base / 10) * 1.13) / 3,
#         fps: fps_base + fps_rand, frametime: 1000 / (fps_base + fps_rand), gpu_core_clock: 1500, gpu_mem_clock: 7000,
#         position: x, gpu_power: fps_base / 10)
#     end
#     puts i
# end

@user = User.create(username: "testuser", password: "1234")
@game = Game.create(name: "Doom Eternal", appid: 782330)
@gpu = Gpu.create(pciid: "1002:744c")
@trace = Trace.create(gpu: @gpu, game: @game)