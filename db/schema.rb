# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 2023_07_12_084057) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", precision: nil, null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", precision: nil, null: false
    t.text "resumable_url"
    t.string "service_name", null: false
    t.string "image_checksum"
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "active_storage_variant_records", force: :cascade do |t|
    t.bigint "blob_id", null: false
    t.string "variation_digest", null: false
    t.index ["blob_id", "variation_digest"], name: "index_active_storage_variant_records_uniqueness", unique: true
  end

  create_table "benches", force: :cascade do |t|
    t.string "name"
    t.string "type_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "bug_mappings", id: false, force: :cascade do |t|
    t.integer "bug_id"
    t.integer "deduped_frame_output_id"
    t.datetime "removed_at", precision: nil
    t.index ["bug_id"], name: "index_bug_mappings_on_bug_id"
    t.index ["deduped_frame_output_id"], name: "index_bug_mappings_on_deduped_frame_output_id"
  end

  create_table "bugs", force: :cascade do |t|
    t.string "issue_url"
    t.string "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "fixed_on", precision: nil
    t.text "description"
    t.boolean "confirmation"
  end

  create_table "commits", force: :cascade do |t|
    t.integer "project_id"
    t.string "version"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["project_id"], name: "index_commits_on_project_id"
  end

  create_table "deduped_frame_comparisons", force: :cascade do |t|
    t.integer "dedup1"
    t.integer "dedup2"
    t.integer "version"
    t.jsonb "result"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "deduped_frame_outputs", force: :cascade do |t|
    t.boolean "is_acceptable"
    t.integer "trace_frame_id"
    t.integer "blob_id"
    t.datetime "created_at"
    t.integer "user_id"
    t.jsonb "auto_acceptance_criteria"
    t.index ["blob_id"], name: "index_deduped_frame_outputs_on_blob_id"
    t.index ["trace_frame_id"], name: "index_deduped_frame_outputs_on_trace_frame_id"
    t.index ["user_id"], name: "index_deduped_frame_outputs_on_user_id"
  end

  create_table "driver_branches", force: :cascade do |t|
    t.string "name"
    t.integer "driver_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["driver_id"], name: "index_driver_branches_on_driver_id"
  end

  create_table "driver_versions", force: :cascade do |t|
    t.string "name"
    t.integer "driver_branch_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["driver_branch_id"], name: "index_driver_versions_on_driver_branch_id"
  end

  create_table "drivers", force: :cascade do |t|
    t.string "name"
    t.string "url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "frame_outputs", force: :cascade do |t|
    t.integer "user_id"
    t.integer "trace_id"
    t.integer "trace_frame_id"
    t.jsonb "metadata", default: {}, null: false
    t.boolean "is_looking_acceptable"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "checksum"
    t.integer "trace_exec_id"
    t.integer "deduped_frame_output_id"
    t.boolean "expected_stable"
    t.index ["deduped_frame_output_id"], name: "index_frame_outputs_on_deduped_frame_output_id"
    t.index ["trace_exec_id"], name: "index_frame_outputs_on_trace_exec_id"
    t.index ["trace_frame_id"], name: "index_frame_outputs_on_trace_frame_id"
    t.index ["trace_id"], name: "index_frame_outputs_on_trace_id"
    t.index ["user_id"], name: "index_frame_outputs_on_user_id"
  end

  create_table "games", force: :cascade do |t|
    t.string "name"
    t.bigint "appid"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "trace_status"
  end

  create_table "games_tags", force: :cascade do |t|
    t.integer "game_id"
    t.integer "tag_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["game_id"], name: "index_games_tags_on_game_id"
    t.index ["tag_id"], name: "index_games_tags_on_tag_id"
  end

  create_table "gpus", force: :cascade do |t|
    t.string "name"
    t.jsonb "metadata"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "pciid"
  end

  create_table "graphics_apis", force: :cascade do |t|
    t.string "name"
    t.string "version"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "job_time_lines", force: :cascade do |t|
    t.jsonb "metadata"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "project"
    t.string "branch"
    t.string "project_url"
    t.string "base_url_for_commits"
    t.string "url_pattern_for_filing_issue"
    t.string "url_for_comparing_commits"
  end

  create_table "jobs", force: :cascade do |t|
    t.jsonb "metadata"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "timeline_id"
    t.datetime "complete_at", precision: nil
    t.boolean "is_released_code"
    t.string "name"
    t.string "commit_version"
    t.index ["timeline_id"], name: "index_jobs_on_timeline_id"
    t.index ["user_id"], name: "index_jobs_on_user_id"
  end

  create_table "metrics", force: :cascade do |t|
    t.integer "run_id"
    t.string "label", limit: 15
    t.float "data", array: true
    t.float "avg"
    t.float "onepercent"
    t.float "ninetyseventh"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["run_id"], name: "index_metrics_on_run_id"
  end

  create_table "projects", force: :cascade do |t|
    t.string "name"
    t.string "repo_url"
    t.string "project_url"
    t.string "base_url_for_commits"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "providers", force: :cascade do |t|
    t.integer "user_id"
    t.string "provider"
    t.integer "uid"
    t.string "token"
    t.string "refresh_token"
    t.datetime "expires_at"
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "username"
    t.string "base_url"
    t.string "email"
  end

  create_table "runs", force: :cascade do |t|
    t.integer "game_id"
    t.integer "bench_id"
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["bench_id"], name: "index_runs_on_bench_id"
    t.index ["game_id"], name: "index_runs_on_game_id"
  end

  create_table "sysinfos", force: :cascade do |t|
    t.integer "bench_id"
    t.string "label", limit: 15
    t.string "info"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "run_id"
    t.index ["bench_id"], name: "index_sysinfos_on_bench_id"
    t.index ["run_id"], name: "index_sysinfos_on_run_id"
  end

  create_table "tags", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "trace_compatibilities", force: :cascade do |t|
    t.integer "commit_id"
    t.integer "trace_id"
    t.integer "gpu_id"
    t.boolean "is_working"
    t.integer "bug_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "agreement_count"
    t.index ["bug_id"], name: "index_trace_compatibilities_on_bug_id"
    t.index ["commit_id"], name: "index_trace_compatibilities_on_commit_id"
    t.index ["gpu_id"], name: "index_trace_compatibilities_on_gpu_id"
    t.index ["trace_id"], name: "index_trace_compatibilities_on_trace_id"
  end

  create_table "trace_execs", force: :cascade do |t|
    t.integer "job_id"
    t.integer "trace_id"
    t.float "execution_time"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "status"
    t.integer "gpu_id"
    t.jsonb "metadata", default: {}, null: false
    t.integer "driver_version_id"
    t.index ["driver_version_id"], name: "index_trace_execs_on_driver_version_id"
    t.index ["gpu_id"], name: "index_trace_execs_on_gpu_id"
    t.index ["job_id"], name: "index_trace_execs_on_job_id"
    t.index ["trace_id"], name: "index_trace_execs_on_trace_id"
  end

  create_table "trace_frames", force: :cascade do |t|
    t.integer "trace_id"
    t.integer "frame_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["frame_id"], name: "index_trace_frames_on_frame_id"
    t.index ["trace_id"], name: "index_trace_frames_on_trace_id"
  end

  create_table "traces", force: :cascade do |t|
    t.integer "game_id"
    t.jsonb "metadata", default: {}, null: false
    t.boolean "obsolete", default: false
    t.integer "frames_to_capture", default: [], array: true
    t.integer "gpu_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "optimize_lock"
    t.integer "user_id"
    t.integer "graphics_api_id"
    t.boolean "deleted"
    t.index ["game_id"], name: "index_traces_on_game_id"
    t.index ["gpu_id"], name: "index_traces_on_gpu_id"
    t.index ["graphics_api_id"], name: "index_traces_on_graphics_api_id"
    t.index ["user_id"], name: "index_traces_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at", precision: nil
    t.datetime "remember_created_at", precision: nil
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "provider"
    t.string "uid"
    t.string "username"
    t.boolean "can_modify_metadata", default: false
    t.boolean "admin", default: false
    t.boolean "online"
    t.integer "frame_lock"
    t.boolean "can_upload_trace"
    t.boolean "can_download_trace"
    t.boolean "can_create_game"
    t.boolean "can_destroy_game"
    t.boolean "can_edit_game"
    t.boolean "can_review_frames"
    t.boolean "can_upload_frames"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "active_storage_variant_records", "active_storage_blobs", column: "blob_id"
end
