class ErrorNotifier
    def initialize(app)
      @app = app
    end

    def call(env)
      @app.call(env)
    rescue StandardError => e
      notify_admin(e, env)
      raise
    end

    private

    def notify_admin(error, env)
      AdminMailer.error_notification(error, env).deliver_now
    end
end
