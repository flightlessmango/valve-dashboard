Gitlab.configure do |config|
    config.endpoint       = 'https://gitlab.freedesktop.org/api/v4' # API endpoint URL, default: ENV['GITLAB_API_ENDPOINT'] and falls back to ENV['CI_API_V4_URL']
end