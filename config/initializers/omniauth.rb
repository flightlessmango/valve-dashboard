require "openid/store/filesystem"
Rails.application.config.middleware.use OmniAuth::Builder do
    # provider :steam, ENV['STEAM_KEY']
    use OmniAuth::Builder do
      provider :gitlab, ENV['GITLAB_KEY'], ENV['GITLAB_SECRET'],
        {
            client_options: {
            site: 'https://gitlab.freedesktop.org'
            }
        }
      end
    provider :github, ENV['GITHUB_KEY'], ENV['GITHUB_SECRET'], scope: "user:email"
end
