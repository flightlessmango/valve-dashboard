Rails.application.routes.draw do
  devise_for :users, controllers: { omniauth_callbacks: 'users/omniauth_callbacks' }
  resources :games
  resources :benches, :path => "benchmarks" do
    collection do
      match 'stats' => 'benches#stats', via: [:get, :post], as: :stats
    end
  end
  resources :runs
  resources :traces do
    collection do
      match 'line_chart_amount' => 'traces#line_chart_amount', via: [:get], as: :line_chart_amount
    end
    member do
      get 'execution_time'
      get 'frames/:frame_id', to: 'traces#frames'
      post 'frames/:frame_id', to: 'traces#frames'
    end
    resources :trace_frames do
      resources :frame_outputs
    end
  end
  resources :static, only: [:index] do
    collection do
      get 'get_url'
      get 'get_ref_frames'
      post 'get_ref_frames'
    end
  end
  resources :users, only: [:edit, :show, :update, :index] do
    member do
      get 'edit_permissions'
    end
  end
  resources :gpus do
    member do
      get 'gpu_frames'
      get 'execution_time'
    end
    resources :frame_outputs
  end
  resources :frame_outputs do
    member do
      get 'history', to: 'frame_outputs#history'
    end
  end
  resources :games_tags, only: [:destroy]
  resources :bugs, only: [:create, :destroy, :index, :show, :update, :new] do
    member do
      post 'associate'
      post 'mark_fixed'
      post 're_open'
      get 'create_upstream_issue'
    end
    collection do
      match 'line_chart_amount' => 'bugs#line_chart_amount', via: [:get], as: :line_chart_amount
    end
  end
  resources :bugs_trace_frames, only: [:destroy]
  resources :jobs, only: [:destroy, :index, :show]
  resources :deduped_frame_outputs do
    member do
      post 'acceptable_frame'
      post 'unacceptable_frame'
      get 'frame_bug'
      get 'pre_merge_frame_bug'
      get 'issue'
      get 'next_frame'
      post 'dissociate'
    end
  end
  match 'stats' => 'static#stats', via: [:get], as: :stats
  resources :trace_execs
  resources :providers, only: [:destroy]

  namespace :api, defaults: { format: :json } do
    namespace :v1 do
      resources :graphics_apis
      match 'checksum' => 'static#checksum', via: [:get, :post], as: :checksum
      match 'image_checksum' => 'static#image_checksum', via: [:get, :post], as: :image_checksum
      match 'unoptimized_traces' => 'static#unoptimized_traces', via: [:get, :post], as: :unoptimized_traces
      match 'trace_lock' => 'static#trace_lock', via: [:get, :post], as: :trace_lock
      match 'frames_needing_review' => 'static#frames', via: [:get], as: :frames
      devise_scope :user do
        post 'login', to: 'sessions#create'
      end
      resources :traces do
        collection do
          match 'enrollment' => 'traces#enrollment', via: [:get], as: :enrollment
        end
      end
      resources :games
      resources :runs
      resources :jobs do
        resources :trace_execs
      end
      resources :trace_execs
      resources :gpus
      resources :users, only: [:index]
      resources :trace_compatibilities
      resources :commits
      resources :projects
      resources :benches
      resources :stats do
        collection do
          match 'frame_outputs' => 'stats#frame_outputs', via: [:get, :post], as: :frame_outputs
          get 'trace_frames/:id/' => 'stats#trace_frames', via: [:get], as: :trace_frames
          get 'blobs/:id/' => 'stats#blobs', via: [:get], as: :blobs
        end
      end
      resources :deduped_frame_outputs
    end
  end
  match "/-/profile", to: "users#profile", via: [:get], as: :profile
  match 'frames_needing_review' => 'static#frames', via: [:get], as: :frames
  match 'gpu_frames' => 'static#gpu_frames', via: [:get, :post], as: :gpu_frames
  root 'static#index'
end