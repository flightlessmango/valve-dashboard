json.trace_frame do
    json.frame_id @trace_frame.frame_id
    json.trace_id @trace_frame.trace_id
    json.trace_name @trace_frame.trace.filename
    json.deduped_frames @deduped do |deduped|
        json.created_at deduped.created_at
        json.id     deduped.id
        json.url    deduped.blob.url if deduped.frame_outputs.any? && Rails.env != "test"
        json.is_acceptable deduped.is_acceptable
        json.found_in_release_code_run deduped.jobs.pluck(:is_released_code).include? true
        last_seen = deduped.jobs.pluck(:created_at, :job_id, :name).max
        json.last_seen_in_job do
            json.id last_seen[1]
            json.name last_seen[2]
            json.created_at last_seen[0]
        end
        
        json.gpus do
            json.array! deduped.gpus do |gpu|
                json.name   gpu.name
                json.pciid  gpu.pciid
                json.frame_outputs_count deduped.frame_outputs.includes(:gpu).where(gpus: {id: gpu.id}).size
            end
        end
    end
end