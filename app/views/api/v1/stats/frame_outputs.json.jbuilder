# json.query                  params[:q]
json.frame_outputs          @frame_outputs.size
json.trace_frames           @trace_frames.size
json.deduped_frame_outputs  @deduped_frames.size
json.deduped_accepted       @accepted.size
json.deduped_unaccepted     @unaccepted.size
json.deduped_needs_review   @needs_review.size
json.participating_gpus     @frame_outputs.includes(:trace_exec).select(:gpu_id).pluck('trace_execs.gpu_id').uniq.size