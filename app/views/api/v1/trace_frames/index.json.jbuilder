json.array! @trace.trace_frames do |frame|
    json.id frame.id
    json.frame_id frame.frame_id
    json.outputs frame.frame_outputs
    # json.outputs api_v1_trace_trace_frame_frame_outputs_path(frame.frame_id)
end