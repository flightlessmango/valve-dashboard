json.array! @compats.each do |compat|
    json.id compat.id
    json.is_working compat.is_working
    if compat.commit
        json.commit do
            json.id compat.commit.id
            json.version compat.commit.version
            json.project do
                json.id compat.commit.project.id
                json.name compat.commit.project.name
            end
        end
    end
    json.bug do
        if compat.bug
            json.id compat.bug.id
        end
    end
    json.trace_id compat.trace_id
    json.gpu_id   compat.gpu_id
    json.agreement_count compat.agreement_count
end