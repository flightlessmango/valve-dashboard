json.array! @frame_output do |frame|
    json.id frame.id
    json.filename frame.upload.filename
    json.url frame.upload.url
end