unless defined?(dedup)
    dedup = @dedup
end
json.id dedup.id
json.url polymorphic_url(dedup.upload)
if dedup.find_ref_frame
    json.ref_frame_url polymorphic_url(dedup.find_ref_frame.upload)
end