json.array! @traces do |trace|
    json.id trace.id
    json.graphics_api trace.graphics_api.name if trace.graphics_api
    tracing_tool = ""
    if trace.filename.to_s.include? ".trace"
        tracing_tool = "apitrace"
    else
        tracing_tool = "gfxrecon"
    end
    json.tracing_tool tracing_tool
    json.game do
        json.id trace.game_id
        json.name trace.game.name
    end
    if trace.upload.attached?
        json.filename trace.upload.filename
        json.file_size trace.upload.blob.byte_size
        json.url trace.upload.url if Rails.env != "test" && (current_user && current_user.can_download_trace)
        json.checksum trace.upload.blob.checksum
    end
    json.metadata trace.metadata
    json.obsolete trace.obsolete
    json.frames_to_capture do
        trace.frames_to_capture.each do |frame|
            json.set! frame, trace.trace_frames.map {|trace_frame| trace_frame.id if trace_frame.frame_id == frame}[0]
        end
    end
    json.trace_frames trace.trace_frames
    json.created_at trace.created_at
    json.updated_at trace.updated_at
    if params[:target_gpu_pciid]
        json.set! "compatibility" do
            json.set! "projects" do
                trace.projects.each do |project|
                    json.set! project.name do
                        project.commits.includes(:trace_compatibilities).where(trace_compatibilities: {gpu_id: @gpu.id}).each do |commit|
                            json.set! commit.version do
                                json.is_working TraceCompatibility.find_by(gpu_id: @gpu.id, commit_id: commit.id).is_working
                                json.bugs commit.bugs
                            end
                        end
                    end
                end
            end
        end
    end
end
