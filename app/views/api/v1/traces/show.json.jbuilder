json.id @trace.id
json.game_id @trace.game_id
if @trace.upload.attached?
    json.filename @trace.upload.filename
    json.file_size @trace.upload.blob.byte_size
    json.url @trace.upload.url
end
json.metadata @trace.metadata
json.obsolete @trace.obsolete
json.frames_to_capture @trace.frames_to_capture
json.trace_frames @trace.trace_frames
json.created_at @trace.created_at
json.updated_at @trace.updated_at