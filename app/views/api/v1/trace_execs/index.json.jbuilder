json.array! @traceExecs do |traceExec|
    json.id traceExec.id
    json.job traceExec.job
    json.trace traceExec.trace
    json.execution_time traceExec.execution_time
    json.status traceExec.status
    json.exec_log traceExec.exec_log.url
    json.gpu traceExec.gpu
    json.trace_frames do
    outs = traceExec.frame_outputs
        traceExec.trace_frames.each do |frame|
            json.set! frame.frame_id do
                json.id frame.id
                json.frame_outputs do
                    outputs = outs.map {|out| out if traceExec.id == out.trace_exec_id && frame.id == out.trace_frame_id}.compact
                    outputs.each do |out|
                        json.id out.id
                        json.deduped_frame_output do
                            json.id out.deduped_frame_output.id
                            json.is_acceptable out.deduped_frame_output.is_acceptable
                            json.blob_id out.deduped_frame_output.blob_id
                            json.created_at out.deduped_frame_output.created_at
                        end
                    end
                end
            end
        end
    end
    if traceExec.driver_version
        json.driver do
            json.name traceExec.driver_version.driver.name
            json.branch traceExec.driver_version.driver_branch.name
            json.commit traceExec.driver_version.name
        end
    end
end
