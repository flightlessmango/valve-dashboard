json.id @traceExec.id
json.job @traceExec.job
json.trace @traceExec.trace
json.execution_time @traceExec.execution_time
json.status @traceExec.status
json.gpu @traceExec.gpu
outs = @traceExec.frame_outputs.includes(:deduped_frame_output)
json.trace_frames do
    @traceExec.trace_frames.each do |frame|
        json.set! frame.frame_id do
            json.id frame.id
            json.frame_outputs do
                outs.map {|out| out if out.trace_frame_id == frame.id }.compact.each do |out|
                    json.id out.id
                    json.deduped_frame_output do
                        json.id out.deduped_frame_output.id
                        json.is_acceptable out.deduped_frame_output.is_acceptable
                        json.blob_id out.deduped_frame_output.blob_id
                        json.created_at out.deduped_frame_output.created_at
                    end
                end
            end
        end
    end
end