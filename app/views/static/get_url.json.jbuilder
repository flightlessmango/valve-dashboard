if Rails.env == "development"
    json.image 'https://via.placeholder.com/1920x1080.png'
else
    json.image url_for(@dedup.upload)
end