json.series @data.each do |gpu, execs|
    json.name @codenames[gpu]
    json.data execs.each do |exec_|
        if exec_.execution_time != nil && exec_.execution_time > 0
            json.x exec_.created_at.to_i * 1000
            json.y exec_.execution_time
            json.exec_id exec_.id
        end
    end
end