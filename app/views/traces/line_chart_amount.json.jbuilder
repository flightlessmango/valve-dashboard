months = Trace.group_by_month(:created_at).size.to_a
month_amount = Trace.group_by_month(:created_at).size.size
array = []
month_amount.times do |i|
    start = Date.parse(months[0][0].to_s)
    _end = Date.parse(months[i][0].to_s).end_of_month
    array << [months[i][0], Trace.select(:id).where(created_at: start.._end).count]
end

json.array! array