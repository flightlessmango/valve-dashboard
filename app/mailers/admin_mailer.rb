class AdminMailer < ApplicationMailer
    default to: ENV['ERROR_EMAIL_TO'], from: ENV['ERROR_EMAIL_FROM']

    def error_notification(error, env)
      @error = error
      @env = env
      mail(subject: "500 Internal Server Error: #{error.message}")
    end
end