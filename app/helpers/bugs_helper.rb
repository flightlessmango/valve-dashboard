module BugsHelper
    include ActionView::Helpers::UrlHelper

    def job_name(job, jobs)
        ago = jobs.index(job)
        link = link_to "#" + job.id.to_s, job
        commit_link = link_to "#" + job.commit_version, job.timeline.base_url_for_commits + job.commit_version if job.commit_version
        return link + " - " + commit_link + " (" + ago.to_s + " runs ago" + ")"
    end

    def repro_rate(frame_outputs, bug)
        tag.i(class: "far fa-list-alt", title: BugsController.repro_rate(frame_outputs, bug))
    end
end