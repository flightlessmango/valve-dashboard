module CommonHelper
    include ActionView::Helpers::UrlHelper
    def deduped_preview(frame, icon, url)
        if frame != {}
            if frame.kind_of?(DedupedFrameOutput)
                frame_struct = Struct.new(:deduped_frame_output)
                frame = frame_struct.new(frame)
            end

            image = ""
            if icon.is_a?(Struct)
                image << tag.i("", class: "fa-2x far fa-image")
            else
                image << icon
            end
            image << tag.span do
                tag.img(src: "")
            end

            if frame.is_a?(FrameOutput)
                url = '/deduped_frame_outputs/' + frame.deduped_frame_output.id.to_s
            elsif frame.is_a?(DedupedFrameOutput)
                url = '/deduped_frame_outputs/' + frame.id.to_s
            end

            image_class = "image-" + frame.deduped_frame_output.id.to_s
            link = tag.a("Frame", class: ["thumbnail", image_class],
            data: {id: frame.deduped_frame_output.id},
            href: url) do
                raw(image)
            end
        end

        if icon.is_a?(Struct)
            icon = tag.i(icon.text, class: [icon.icon, icon.color], title: [icon.title])
            return icon + " " + link
        else
            return link
        end
    end

    def show_percentage(count, total)
        if total != 0
            percentage = ((count.to_f / total) * 100).to_i
            return "#{count} / #{total} (#{percentage}%)"
        else
            return "#{count} / #{total}"
        end
    end
end
