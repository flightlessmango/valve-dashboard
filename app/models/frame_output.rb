class FrameOutput < ApplicationRecord
    belongs_to :user
    has_one_attached :upload
    has_one_attached :img_diff
    belongs_to :deduped_frame_output
    belongs_to :trace_exec, optional: true
    belongs_to :trace_frame, optional: true
    has_one :trace, through: :trace_frame
    has_one :job, through: :trace_exec
    has_one :gpu, through: :trace_exec
    has_one :game, through: :trace
    has_many :bugs, -> { select(:id).distinct }, through: :deduped_frame_output
    has_one :timeline, through: :job
    has_one :driver_version, through: :trace_exec
    has_one :driver_branch, through: :driver_version
    has_one :driver, through: :driver_branch
    has_one :blob, through: :deduped_frame_output
    has_one :timeline, through: :job, foreign_key: :timeline_id, class_name: 'JobTimeLine'
    attr_accessor :ignore_for_repro

    def icon(bug = nil)
        if self.deduped_frame_output.is_acceptable
            icon = "fas fa-check-circle"
            color = "text-success"
            title = "Acceptable"
            text = ""
        elsif self.deduped_frame_output.is_acceptable == nil
            icon = "fas fa-question-circle"
            color = "text-primary"
            title = "Needs review"
            text = ""
        elsif not bug and self.deduped_frame_output.is_acceptable == false
            icon = "fas fa-times-circle"
            color = "text-danger"
            title = "Not acceptable"
            text = ""
        elsif bug and self.bugs.ids.include? bug.id
            icon = "fas fa-times-circle"
            color = "text-danger"
            title = "Is instance of current bug"
            text = ""
        else
            if (bug)
                icon = "fas fa-exclamation-circle"
                color = "text-warning"
                title = "Is instance of a different bug"
                text = ""
            end
        end
        icon_struct = Struct.new(:icon, :color, :title, :text)
        return icon_struct.new(icon, color, title, text)
    end

    def history
        FrameOutput.includes(:gpu, :driver, :job, :trace_frame)
                   .joins(:gpu, :driver, :job)
                   .where(gpus: { id: self.gpu.id }, drivers: { id: self.driver.id }, jobs: { timeline_id: self.timeline.id }, trace_frame: {id: self.trace_frame_id}).
                    order('jobs.id DESC')
    end
end
