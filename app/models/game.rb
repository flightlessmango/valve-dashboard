class Game < ApplicationRecord
    has_many :runs
    has_many :traces, dependent: :destroy
    has_many :benches, through: :runs
    has_and_belongs_to_many :tags
    validates :appid, uniqueness: true, if: Proc.new { |game| game.appid.present? }
    validates :name, uniqueness: true

    # def self.steam_game_query(trace, appid)
    #     require 'net/http'
    #     source = "https://store.steampowered.com/api/appdetails?appids=" + appid
    #     resp = Net::HTTP.get_response(URI.parse(source))
    #     json = JSON.parse(resp.body)
    #     if json[appid]["success"]
    #         @game = Game.create!(name: json[appid]["data"]["name"], appid: appid)
    #         trace.game = @game
    #         return trace
    #     else
    #         render html: "Incorrect appid: #{appid}"
    #         return nil
    #     end
    # end

    # def self.steam_get_name(appid)
    #     require 'net/http'
    #     appid = appid.to_s
    #     source = "https://store.steampowered.com/api/appdetails?appids=" + appid
    #     resp = Net::HTTP.get_response(URI.parse(source))
    #     json = JSON.parse(resp.body)
    #     if json[appid]["success"]
    #         return json[appid]["data"]["name"]
    #     else
    #         return false
    #     end
    # end
end
