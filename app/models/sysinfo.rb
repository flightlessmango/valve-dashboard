class Sysinfo < ApplicationRecord
    belongs_to :bench, optional: true
    belongs_to :run, optional: true
end
