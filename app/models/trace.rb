class Trace < ApplicationRecord
    belongs_to :game
    has_many :trace_frames, dependent: :destroy
    has_one_attached :upload
    has_one_attached :optimized
    belongs_to :gpu, optional: true
    belongs_to :graphics_api, optional: true
    belongs_to :user, optional: true
    has_many :trace_compatibilities
    has_many :projects, -> { distinct }, through: :trace_compatibilities
    has_many :frame_outputs
    has_many :timelines, -> { distinct }, through: :frame_outputs
    has_many :deduped_frame_outputs, -> { distinct }, through: :frame_outputs
    has_many :trace_execs
    has_many :gpus, -> { distinct }, through: :trace_execs
    has_many :blobs, -> { select(:id, :service_name, :filename).distinct }, through: :upload_attachment
    accepts_nested_attributes_for :trace_frames

    TRACE_STATUS=["Traced", "Requires Tracing", "Requires GFXRecon Trace", "Tracing Failed"]

    def filename
        self.upload.filename.to_s
    end

    def self.filenames
        return Trace.includes(:blobs).pluck(:id, 'active_storage_blobs.filename').to_h
    end 

end