class GraphicsApi < ApplicationRecord
    has_many :traces
    has_many :trace_execs, through: :traces
    has_many :frame_outputs, through: :trace_execs
    has_many :jobs, through: :trace_execs
end
