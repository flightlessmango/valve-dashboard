class DriverVersion < ApplicationRecord
    has_many :trace_execs
    belongs_to :driver_branch, optional: true
    has_one :driver, -> { distinct }, through: :driver_branch
    has_many :gpus, through: :trace_exec
    has_many :jobs, -> { distinct }, through: :trace_execs
    has_many :timelines, through: :jobs

    def to_s
        if self.driver.name && self.name
            self.driver.name + " (" + self.name + ")"
        end
    end

    def self.all_drivers
        versions = Hash.new { |h, k| h[k] = Hash.new(&h.default_proc) }
        DriverVersion.includes(:driver).where.not(name: nil, driver: {name: nil}).each do |version|
            versions[version.id] = version.to_s if version.to_s
        end
        return versions
    end
end
