class DriverBranch < ApplicationRecord
    belongs_to :driver
    has_many :driver_version, -> { distinct }

    def to_s
        if self.driver.name && self.name
            self.driver.name + " (" + self.name + ")"
        end
    end

    def self.all_drivers
        branches = Hash.new { |h, k| h[k] = Hash.new(&h.default_proc) }
        DriverBranch.includes(:driver).where.not(name: nil, driver: {name: nil}).each do |branch|
            branches[branch.id] = branch.to_s if branch.to_s
        end
        return branches
    end
end
