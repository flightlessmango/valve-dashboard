class Bug < ApplicationRecord
    has_many :bug_mappings
    has_many :deduped_frame_outputs, -> { distinct }, through: :bug_mappings
    has_many :frame_outputs, -> { distinct }, through: :deduped_frame_outputs
    has_many :trace_frames, -> { distinct }, through: :deduped_frame_outputs
    has_many :trace_execs, -> { distinct }, through: :frame_outputs
    has_many :traces, -> { distinct }, through: :trace_execs
    has_many :gpus, -> { distinct }, through: :trace_execs
    has_many :jobs, -> { distinct }, through: :trace_execs
    has_many :driver_versions, -> { distinct }, through: :trace_execs
    has_many :driver_branches, -> { distinct }, through: :driver_versions
    has_many :drivers, -> { distinct }, through: :driver_branches
    has_many :timelines, -> { distinct }, through: :jobs
    has_many :games, -> { distinct}, through: :frame_outputs

    class Probabilities < Struct.new(:game, :trace, :trace_frame, :gpu, :pciid,
                                    :codename, :driver, :driver_branch, :driver_version, :timeline)
    end

    def probability(frame)
        probs = Probabilities.new
        prob = Struct.new(:bug_association, :association, :is_matched, :description, :matched)
        probs.game = prob.new(self.games, "game", false, "Game", [])
        probs.trace = prob.new(self.traces, "trace", false, "Trace", [])
        probs.trace_frame = prob.new(self.trace_frames, "trace_frame", false, "Trace Frame", [])
        probs.gpu = prob.new(self.gpus, "gpus", false, "GPU", [])
        probs.codename = prob.new(self.gpus.pluck(:name), "codenames_prob", false, "Codename", [])
        probs.pciid = prob.new(self.gpus.pluck(:pciid), "pciids", false, "PCIID", [])
        probs.driver = prob.new(self.drivers, "drivers", false, "Driver", [])
        probs.driver_branch = prob.new(self.driver_branches, "driver_branches", false, "Driver Branch", [])
        probs.driver_version = prob.new(self.driver_versions, "driver_versions", false, "Driver Versions", [])
        probs.timeline = prob.new(self.timelines, "timelines", false, "Timeline", [])

        probs.each do |prob_|
            if prob_ != nil
                prob_.bug_association.each do |self_|
                    if frame.public_send(prob_.association).class.to_s.include?("ActiveRecord_Associations")
                        frame.public_send(prob_.association).each do |assoc|
                            if assoc.id == self_.id
                                prob_.is_matched = true
                                prob_.matched << assoc
                            end
                        end
                    elsif frame.public_send(prob_.association).class.to_s.include?("Array")
                        frame.public_send(prob_.association).each do |assoc|
                            if assoc == self_
                                prob_.is_matched = true
                                prob_.matched << assoc
                            end
                        end
                    else
                        if frame.public_send(prob_.association).id == self_.id
                            prob_.is_matched = true
                            prob_.matched << frame.public_send(prob_.association)
                        end
                    end
                end
            end
        end

        possible_score = Probabilities.members.count
        score = 0
        probs.map {|prob| score += 1 if prob.is_matched}
        
        probability = ((score.to_f / possible_score.to_f) * 100).to_i.to_s + "%"
        tooltip = "Score: #{score}/#{possible_score}\n\n"
        return [tooltip + Bug::probability_tooltip(probs), probability]
    end

    def self.probability_tooltip(probs)
        tooltip = ""
        probs.each do |prob|
            if prob != nil
                if prob.is_matched
                    if prob.matched.count > 0 && prob.matched[0].class.to_s == "String"
                        tooltip += "#{prob.description}: Matched "
                        tooltip += "("
                        prob.matched.each do |match|
                            tooltip += match + ", "
                        end
                        tooltip = tooltip.chomp(', ')
                        tooltip += ")\n"
                    else
                        tooltip += "#{prob.description}: Matched\n"
                    end
                else
                    tooltip += "#{prob.description}: Different\n"
                end
            end
        end

        return tooltip
    end
end

