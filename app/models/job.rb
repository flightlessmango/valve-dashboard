class Job < ApplicationRecord
    has_many :trace_execs, -> { distinct }, dependent: :destroy
    has_many :traces, -> { distinct  }, through: :trace_execs
    has_many :graphics_apis, -> { distinct  }, through: :traces
    has_many :gpus, -> { distinct }, through: :trace_execs
    has_many :frame_outputs, -> { distinct  }, through: :trace_execs
    has_many :deduped_frame_outputs, -> { distinct }, through: :frame_outputs
    has_many :bugs, -> { distinct }, through: :deduped_frame_outputs
    has_many :jobs, -> { distinct }, through: :deduped_frame_outputs
    has_many :driver_versions, -> { distinct }, through: :trace_execs
    has_many :driver_branches, -> { distinct }, through: :driver_versions
    has_many :drivers, -> { distinct }, through: :driver_branches
    has_many :blobs, -> { select(:id, :filename, :service_name).distinct}, through: :traces
    has_many :trace_frames, through: :traces
    belongs_to :user
    belongs_to :timeline, foreign_key: :timeline_id, class_name: 'JobTimeLine'

    def next_in_timeline
        Job.where(timeline: self.timeline).where("id > ?", self.id).first
    end
    
    def prev_in_timeline
        Job.where(timeline: self.timeline).where("id < ?", self.id).last
    end
end