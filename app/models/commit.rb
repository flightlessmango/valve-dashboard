class Commit < ApplicationRecord
    belongs_to :project
    has_many :trace_compatibilities
    has_many :bugs, :through => :trace_compatibilities
    validates :version, uniqueness: { scope: :project_id}
end
