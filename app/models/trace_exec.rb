class TraceExec < ApplicationRecord
    belongs_to :job
    belongs_to :trace
    belongs_to :gpu
    has_one :graphics_api, -> { select(:name, :id) }, through: :trace
    has_many :frame_outputs, dependent: :destroy
    has_many :deduped_frame_outputs, -> { distinct }, through: :frame_outputs
    has_many :trace_frames, -> { distinct }, through: :frame_outputs
    has_many :bugs, -> { distinct }, through: :deduped_frame_outputs
    has_one_attached :exec_log
    belongs_to :driver_version
    has_one :driver_branch, through: :driver_version
    has_one :driver, through: :driver_branch
    has_one :timeline, through: :job
end
