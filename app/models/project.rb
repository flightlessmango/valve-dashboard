class Project < ApplicationRecord
    has_many :commits
    validates :name, uniqueness: true
end
