class DedupedFrameComparison < ApplicationRecord
    belongs_to :deduped_frame_output1, foreign_key: :dedup1, class_name: "DedupedFrameOutput"
    belongs_to :deduped_frame_output2, foreign_key: :dedup2, class_name: "DedupedFrameOutput"

    validate :ids_are_sorted
    validate :ids_are_different

    private

    def ids_are_sorted
      if dedup1 > dedup2
        errors.add(:base, "dedup1 is greater than dedup2")
      end
    end

    def ids_are_different
      if dedup1 == dedup2
        errors.add(:base, "dedup1 is the same as dedup2")
      end
    end
end
