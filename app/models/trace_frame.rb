class TraceFrame < ApplicationRecord
    belongs_to :trace
    has_many :frame_outputs
    has_many :deduped_frame_outputs
    has_one_attached :upload
    has_many :bugs, through: :deduped_frame_outputs
    has_many :jobs, through: :frame_outputs
    has_many :trace_execs, through: :frame_outputs
    has_many :gpus, -> { distinct }, through: :trace_execs
    has_many :timelines, -> { distinct }, through: :frame_outputs
    has_many :driver_versions, -> { distinct }, through: :trace_execs
    has_many :driver_branches, -> { distinct }, through: :driver_versions
    has_many :drivers, -> { distinct }, through: :driver_branches
    
    def game
        return self.trace.game
    end

    def latest_generated_frames(gpus = [], timelines = [], drivers = [])
        drivers = self.drivers if drivers.empty?
        gpus = self.gpus if gpus.empty?
        timelines = self.timelines if timelines.empty?

        data = TraceFrame.includes(:gpus, :timelines, :drivers, :jobs).
                where(gpus: {id: gpus.pluck(:id)},
                        timelines: {id: timelines.pluck(:id)},
                        drivers: {id: drivers.pluck(:id)},
                        trace_frames: {id: self.id}).
                        group('gpus.id', 'timelines.id', 'drivers.id').maximum('jobs.id')
    
                        
        outputs = FrameOutput.none
        data.each do |ids, job_id|
            query = FrameOutput.includes(:gpu, :driver, :job, :trace_frame).
            where(gpus: {id: ids[0]}, drivers: {id: ids[2]}, trace_frames: {id: self.id}, jobs: {id: job_id, is_released_code: true})

            outputs = outputs.or(query)
        end

        return outputs
    end
end
