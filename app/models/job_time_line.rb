class JobTimeLine < ApplicationRecord
    has_many :jobs, -> {distinct}, foreign_key: :timeline_id
    has_many :frame_outputs, -> {distinct}, through: :jobs
    has_many :deduped_frame_outputs, -> {distinct}, through: :frame_outputs

    def name
        self.project + " - " + self.branch
    end

    def self.timelines
        return JobTimeLine.all.map {|timeline| [timeline.id, timeline.name]}.to_h
    end

    def to_s
        self.project + " - " + self.branch
    end
end