class DedupedFrameOutput < ApplicationRecord
    has_many :bug_mappings
    has_many :bugs, -> { distinct.order(:id) }, through: :bug_mappings
    has_many :frame_outputs, -> { distinct }
    belongs_to :trace_frame
    has_many :trace_execs, -> { distinct }, through: :frame_outputs
    has_many :gpus, -> { distinct }, through: :frame_outputs
    has_one :trace, -> { distinct }, through: :trace_frame
    has_many :jobs, -> { distinct }, through: :frame_outputs
    belongs_to :blob, class_name: "ActiveStorage::Blob", foreign_key: "blob_id"
    has_one :game, through: :trace
    belongs_to :user, optional: true
    has_many :driver_versions, -> { distinct }, through: :trace_execs
    has_many :driver_branches, -> { distinct }, through: :driver_versions
    has_many :drivers, -> { distinct }, through: :driver_branches
    has_many :timelines, -> { distinct }, through: :frame_outputs
    has_many :deduped_frame_outputs, through: :frame_outputs
    attr_accessor :title
    # after_create :can_be_accepted

    def upload
        self.frame_outputs.joins(:upload_attachment).last.upload
    end

    def next
        self.trace_frame.deduped_frame_outputs.where("deduped_frame_outputs.id > ?", self.id).first
    end

    def prev
        self.trace_frame.deduped_frame_outputs.where("deduped_frame_outputs.id < ?", self.id).last
    end

    def seen_in_released_job
        if self.jobs.pluck(:is_released_code).include? true
            return true
        else
            return false
        end
    end

    def acceptability
        case self.is_acceptable
        when true
            return "Acceptable"
        when false
            return "Not acceptable"
        else
            return "Needs review"
        end
    end

    def to_s
        text = ""
        text << "#" + self.id.to_s
        text << ": " + self.jobs.first.created_at.strftime("%Y-%m-%d").to_s
        text << " -> " + self.jobs.last.created_at.strftime("%Y-%m-%d").to_s
        text << ": " + self.acceptability
    end

    def codenames
        return self.gpus.where.not(name: ["No codename", "{}"]).pluck(:id, :name).to_h
    end

    def codenames_prob
        return self.gpus.where.not(name: ["No codename", "{}"]).pluck(:name).to_a
    end

    def pciids
        return self.gpus.where.not(pciid: [nil, ""]).pluck(:pciid).to_a
    end

    def self.needing_review(limit: nil, trace_frame_pk: nil, driver_version_pk: nil, trace_pk: nil, gpu_pk: nil, timeline_pk: nil)
        subquery = FrameOutput.joins(:job, :trace_exec, :deduped_frame_output, trace_exec: [:driver_version], job: [:timeline]).where(jobs: {is_released_code: true}).
        select('"trace_execs"."gpu_id", "trace_execs"."driver_version_id", "frame_outputs"."trace_frame_id", "jobs"."timeline_id", MAX("trace_execs"."job_id") AS latest_job_id').
        group('"trace_execs"."gpu_id", "trace_execs"."driver_version_id", "frame_outputs"."trace_frame_id", "jobs"."timeline_id", "trace_execs"."gpu_id"')

        if trace_frame_pk
            subquery = subquery.where(trace_frame_id: trace_frame_pk)
        end

        if driver_version_pk
            subquery = subquery.where(trace_execs: {driver_version_id: driver_version_pk})
        end

        if trace_pk
            subquery = subquery.where(trace_execs: {trace_id: trace_pk})
        end

        if gpu_pk
            subquery = subquery.where(trace_execs: {gpu_id: gpu_pk})
        end

        if timeline_pk
            subquery = subquery.where(jobs: {timeline_id: timeline_pk})
        end

        return FrameOutput.joins(:job, :trace_exec, :deduped_frame_output).
        where(deduped_frame_output: {is_acceptable: nil}).where(jobs: {is_released_code: true}).
        select('"frame_outputs"."id", "frame_outputs"."deduped_frame_output_id", "trace_execs"."gpu_id", "trace_execs"."driver_version_id", "frame_outputs"."trace_frame_id", "jobs"."timeline_id", "trace_execs"."job_id"').
        where('("trace_execs"."gpu_id", "trace_execs"."driver_version_id", "frame_outputs"."trace_frame_id", "jobs"."timeline_id", "trace_execs"."job_id") IN (?)',
        subquery).
        limit(limit)
    end

    def find_ref_frame(dedup_refs=nil)
        frame = self
        unless dedup_refs
            dedup_refs = DedupedFrameOutput.includes(:gpus, :blob, :jobs, :trace_execs).
            where(trace_frame_id: frame.trace_frame.id, jobs: {is_released_code: true}).
            where("jobs.id < ?", frame.jobs.last.id).limit(1000)
        end
        where_hash = Hash.new { |h, k| h[k] = Hash.new(&h.default_proc) }
        where_hash["gpus"] = {id: frame.gpus.ids} if frame.gpus.size > 0
        where_hash["jobs"]["timeline_id"] = frame.timelines.ids if frame.timelines.size > 0
        where_hash["drivers"] = {id: frame.drivers.ids} if frame.drivers.size > 0
        where_hash["driver_branches"] = {id: frame.driver_branches.ids} if frame.driver_branches.size > 0
        where_hash["driver_versions"] = {id: frame.driver_versions.ids} if frame.driver_versions.size > 0
        while true do
            query = dedup_refs.includes(:gpus, :drivers, :driver_branches,
                                         :driver_versions, :jobs).
                                         where(trace_frame_id: frame.trace_frame_id).
                                         where.not(id: frame.id).
                                         where(is_acceptable: true)
            query = query.where(where_hash)
            break if !query.empty? || where_hash.empty?
            where_hash.delete(where_hash.keys.last)
        end
        return query.order(:id).last
    end

    def can_be_accepted
        auto_acceptance = Hash.new
        started_at = DateTime.now
        auto_acceptance["started_at"] = started_at.as_json
        MiniMagick.configure do |config|
            config.whiny = false
        end
        # Fetch images to compare
        if Rails.env.development?
            current = MiniMagick::Image.open("public/old.png")
            source = MiniMagick::Image.open("public/new.png")
            auto_acceptance["reference_frame"] = "public/new.png"
        elsif Rails.env.production?
            ref_frame = self.find_ref_frame
            return if not ref_frame
            if ref_frame
                auto_acceptance["reference_frame"] = ref_frame.id
                current = MiniMagick::Image.read(self.upload.download)
                source = MiniMagick::Image.read(ref_frame.upload.download)
            else
                return false
            end
        end

        auto_acceptance["success_criteria"] = Hash.new
        auto_acceptance["success_criteria"]["pixels_without_fuzz"] = 10
        auto_acceptance["success_criteria"]["pixels_with_fuzz"] = 0
        auto_acceptance["success_criteria"]["fuzz"] = 765

        # First compare to check that the pixel count is less than 10
        compare = MiniMagick::Tool::Compare.new
        compare.metric "ae"
        compare << source.path
        compare << current.path
        compare << "NULL:"
        compare.call do |stdout, stderr|
            auto_acceptance["pixels_without_fuzz"] = stderr.to_i
            if stderr.to_i < auto_acceptance["success_criteria"]["pixels_without_fuzz"]
                # Second compare with fuzz 765 (tolerance 3) checking that the pixel count is now 0
                compare = MiniMagick::Tool::Compare.new
                compare.metric "ae"
                compare.fuzz "765"
                compare << source.path
                compare << current.path
                compare << "NULL:"
                compare.call do |stdout, stderr|
                    auto_acceptance["pixels_with_fuzz"] = stderr.to_i
                    auto_acceptance["duration_s"] = (DateTime.now.to_i - started_at.to_i)
                    # Reload and lock the dedup before making sure it has not been marked as acceptable
                    # while we were processing. This prevents concurrent writes to the dedup
                    DedupedFrameOutput.transaction do
                        self.reload(lock: true)
                        if self.is_acceptable == nil
                            # if it's 0 then we deem it acceptable
                            self.update(auto_acceptance_criteria: auto_acceptance)
                            if stderr.to_i == auto_acceptance["success_criteria"]["pixels_with_fuzz"]
                                self.update(is_acceptable: true)
                            end
                        end
                    end
                    return stderr.to_i == 0
                end
            else
                self.update(auto_acceptance_criteria: auto_acceptance)
                return false
            end
        end
    end

    def issue_title(bug)
        "[Trace Testing] #{bug.title}"
    end

    def issue_body(bug)
        url_helper = Rails.application.routes.url_helpers
        ERB::Util.url_encode("_**Note**: We, the people behind [Valve ci-dashboard](https://ci-dashboard.steamos.cloud/), found this odd-looking frame in our periodic trace execution, which we track as [bug #{bug.id}](https://linux-perf.steamos.cloud/bugs/#{bug.id}). Through this issue, we are looking for a developer to lend their expert eye on it for a quick assessment, and maybe detect a regression._

# Description of the issue

#{bug.description}

# Affected traces / GPUs / drivers

The list of affected traces/drivers/GPUs is tracked in [bug #{bug.id}](https://ci-dashboard.steamos.cloud/bugs/#{bug.id}) of the ci-dashboard website.")
    end
    
    def self.layer_shortcuts_tooltip
        "Keybinds for layers

0-9: change difference tolerance to the number
Q: Toggle Diff frame visibility
W: Toggle Current frame visibility
A: Submits this frame as looking good"
    end
    
    def self.test_bug_thing
        gpu_codenames = Hash.new { |h, k| h[k] = Hash.new{|h, k| h[k] = Set.new()} }
        drivers = Hash.new { |h, k| h[k] = Hash.new{|h, k| h[k] = Set.new()} }
        @bug = Bug.find(31)
        timeline = JobTimeLine.find(443)
        @bug.trace_execs.includes(:driver_version, :timeline, :gpu).where(timeline: {id: timeline.id}).distinct.group_by(&:gpu).each do |gpu, trace_execs|
            trace_execs.each do |exec_|
                gpu_codenames[gpu.codename][gpu.pciid].add(exec_.driver_version)
            end
        end
        return gpu_codenames
        # gpu_codenames.each do |codename|
        #     gpu_codenames[codename[0]].each do |pciid|
        #         gpu_codenames[codename[0]][pciid[0]].each do |driver_version|
        #             drivers[driver_version.driver.name][driver_version.name]
        #         end
        #     end
        # end
    end
end