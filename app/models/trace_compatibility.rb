class TraceCompatibility < ApplicationRecord
    belongs_to :commit
    has_many :projects, through: :commit
    belongs_to :bug, optional: true
    belongs_to :trace
    belongs_to :gpu
    validates :commit_id, uniqueness: { scope: [:trace_id, :gpu_id]}
    validates :is_working, inclusion: { in: [ true, false ] }
end
