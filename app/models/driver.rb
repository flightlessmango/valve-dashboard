class Driver < ApplicationRecord
    has_many :driver_branches
    has_many :driver_versions, through: :driver_branches

    def self.valid_drivers
        Driver.where.not(name: ["Unknown", "N/A", nil])
    end
end
