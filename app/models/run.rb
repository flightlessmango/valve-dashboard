class Run < ApplicationRecord

    belongs_to :bench, optional: true
    belongs_to :game
    has_many :metrics, dependent: :destroy
    has_many :sysinfos, dependent: :destroy
    has_one_attached :upload

    def parse_upload
        self.metrics.destroy_all
        require 'csv'
        parsed = CSV.parse(upload.attachment.download)
        log_data_pos = find_row(parsed, "FRAME METRICS")
        sys_info_pos = find_row(parsed, "SYSTEM INFO")
        skip = 0
        # skip = skip_amount("Strange Brigade", parsed)
        metrics = []
        sysinfos = []
        if sys_info_pos > 0
            parsed[sys_info_pos].each_with_index do |col, i|
                sysinfos << Sysinfo.new(run_id: self.id, label: col, info: parsed[sys_info_pos + 1][i])
            end
        end
        log_data_pos = 2 if log_data_pos == 0
        parsed[log_data_pos].each_with_index do |col, x|
            data = []
            parsed.each_with_index do |row, i|
                if row[0].to_i > 0 && row[row.count - 1].to_i > skip
                    if col == "elapsed"
                        data.push(row[x].to_i - skip)
                    else
                        if col == "frametime"
                            if row[x].to_i > 10000
                                data.push(row[x].to_f / 1000)
                            else
                                data.push(row[x])
                            end
                        else
                            data.push(row[x])
                        end
                    end
                end
            end
            sum = 0
            data.each do |s|
                sum += s.to_f
            end
            metrics << Metric.new(label: col, data: data, run_id: id, avg: sum / data.count)
        end
        Metric.import metrics
        Sysinfo.import sysinfos if sysinfos.count > 0
    end

    def parse_hml
        # require 'csv'
        # parsed = CSV.parse(upload.attachment.download)
        # length = parsed.count
        # count = 0
        # fps = gpu = cpu = 0
        # parsed.each_with_index do |parse, i|
        #   if parsed[2][i] == "Framerate           "
        #     fps = i
        #   end
        #   if parsed[2][i] == "GPU1 usage          " || parsed[2][i] == "GPU usage           "
        #     gpu = i
        #   end
        #   if parsed[2][i] == "CPU usage           "
        #     cpu = i
        #   end
        # end
        # inputs = []
        # parsed.each_with_index do |parse, i|
        #     if i > 2
        #         inputs << Metric.new(run_id: self.id, fps: parse[fps], frametime: parse[fps].to_f / 1000, cpu: parse[cpu],
        #             gpu: parse[gpu], position: i)
        #     end
        # end
        # ActiveStorage::Attachment.import inputs
        # self.inputs.where(position: nil).destroy_all
        # self.inputs.where(fps: [0, nil]).destroy_all
    end

    def find_row(parsed, search)
        parsed.each_with_index do |parse, i|
            if (parse[0].include?(search))
                return i + 1
            end
        end
        return 0
    end
    
    # def skip_amount(game, parsed)
    #     if game == "Strange Brigade"
    #         return (1000000000 * 59) - parsed.last.last.to_i
    #     end
    # end
end