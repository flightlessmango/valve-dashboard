class Bench < ApplicationRecord

    has_many :runs, dependent: :destroy
    has_many :sysinfos, dependent: :destroy
    has_many :games, through: :runs
    
    TWENTY    =  [ '#e6194b', '#3cb44b', '#4363d8', '#911eb4', '#f58231', '#E899DC', '#bcf60c', '#fabebe', '#008080', '#e6beff', '#9a6324', '#fffac8',
        '#800000', '#aaffc3', '#808000', '#000075', '#808080' ]
    SYNC = ["NONE", "ESYNC", "FSYNC", "FUTEX2"]
    FILETYPES = ["MANGO", "HML"]
end