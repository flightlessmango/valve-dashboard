class User < ApplicationRecord
  has_many :providers, dependent: :destroy
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  Providers = %i[gitlab github]
  ProviderLinks = {github: "https://github.com", gitlab: "https://gitlab.freedesktop.org"}
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable,
         :omniauthable, omniauth_providers: Providers

  validates :username, uniqueness: true
  WHITELIST = ["76561199139518728", "76561198088081180", "76561197962413930"]
  PERMISSIONS = ["can_upload_trace", "can_download_trace", "can_create_game",
    "can_destroy_game", "can_edit_game", "can_review_frames",
    "can_upload_frames"]

  def self.from_omniauth(auth)
    # Check if we already have an account linked to this provider
    @provider = Provider.find_by(uid: auth.uid.to_i, name: auth.provider)
    username = auth.info.nickname ? auth.info.nickname : auth.info.username
    url = auth.extra.raw_info.url ? auth.extra.raw_info.url : auth.extra.raw_info.web_url
    @user = nil # Ensure @user is nil initially

    if @provider
      @provider.update(
        token: auth.credentials.token,
        expires_at: auth.credentials.expires_at,
        refresh_token: auth.credentials.refresh_token,
        username: username,
        base_url: url.to_s.gsub(username, ""),
        email: auth.info.email
      )
      @user = @provider.user
      return @user
    else
      if auth.info.email
        @provider = Provider.find_by(email: auth.info.email)
        if @provider
          @user = @provider.user
        end
      end
    end

    # If no provider or user has been found, create a new user
    if @user.nil?
      @user = User.create!(
        username: username,
        email: auth.info.email,
        password: Devise.friendly_token[0, 20]
      )
    end

    # Associate the new user to the provider
    Provider.create!(
      user: @user,
      uid: auth.uid.to_i,
      name: auth.provider,
      token: auth.credentials.token,
      expires_at: auth.credentials.expires_at,
      refresh_token: auth.credentials.refresh_token,
      username: username,
      base_url: url.gsub(username, ""),
      email: auth.info.email
    )

    return @user
  end

  def self.from_omniauth_logged_on(user, auth)
    @provider = Provider.find_by(uid: auth.uid.to_i, name: auth.provider)
    url = "https://github.com/" if auth.provider == "github"
    url = "https://gitlab.freedesktop.org/" if auth.provider == "gitlab"
    username = auth.info.nickname ? auth.info.nickname : auth.info.username
    if @provider
      @provider.update(username: username, base_url: url)
    end
    if not @provider
      Provider.create!(user: user, uid: auth.uid, name: auth.provider,
                      token: auth.credentials.token, expires_at: auth.credentials.expires_at,
                      refresh_token: auth.credentials.refresh_token, username: username,
                      base_url: url)
    end
  end

  def is_online
    self.update(online: true)
  end

  def is_offline
    self.update(online: false)
  end

  def to_s
    if username == uid || username.nil?
      "(" + uid.to_s + ")"
    else
      username + " " + "(" + uid.to_s + ")"
    end
  end

  def list_permissions
    out = ""
    User::PERMISSIONS.each do |p|
      if self.public_send(p)
        out = out + p + "</br>"
      end
    end
    return out
  end

  def list_permissions_as_list
    out = ""
    User::PERMISSIONS.each do |p|
      if self.public_send(p)
        out += "<li>"
        out = out + p
        out += "</li>"
      end
    end
    return out
  end

  def any_permissions
    User::PERMISSIONS.each do |p|
      if self.public_send(p)
        return true
      end
    end
    return false
  end

  def give_every_permission
    user = self
    User::PERMISSIONS.map {|s| s+"="}.each do |p|
      user.send(p, true)
    end
    user.admin = true
    user.save!
  end

  def gitlab
    return self.providers.find_by(name: "gitlab")
  end

  def github
    return self.providers.find_by(name: "github")
  end

end
