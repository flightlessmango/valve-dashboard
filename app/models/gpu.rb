class Gpu < ApplicationRecord
    has_one_attached :gpuz
    has_many :trace_execs
    has_many :traces, -> { distinct }, through: :trace_execs
    has_many :frame_outputs, -> { distinct }, through: :trace_execs
    has_many :jobs, -> { distinct }, through: :frame_outputs
    has_many :deduped_frame_outputs, -> { distinct }, through: :frame_outputs
    has_many :bugs, -> { distinct }, through: :deduped_frame_outputs
    has_many :driver_versions, -> { distinct }, through: :jobs
    has_many :drivers, -> { distinct }, through: :driver_versions
    after_initialize :set_codename

    validates_uniqueness_of :pciid, if: -> {user_gpu}

    def user_gpu
        return self.metadata.blank? || self.metadata["user"].blank?
    end

    # def get_device_name
    #     split = self.pciid.split(":")
    #     vendor = split[0].gsub("0x", "")
    #     device = split[1].gsub("0x", "")
    #     url = "https://pci-ids.ucw.cz/read/PC/" + vendor + "/" + device
    #     resp = Net::HTTP.start("pci-ids.ucw.cz", 80) { |http| http.get(URI.parse(url))}
    #     if resp.code.to_i != 200
    #         return "failed to get gpu name"
    #     end
    #     name = Nokogiri::HTML.parse(resp.body).css('.name').children.children.to_s.gsub("Name: ", "")
    #     self.update(name: name)
    #     return name
    # end

    # def self.gpu_name(pciid)
    #     split = pciid.split(":")
    #     vendor = split[0].gsub("0x", "")
    #     device = split[1].gsub("0x", "")
    #     url = "https://pci-ids.ucw.cz/read/PC/" + vendor + "/" + device
    #     resp = Net::HTTP.start("pci-ids.ucw.cz", 80) { |http| http.get(URI.parse(url))}
    #     if resp.code.to_i != 200
    #         return
    #     end
    #     name = Nokogiri::HTML.parse(resp.body).css('.name').children.children.to_s.gsub("Name: ", "")
    #     return name
    # end

    def codename
        if !self.trace_execs.empty?
            self.trace_execs.last.metadata["machine_tags"].each do |tag|
                if tag.include? "amdgpu:codename"
                    return tag.gsub!("amdgpu:codename:", "")
                end
            end
        end

        if self.pciid == nil
            return "No codename"
        end
    end

    def set_codename
        return if self.metadata and self.metadata["user"]
        if !self.name
            self.update(name: self.codename)
        end
    end

    def self.codenames
        return Gpu.where.not(name: ["No codename", "{}"]).pluck(:id, :name).to_h
    end

    def self.valid_gpus
        return Gpu.where.not(name: ["No codename", "{}"])
    end

    def most_compatible_gpu
        compatible_gpu = nil
        highest = 0
        if self.trace_execs.last.present?
            tags_for_self = self.trace_execs.last.metadata["machine_tags"]
            Gpu.valid_gpus.where.not(id: self.id).each do |gpu|
                if gpu.trace_execs.any?
                    tags = gpu.trace_execs.last.metadata["machine_tags"]
                    shared = tags_for_self & tags
                    if shared.size > highest
                        highest = shared.size
                        compatible_gpu = gpu
                    end
                end
            end
        end
        if compatible_gpu.nil?
            compatible_gpu = self.adjacent_gpu
        end
        return compatible_gpu
    end

    def adjacent_gpu
        gpu = Gpu.valid_gpus.where('id < ?', self.id).last
        if gpu.nil?
            Gpu.valid_gpus.where('id > ?', self.id).first
        end
        return gpu
    end

    def traced_gpu
        self.metadata["user"]
    end

end