class DedupedFrameOutputsController < ApplicationController
    before_action :authenticate_user!, except: [:show]
    before_action :can_review_frames, except: [:show]
    before_action :admin, except: [:show, :acceptable_frame,
                                  :dissociate, :pre_merge_frame_bug,
                                  :issue, :next_frame, :find_next_frame]
    skip_forgery_protection if Rails.env.test?

    def show
        @codenames = {}
        @bugs = []
        @bug = Bug.new
        @frame = []
        @frame_bugs = []
        @previous_accepted_frame = []
        @dedup_refs = []
        @drivers = Hash.new { |h, k| h[k] = Hash.new(&h.default_proc) }
        @timelines = Hash.new { |h, k| h[k] = Hash.new(&h.default_proc) }
        @frame = DedupedFrameOutput.where(id: params[:id]).
        includes(:trace_frame, :gpus, :bugs, :timelines, :drivers).last
        # outputs = FrameOutput.includes(:deduped_frame_output, :trace_exec).
        # where(deduped_frame_outputs: {is_acceptable: true}, trace_frame_id: @frame.trace_frame.id)
        @codenames = Gpu.codenames
        @q = DedupedFrameOutput.includes(:gpus, :blob, :jobs, :trace_execs).
             where(trace_frame_id: @frame.trace_frame.id, jobs: {is_released_code: true}).
             where("jobs.id < ?", @frame.jobs.last.id).
             ransack(params[:q])
        @dedup_refs = @q.result(distinct: true).limit(1000)
        @frame_bugs = @frame.bugs
        @bugs = Bug.includes(:trace_frames, :traces, :gpus).where(trace_frames: {id: @frame.trace_frame.id}).
        or(Bug.where(traces: {id: @frame.trace_frame.trace.id})).
        or(Bug.where(gpus: @frame.gpus))

        if params[:q] && params[:q][:id_eq]
            @previous_accepted_frame = DedupedFrameOutput.find(params[:q][:id_eq])
        else
           @previous_accepted_frame = @frame.find_ref_frame(@dedup_refs)
        end

        @frame.driver_versions.includes(:driver).order(:name, 'driver.name').each do |driver_version|
            @drivers[driver_version.driver.name][driver_version.name] = nil
        end
        if params[:q]
            @job = Job.find(params[:q][:job_id]) if params[:q][:job_id].present?
        end
        @frame.frame_outputs.includes(:timeline, :gpu).each do |out|
            @timelines[out.timeline][@codenames[out.gpu.id]][out.gpu] = out
        end
    end

    def acceptable_frame
        @frame = DedupedFrameOutput.find(params[:id])
        @frame.update!(is_acceptable: true, user: current_user)
        @frame.bugs.clear
        redirect_to next_frame_deduped_frame_output_path(@frame, job: params[:job])
    end

    def unacceptable_frame
        @frame = DedupedFrameOutput.find(params[:id])
        @frame.update!(is_acceptable: false, user: current_user)
        @frame.bugs.clear
        redirect_to next_frame_deduped_frame_output_path(@frame, job: params[:job])
    end

    def dissociate
        @frame = DedupedFrameOutput.find(params[:id])
        @bug = Bug.find(params[:bug])
        @frame.bugs.delete(@bug)
        redirect_to deduped_frame_output_path(@frame)
    end

    def pre_merge_frame_bug
        @frame = DedupedFrameOutput.find(params[:id])
        @frame.update(is_acceptable: false, user: current_user)
        redirect_to next_frame_deduped_frame_output_path(@frame, job: params[:job])
    end

    def issue
        paser = URI::Parser.new
        @frame = DedupedFrameOutput.find(params[:id])
        @timeline = JobTimeLine.find(params[:timeline])
        @trace_frame = @frame.trace_frame
        @trace = @trace_frame.trace
        @game = @trace.game
        title = paser.escape("[Trace Testing] #{@game.name}")
        trace_execs = TraceExec.where(id: @frame.frame_outputs.pluck(:trace_exec_id).uniq)
        metadatas = trace_execs.pluck(:metadata).uniq
        # gpu families
        gpu_codenames = ""
        @frame.codenames.each do |val, key|
            gpu_codenames += "      * " + key + "\n"
        end
        desc = "&issue[description]=" +
        paser.escape("- Trace: #{@trace.upload.filename}
- Game: #{@game.name}
- Frame id: #{@trace_frame.frame_id}
- Affects:
   * Gpu Families:\n" + gpu_codenames + "
\n![frame](#{url_for(@frame.upload)})")
        labels = "&issue[labels]=" + paser.escape("test")
        @url = @timeline.url_pattern_for_filing_issue % {title: title, description: desc}
        respond_to do |format|
            format.js
        end
    end

    def next_frame
        @frame = DedupedFrameOutput.find(params[:id])
        job_id = params[:job] && params[:job].size > 0 ? params[:job] : nil
        @next_frame = find_next_frame(@frame, job_id)
        if @next_frame.nil?
            if params[:job]
                redirect_to job_path(job_id)
            else
                redirect_to frames_path
            end
        else
            if job_id
                redirect_to deduped_frame_output_path(@next_frame, q: {job_id: job_id})
            else
                redirect_to deduped_frame_output_path(@next_frame)
            end
        end
    end

    def find_next_frame(frame, job_id=nil)
        if job_id
            @next_frames = DedupedFrameOutput.includes(:trace, frame_outputs: :trace_exec).
            where(is_acceptable: nil, trace_frame: frame.trace_frame, frame_outputs: {trace_execs: {job_id: job_id}}).
            order(:trace_frame_id, 'trace.id').limit(1)
            if @next_frames.empty?
                @next_frames = DedupedFrameOutput.includes(:trace, frame_outputs: :trace_exec).
                where(is_acceptable: nil, trace: frame.trace, frame_outputs: {trace_execs: {job_id: job_id}}).
                order(:trace_frame_id, 'trace.id').limit(1)
            end
            if @next_frames.empty?
                @next_frames = DedupedFrameOutput.includes(:trace, frame_outputs: :trace_exec).
                where(is_acceptable: nil, frame_outputs: {trace_execs: {job_id: job_id}}).
                order(:trace_frame_id, 'trace.id').limit(1)
            end
            return @next_frames.first
        else
            # We want to get the frame reviewer to review all the same frames for the same driver first
            # then the same trace for the same driver, then any other trace for the same driver,
            # then any other frame from the same driver, then any other frame.
            #
            # This maximizes the chances of figuring out a pattern or rendering
            # issue that would be visible on some GPUs only, and maybe could be visible on more than one frame for the trace.
            # We however prioritize reviewing all the frames for a particular driver first,
            # to avoid the situation where new frames for a driver in development would be prioritized over documenting released code

            # Try to find a dedup from the same trace frame and the same driver (we pick the oldest one when seen on many drivers)
            driver_id = frame.driver_versions.order(id: :asc).first.id
            needs_review_raw = DedupedFrameOutput.needing_review(limit: 1, trace_frame_pk: frame.trace_frame_id,
                                                                 driver_version_pk: driver_id)

            if needs_review_raw.empty?
                # Try to find a dedup from the same trace and the same driver (we pick the oldest one when seen on many drivers)
                needs_review_raw = DedupedFrameOutput.needing_review(limit: 1, trace_pk: frame.trace_frame.trace_id,
                                                                     driver_version_pk: driver_id)

                if needs_review_raw.empty?
                    # Try to find a dedup from the same driver (we pick the oldest one when seen on many drivers)
                    needs_review_raw = DedupedFrameOutput.needing_review(limit: 1, driver_version_pk: driver_id)

                    if needs_review_raw.empty?
                        # Pick any frame that is left to review
                        needs_review_raw = DedupedFrameOutput.needing_review(limit: 1)
                    end
                end
            end

            return DedupedFrameOutput.find(needs_review_raw.first.deduped_frame_output_id)
        end
    end

end