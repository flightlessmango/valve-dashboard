class ApplicationController < ActionController::Base
    before_action :authenticate_user!
    before_action :admin

    class ExecColumn < Struct.new(:codename, :gpu, :driver_version)
        def <=>(other)
            if (self[:codename] && other[:codename]) && self[:codename] != other[:codename]
                self[:codename] <=> other[:codename]
            elsif (self[:gpu] && other[:gpu]) && self[:gpu] != other[:gpu]
                self[:gpu] <=> other[:gpu]
            elsif (self[:driver_version] && other[:driver_version]) && self[:driver_version] != other[:driver_version]
                self[:driver_version] <=> other[:driver_version]
            else
                return 0
            end
        end
    end

    def admin
        if user_signed_in?
            unless current_user.admin?
                render html: "You don't have admin access", status: 401
            end
        else
            render html: "You don't have admin access", status: 401
        end
    end

    def can_upload_trace
        if user_signed_in?
            unless current_user.can_upload_trace?
                render html: "Not permitted to upload traces", status: 401
            end
        else
            render html: "Not permitted to upload traces", status: 401
        end
    end

    def can_create_game
        if user_signed_in?
            unless current_user.can_create_game?
                render html: "Not permitted to create game", status: 401
            end
        else
            render html: "Not permitted to create game", status: 401
        end
    end

    def can_destroy_game
        if user_signed_in?
            unless current_user.can_destroy_game?
                render html: "Not permitted to destroy game", status: 401
            end
        else
            render html: "Not permitted to destroy game", status: 401
        end
    end

    def can_edit_game
        if user_signed_in?
            unless current_user.can_edit_game?
                render html: "Not permitted to edit game", status: 401
            end
        else
            render html: "Not permitted to edit game", status: 401
        end
    end

    def can_review_frames
        if user_signed_in?
            if current_user.can_review_frames?
            else
                render html: "Not permitted to review frames", status: 401
            end
        else
            render html: "Not permitted to review frames", status: 401
        end
    end

    def can_upload_frames
        if user_signed_in?
            unless current_user.can_upload_frames?
                render html: "Not permitted to upload frames", status: 401
            end
        else
            render html: "Not permitted to upload frames", status: 401
        end
    end

end
