class BugsController < ApplicationController
    skip_forgery_protection
    before_action :authenticate_user!, except: [:index, :show, :line_chart_amount]
    before_action :can_review_frames, only: [:create, :new, :mark_fixed, :re_open, :associate, :update]
    before_action :admin, except: [:index, :show, :line_chart_amount, :create,
                                   :new, :mark_fixed, :re_open, :associate, :update]

    include BugsHelper

    class ReproductionRate < Struct.new(:frames_found, :frame_reproducing, :last_job, :first_job)
        require 'active_support/core_ext/numeric/time'
        require 'action_view'
        require 'action_view/helpers'
        include ActionView::Helpers::DateHelper

        def repro_rate
            if self.frames_found == 0
                rate = 0.0
            else
                rate = self.frame_reproducing.to_f / self.frames_found
            end
            return "Reproduction rate: #{self.frame_reproducing}/#{self.frames_found} (#{(rate * 100).to_i}%)"
        end

        def to_s
            tooltip = self.repro_rate
            if self.last_job != nil
                tooltip += "\nLatest sighting: #{self.last_job.name}: #{time_ago_in_words(self.last_job.created_at)} ago"
            end
            if self.first_job != nil
                tooltip += "\nEarliest sighting: #{self.first_job.name}: #{time_ago_in_words(self.first_job.created_at)} ago"
            end
            return tooltip
        end

        def pick_job(job1, job2, newest=true)
            if job1 == nil
                return job2
            elsif job2 == nil
                return job1
            elsif newest
                return job1.created_at > job2.created_at ? job1 : job2
            else
                return job1.created_at > job2.created_at ? job2 : job1
            end
        end

        def +(obj)
            last_job = pick_job(self.last_job, obj.last_job, newest=true)
            first_job = pick_job(self.first_job, obj.first_job, newest=false)
            return ReproductionRate.new(frames_found=self.frames_found+obj.frames_found,
                                        frame_reproducing=self.frame_reproducing+obj.frame_reproducing,
                                        last_job=last_job, first_job=first_job)
        end
    end

    def index
        @bugs = Bug.all.order(:id).where(fixed_on: nil)
        @bugs_fixed = Bug.all.order(:id).where.not(fixed_on: nil)
        @filenames = Trace.filenames
    end

    def create
        @bug = Bug.new(bug_params)
        @trace_frame = TraceFrame.find(params[:trace_frame_id])
        @frame = DedupedFrameOutput.find(params[:deduped_frame_output_id])
        respond_to do |format|
            if @bug.save
                @frame.bugs << @bug
                @frame.update(is_acceptable: false, user: current_user)
                format.html
                format.js
            end
        end
    end

    def self.repro_rate(frame_outputs, bug)
        rate = ReproductionRate.new(0, 0, nil, nil)
        current_job = nil
        frame_outputs.each do |out|
            value = frame_outputs[out[0]]
            if out[0].class.to_s == "Job"
                current_job = out[0]
                if rate.last_job == nil || rate.last_job.created_at < current_job.created_at
                    rate.last_job = current_job
                end
                if rate.first_job == nil || rate.first_job.created_at > current_job.created_at
                    rate.first_job = current_job
                end
            end
            if value.class.to_s == "Hash"
                rate += repro_rate(value, bug)
            elsif value.class.to_s == "FrameOutput" && !frame_outputs[out[0]].ignore_for_repro
                rate.frames_found += 1
                if value.bugs.ids.include? bug.id
                    rate.frame_reproducing += 1
                end
            end
        end
        return rate
    end

    def show
        @bug = Bug.find(params[:id])
        @timelines = Hash.new
        timelinebug = Struct.new(:timeline, :frame_outputs, :drivers, :gpu_codenames,
                                 :gpu_repro, :jobs, :columns, :traces, :hidden_jobs,
                                 :current_job, :previous_job)
        @filenames = Trace.filenames
        @hidden_jobs = 0
        codenames = Gpu.codenames
        threads = []
        @bug.timelines.each do |timeline|
            threads << Thread.new {
                frame_outputs = Hash.new { |h, k| h[k] = Hash.new(&h.default_proc) }
                drivers = Hash.new { |h, k| h[k] = Hash.new(&h.default_proc) }
                gpu_repro = Hash.new { |h, k| h[k] = Hash.new(&h.default_proc) }
                gpu_codenames = Hash.new { |h, k| h[k] = Hash.new{|h, k| h[k] = Set.new()} }
                #jobs = @bug.jobs.includes(:bugs, :timeline).where(is_released_code: true, timeline: timeline).order(id: :desc)
                jobs = Job.includes(:bugs, :timeline).
                where(bugs: {id: @bug.id}, is_released_code: true, timeline: timeline).
                distinct.order(id: :desc)
                # raise if timeline.id == 443
                traces = @bug.trace_execs.includes(:timeline, :trace).where(timeline: {id: timeline.id}).pluck(:trace_id)
                columns = SortedSet.new()
                next if jobs.size == 0
                gpus = @bug.gpus
                # gpus = FrameOutput.includes(:gpu, :timeline, :bugs).where(timeline: {id: timeline.id}, bugs: {id: @bug.id}).map {|f| f.gpu}.uniq
                # before and after are from a timeline point of view
                job_after_bug = Job.includes(:trace_frames, :timeline).where("jobs.id > ?", jobs.first.id).
                where(timeline: timeline, trace_frames: {id: @bug.trace_frames.pluck(:id)}).order(id: :asc).first
                # job_before_bug = Job.includes(:trace_frames, :timeline).where("jobs.id < ?", jobs.last.id).
                # where(timeline: timeline, trace_frames: {id: @bug.trace_frames.pluck(:id)}).order(id: :desc).last
                job_before_bug = Job.includes(:trace_frames, :timeline, :gpus, :trace_execs).where("jobs.id < ?", jobs.last.id).
                where(gpus: gpus, timeline: timeline, trace_frames: {id: @bug.trace_frames.pluck(:id)}).
                where(trace_execs: {status: 0}).order(id: :desc).first

                jobs = jobs.to_a.push(job_before_bug) if job_before_bug != nil
                jobs = jobs.to_a.unshift(job_after_bug) if job_after_bug != nil
                previous_job = Hash.new { |h, k| h[k] = Hash.new{|h, k| h[k] = Set.new()} }
                current_job = Hash.new { |h, k| h[k] = Hash.new{|h, k| h[k] = Set.new()} }
                timeline_bug = timelinebug.new(timeline, frame_outputs,
                    drivers, gpu_codenames, gpu_repro,
                    jobs, columns, traces, 0, current_job, previous_job)

                @timelines[timeline] = timeline_bug

                outputs = @bug.frame_outputs.to_a
                if job_before_bug
                    job_before_bug.frame_outputs.where(trace_frame_id: @bug.trace_frame_ids).each do |frame|
                        outputs << frame
                    end
                end

                execs = @bug.trace_execs.to_a
                if job_before_bug
                    job_before_bug.trace_execs.includes(:trace_frames).where(gpu_id: @bug.gpu_ids, trace_frames: {id: @bug.trace_frame_ids}).each do |_exec|
                        execs << _exec
                    end
                end

                TraceExec.where(id: execs.pluck(:id)).includes(:driver_version, :timeline, :gpu).where(timeline: {id: timeline.id}).distinct.group_by(&:gpu).each do |gpu, trace_execs|
                    trace_execs.each do |exec_|
                        gpu_codenames[codenames[gpu.id]][gpu.pciid].add(exec_.driver_version)
                    end
                end

                driver_versions = gpu_codenames.map {|x| x[1].map {|y| y[1]}}.first.first

                driver_versions.each do |driver_version|
                    drivers[driver_version.driver.name][driver_version.name] = nil
                end

                trace_frames = TraceFrame.includes(:bugs, :timelines).where(bugs: {id: @bug.id}, timelines: {id: timeline.id})
                FrameOutput.where(id: outputs.pluck(:id)).includes(:trace_exec, :trace_frame, :trace, :gpu, :job, :bugs, :timeline, :driver_version, :deduped_frame_output).
                where(trace_frame: trace_frames, job: {id: jobs.map {|job| job.id }, timeline: timeline},
                        gpu: {id: gpus.pluck(:id)}, driver_version: {id: driver_versions.map(&:id)}).
                each do |frame|
                    columns << ExecColumn.new(codenames[frame.gpu.id], frame.gpu, frame.driver_version)
                    if frame.job == job_before_bug || frame.job == job_after_bug
                        frame.ignore_for_repro = true
                    end

                    frame_outputs[frame.timeline][frame.trace][frame.trace_frame][frame.gpu][frame.driver_version][frame.job] = frame
                end
            }
            threads.map(&:join) if threads.count > 3
        end
        threads.map(&:join)
        #"For the whole bug: # of jobs affected / # of job executions since the oldest job reproducing the bug in the timelines reproducing the bug "
    end

    def line_chart_amount
    end

    def associate
        @bug = Bug.find(params[:id])
        @frame = DedupedFrameOutput.find(params[:dedup_id])
        @frame.bugs << @bug
        @frame.update(is_acceptable: false, user: current_user)
        redirect_to next_frame_deduped_frame_output_path(@frame, job: params[:job])
    end

    def mark_fixed
        @bug = Bug.find(params[:id])
        @bug.update(fixed_on: Date.today)
        redirect_to bug_path(@bug)
    end

    def re_open
        @bug = Bug.find(params[:id])
        @bug.update(fixed_on: nil)
        redirect_to bugs_path
    end

    def update
        @bug = Bug.find(params[:id])
        @bug.update(bug_params)
        respond_to do |f|
            f.js
        end
    end

    def new
        @bug = Bug.new
        @frame = DedupedFrameOutput.find(params[:frame])
        @bug.deduped_frame_outputs << @frame
        @timeline = JobTimeLine.find(params[:timeline])
        respond_to do |format|
            format.js
        end
    end

    def create_upstream_issue
        @bug = Bug.find(params[:id])
        @frame = @bug.deduped_frame_outputs.last
        @timeline = JobTimeLine.find(params[:timeline])
        @url = @timeline.url_pattern_for_filing_issue % {title: @frame.issue_title(@bug), description: @frame.issue_body(@bug)}
        respond_to do |f|
            f.js
        end
    end

    def destroy
        @bug = Bug.find(params[:id])
        @bug.destroy
        redirect_to bugs_path
    end

    private
    def bug_params
        params.require(:bug).permit(:issue_url, :title, :description, :confirmation)
    end

end
