class BenchesController < ApplicationController
    def index
        @q = Bench.ransack(params[:q])
        @benchmarks = @q.result
    end

    def new
        @benchmark = Bench.new
    end

    def create
        @benchmark = Bench.new(bench_params)
        respond_to do |format|
            format.html { redirect_to @benchmark, notice: 'Benchmark was successfully created.' }
            format.json { render :edit, status: :created, location: @benchmark }
            format.js
        end
    end
    
    def edit
        @benchmark = Bench.find(params[:id])
    end
    
    def show
        @benchmark = Bench.find(params[:id])
        @run = Run.new
    end

    def update
        @benchmark = Bench.find(params[:id])
        if @benchmark.update(bench_params)
            respond_to do |format|
                format.html { redirect_to bench_path(@benchmark), flash: {success: 'Successfully updated benchmark'} }
            end
        end
    end

    def destroy
        @benchmark = Bench.find(params[:id])
        @benchmark.destroy
        redirect_to root_path
    end

    private
    # Never trust parameters from the scary internet, only allow the white list through.
    def bench_params
      params.require(:bench).permit(:name, :type_name)
    end
end
