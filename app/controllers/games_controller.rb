class GamesController < ApplicationController
    before_action :can_create_game, only: [:create, :new], except: [:show, :index]
    before_action :can_destroy_game, only: [:destroy], except: [:show, :index, :new]
    before_action :can_edit_game, only: [:edit, :update], except: [:show, :index, :new]
    before_action :authenticate_user!, except: [:show, :index]
    before_action :admin, except: [:show, :index, :destroy, :create, :edit, :update, :new]

    def index
        @q = Game.ransack(params[:q])
        @games = @q.result(distinct: true)
        @tags = Tag.order(name: :asc).uniq
    end

    def new
        @game = Game.new
    end

    def create
        @game = Game.new(game_params)
        respond_to do |format|
            if @game.save
                if params[:add_tag] != ""
                    @tag = Tag.find_or_create_by(name: params[:add_tag])
                    GamesTag.create(game_id: @game.id, tag_id: @tag.id) unless GamesTag.where(game_id: @game.id, tag_id: @tag.id).any?
                end
                format.html { redirect_to @game, notice: 'game was successfully created.' }
                format.json { render :edit, status: :created, location: @game }
                format.js
            else
                format.html {render json: @game.errors}
            end
        end
    end
    
    def edit
        @game = Game.find(params[:id])
    end
    
    def show
        @game = Game.find(params[:id])
        @trace = Trace.new
        @trace.trace_frames.build
        @traces = @game.traces.where(deleted: [nil, false])
        @gpu = Gpu.new
    end

    def update
        @game = Game.find(params[:id])
        if params[:add_tag] != ""
            @tag = Tag.find_or_create_by(name: params[:add_tag])
            GamesTag.create(game_id: @game.id, tag_id: @tag.id) unless GamesTag.where(game_id: @game.id, tag_id: @tag.id).any?
        end
        redirect_to game_path(@game)
    end

    def destroy
        @game = Game.find(params[:id])
        @game.destroy
        redirect_to games_path
    end

    private
    # Never trust parameters from the scary internet, only allow the white list through.
    def game_params
      params.require(:game).permit(:name, :appid, :trace_status, tags: [], :traces => [])
    end
end
