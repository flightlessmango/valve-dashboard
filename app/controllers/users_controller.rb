class UsersController < ApplicationController
    before_action :admin, only: [:index]

    def index
        @users = User.all
    end

    def edit
        @user = User.find(params[:id])
        if !current_user.admin?
            render html: "Not permitted", status: 401 if current_user != @user
        end
    end

    def edit_permissions
        if !current_user.admin?
            render html: "Not permitted", status: 401 if current_user != User.find(params[:id])
        end
        @user = User.find(params[:id])
    end

    def show
        if !current_user.admin?
            render html: "Not permitted", status: 401 if current_user != User.find(params[:id])
        end
        @user = User.find(params[:id])
    end

    def update
        @user = User.find(params[:id])
         if current_user == @user || current_user.admin?
            if @user.update(user_params)
                if current_user.admin?
                    redirect_to users_path 
                else
                    redirect_to root_path
                end
            else
                render json: @user.errors
            end
        end
    end

    def profile
        @user = current_user
    end

    def user_params
        if current_user.admin?
            params.require(:user).permit(:password, :username, :reset_password_token,
                                         :password_confirmation, :admin, :can_upload_trace,
                                         :can_download_trace, :can_create_game, :can_destroy_game,
                                         :can_edit_game, :can_review_frames, :can_upload_frames)
        else
            params.require(:user).permit(:password, :username, :reset_password_token, :password_confirmation)
        end
    end

end
