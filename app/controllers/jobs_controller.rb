class JobsController < ApplicationController
    before_action :can_review_frames, except: [:show, :index, :destroy]
    before_action :admin, only: [:destroy]
    before_action :authenticate_user!, except: [:show, :index]

    class ExecTimeStats < Struct.new(:min, :max, :avg, :sample_count)
        def interval
            return self.max - self.min
        end

        def sample_difference_percentage(execution_time)
            (((execution_time / self.avg) * 100.0) - 100).to_f
        end

        def is_sample_within_variance(exec_time)
            safety_margin = ((self.interval / 2) * 1.2)
            return (exec_time >= (self.min - safety_margin)) && (exec_time <= (self.max + safety_margin))
        end

        def to_s
            "#{self.avg.round(1)} ±#{(self.interval / 2).round(1)}"
        end
    end

    def index
        @q = Job.ransack(params[:q])
        @jobs = @q.result.order(id: :desc).page(params[:page]).per(10)
    end

    def show
        @job = Job.find(params[:id])
        @codenames = Hash.new { |h, k| h[k] = Hash.new(&h.default_proc) }
        @execs = Hash.new { |h, k| h[k] = Hash.new(&h.default_proc) }
        @filenames = Hash.new { |h, k| h[k] = Hash.new(&h.default_proc) }
        @trace_exec_history = Hash.new { |h, k| h[k] = Hash.new(&h.default_proc) }
        @frame_outputs = @job.frame_outputs
        @codenames = Gpu.codenames
        
        threads = []

        threads << Thread.new {
            @dedup_released = Set.new(@frame_outputs.includes(:job).where(job: {is_released_code: true}).pluck(:deduped_frame_output_id))
            @job.blobs.includes(:attachments).pluck('active_storage_attachments.record_id', :filename).each do |blob|
                @filenames[blob[0]] = blob[1]
            end
        }

        threads << Thread.new {
            @frames = @frame_outputs.includes(:trace_exec, :trace_frame, :trace, :gpu,
            :deduped_frame_output, :bugs, trace_exec: [driver_version: [driver_branch: [:driver]]],
            trace: :upload_blob).where(deduped_frame_output: {is_acceptable: [nil, false]}).
            select(:trace_exec_id, :trace_frame_id, :deduped_frame_output_id)
        }

        @trace_execs = @job.trace_execs
        threads << Thread.new {
            prev_job_created_at = Job.where("id < ?", @job.id).last.created_at
            last_jobs_in_timeline = Job.where(timeline: @job.timeline, is_released_code: true, created_at: Date.new..prev_job_created_at).last(20).pluck(:id)
            TraceExec.includes(:job, :gpu, :trace).where(gpu: @job.gpus, trace: @job.traces, status: 0, job: {id: last_jobs_in_timeline}).
                where('execution_time > ?', 0).where.not(id: @job.trace_execs.pluck(:id)).order(id: :desc).pluck(:trace_id, :gpu_id, :driver_version_id, :execution_time).each do |exec_|
                    trace_id = exec_[0]
                    gpu_id = exec_[1]
                    driver_version_id = exec_[2]
                    execution_time = exec_[3]
                    if @trace_exec_history[trace_id][gpu_id][driver_version_id].size == 0
                        @trace_exec_history[trace_id][gpu_id][driver_version_id] = [execution_time]
                    elsif @trace_exec_history[trace_id][gpu_id][driver_version_id].size < 10
                        @trace_exec_history[trace_id][gpu_id][driver_version_id].append(execution_time)
                    end
                end
                
                @trace_exec_history.each do |trace_id, exec_history|
                    exec_history.each do |gpu_id, exec_history|
                        exec_history.each do |driver_version_id, exec_history|
                            @trace_exec_history[trace_id][gpu_id][driver_version_id] = ExecTimeStats.new(exec_history.min, exec_history.max, exec_history.sum / exec_history.size, exec_history.size)
                        end
                    end
                end
        }
        threads << Thread.new {
            @columns = SortedSet.new()
            @unknown = GraphicsApi.new(id: nil, name: "Unknown")
            @trace_execs.includes(:driver, :graphics_api, :trace, :gpu, :trace, driver_version: [driver_branch: [:driver]]).each do |exec_|
                @columns << ExecColumn.new(@codenames[exec_.gpu_id], exec_.gpu, exec_.driver_version)
                if exec_.graphics_api == nil
                    @execs[@unknown][exec_.trace_id][exec_.gpu_id][exec_.driver_version][exec_] = nil
                else
                    @execs[exec_.graphics_api][exec_.trace_id][exec_.gpu_id][exec_.driver_version][exec_] = nil
                end
            end
        }

        threads.each(&:join)
    end

    def destroy
        @job = Job.find(params[:id])
        @job.destroy
        DedupedFrameOutput.left_joins(:frame_outputs).where(frame_outputs: {id: nil}).destroy_all
        redirect_to jobs_path
    end

    # private
    # def job_params
    #     params.require(:job).permit(:gpu_id, :user_id, metadata: {})
    # end

end