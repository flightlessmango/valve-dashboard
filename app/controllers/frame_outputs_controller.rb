class FrameOutputsController < ApplicationController

    def history
        @frame = FrameOutput.find(params[:id])
        @history = @frame.history
    end

end
