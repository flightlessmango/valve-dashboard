class ProvidersController < ApplicationController
    before_action :admin, only: []
    def destroy
        @provider = Provider.find(params[:id])
        @provider.destroy
        redirect_back(fallback_location: "/")
    end

    private
    def provider_params
      params.require(:provider).permit(:user_id, :uid, :name, :token, :refresh_token, :expires_at)
    end
end
