module Api
    module V1
        class BenchesController < ApplicationController
            skip_forgery_protection

            def index
                @q = Bench.ransack(params[:q])
                @benchmarks = @q.result
                respond_to do |f|
                    f.json
                end
            end

            def new
                @benchmark = Bench.new
            end

            def create
                @benchmark = Bench.new(bench_params)
                if @benchmark.save
                    render json: @benchmark
                end
            end
            
            def edit
                @benchmark = Bench.find(params[:id])
            end
            
            def show
                @benchmark = Bench.find(params[:id])
                @run = Run.new
            end

            def update
                @benchmark = Bench.find(params[:id])
                if @benchmark.update(bench_params)
                    redirect_to bench_path(@benchmark), flash: {success: 'Successfully updated benchmark'}
                end
            end

            def destroy
                @benchmark = Bench.find(params[:id])
                @benchmark.destroy
                redirect_to root_path
            end

            private
            # Never trust parameters from the scary internet, only allow the white list through.
            def bench_params
            params.require(:bench).permit(:name, :type_name)
            end
        end
    end
end