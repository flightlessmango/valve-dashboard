module Api
    module V1
        class GpusController < ApplicationController
            skip_forgery_protection

            def index
                @gpus = Gpu.valid_gpus
                respond_to do |f|
                    f.json
                end
            end

            def create
                if current_user
                    @gpu = Gpu.new(gpu_params)
                    if Gpu.find_by(pciid: @gpu.pciid)
                        @gpu = Gpu.find_by(pciid: @gpu.pciid)
                    end
                    @gpu.save
                    render json: @gpu
                end
            end
            
            private
            def gpu_params
                params.require(:gpu).permit(:name, :gpuz, :pciid, metadata: {})
            end 

        end
    end
end