module Api
    module V1
        class GamesController < ApplicationController
            skip_forgery_protection
            before_action :can_create_game, only: [:create]
            before_action :admin, except: [:create, :index]
            before_action :authenticate_user!, except: [:index]

            def index
                @games = Game.all.includes(:traces)
            end

            def create
                @game = Game.new(game_params)
                @game = Game.create(game_params)
                if @game.errors.any?
                    render json: @game.errors
                else
                    render json: @game
                end
            end

            private
            def game_params
                params.require(:game).permit(:name, :appid)
            end

        end
    end
end