module Api
    module V1
        class TraceExecsController < ApplicationController
            skip_forgery_protection
            before_action :admin, except: [:create_single, :create, :show, :update]
            before_action :can_upload_frames, only: [:create_single, :create, :update]

            def index
                respond_to do |f|
                @q = TraceExec.ransack(params[:q])
                @traceExecs = @q.result.includes(:gpu, :trace, :job, :trace_frames, :deduped_frame_outputs, trace_frames: :frame_outputs, frame_outputs: :deduped_frame_output)
                    f.json
                end
            end

            def create_single
                @traceExec = TraceExec.new(trace_exec_params.except(:exec_log))
                @pciid = ""
                if params[:pciid]
                    @pciid = params[:pciid]
                else
                    @pciid = @traceExec.metadata["machine_tags"].map { |s| s.gsub("amdgpu:pciid:", "") if s.include?("amdgpu:pciid:")}.compact[0] if @traceExec.metadata.to_s.include?("amdgpu:pciid:")
                end

                if @pciid == nil
                    @gpu = Gpu.where(pciid: nil).first
                else
                    @gpu = Gpu.find_or_create_by(pciid: @pciid) if @pciid
                end

                if params[:driver]
                    @driver = Driver.find_or_create_by(name: params[:driver][:name])
                    @driver_branch = DriverBranch.find_or_create_by(driver: @driver, name: params[:driver][:branch])
                    @driver_version = DriverVersion.find_or_create_by(driver_branch: @driver_branch, name: params[:driver][:version])
                    @traceExec.driver_version = @driver_version
                end
                @traceExec.gpu_id = @gpu.id
                @trace_exists = TraceExec.where(trace_id: @traceExec.trace_id, job_id: @traceExec.job_id, gpu_id: @traceExec.gpu_id, driver_version_id: @traceExec.driver_version_id).last
                if @trace_exists
                    render json: {"status": "already exists, id: #{@trace_exists.id}"}, status: 409
                else
                @traceExec.save
                if @traceExec.errors.any?
                    render json: @traceExec.errors
                else
                    blob = ActiveStorage::Blob.find_signed(params[:trace_exec][:exec_log])
                    attachments = []
                    attachments << ActiveStorage::Attachment.new(name: "exec_log", blob: blob, record: @traceExec, record_type: "TraceExec")
                    params[:frame_blobs].each do |key,val|
                        blob = ActiveStorage::Blob.find_signed(val)
                        @trace_frame = TraceFrame.find_or_create_by(trace: @traceExec.trace, frame_id: key.to_i)
                        @frame_output = FrameOutput.new(trace_id: @traceExec.trace_id, trace_exec_id: @traceExec.id,
                                                        user: current_user, trace_frame_id: @trace_frame.id)
                        @dedup = DedupedFrameOutput.find_or_create_by(blob_id: blob.id, trace_frame_id: @frame_output.trace_frame.id)
                        @frame_output.deduped_frame_output_id = @dedup.id
                        @frame_output.save!
                        attachments << ActiveStorage::Attachment.new(name: "upload", blob: blob, record: @frame_output, record_type: "FrameOutput")
                    end
                    ActiveStorage::Attachment.import attachments
                end
                end
            end

            def create
                create_single
                respond_to do |f|
                    f.json
                end
            end
            
            def update
                @traceExec = TraceExec.find(params[:id])
                @traceExec.update(trace_exec_params)
                render json: @traceExec
            end

            def show
                @traceExec = TraceExec.find(params[:id])
                respond_to do |f|
                    f.json
                end
            end

            private
            def trace_exec_params
                params.require(:trace_exec).permit(:job_id, :trace_id, :execution_time, :status, :exec_log, :expected_stable, metadata: {})
            end

        end
    end
end