module Api
    module V1
        class StaticController < ApplicationController
            skip_forgery_protection

            def checksum
                @blob = ActiveStorage::Blob.where(checksum: params[:checksum]).first
                if @blob
                    blob_json = JSON.parse(@blob.to_json)
                    blob_json[:signed_id] = @blob.signed_id
                    blob_json[:record] = @blob.attachments.first.record if @blob.attachments.any?
                    # blob_json[:record_type] = @output.upload.attachments.first.record_type
                    render json: blob_json, status: 200
                else
                    hash = {}
                    hash[:accepted] = "File is unique"
                    render json: hash, status: 200
                end
            end

            def image_checksum
                @output = ActiveStorage::Blob.find_by(image_checksum: params[:checksum])
                if @output
                    output_json = JSON.parse(@output.to_json)
                    output_json[:signed_id] = @output.signed_id
                    render json: output_json, status: 200
                else
                    hash = {}
                    hash[:accepted] = "File is unique"
                    render json: hash, status: 200
                end
            end

            def unoptimized_traces
                # @trace = Trace.left_joins(:optimized_attachment).where(active_storage_attachments: { id: nil }).first
                # if @trace.nil?
                #     render json: nil
                # else
                #     render "api/v1/traces/show.json.jbuilder"
                # end
            end

            def trace_lock
                # @trace = Trace.find(params[:id])
                # hash = {}
                # if @trace.locked?
                #     hash[:file] = "Locked"
                #     render json: hash, status: 200
                # else
                #     hash[:file] = "Locking"
                #     @trace.update(optimize_lock: true)
                #     render json: hash, status: 200
                # end
            end

        end
    end
end