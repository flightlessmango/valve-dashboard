module Api
    module V1
        class GraphicsApisController < ApplicationController
            skip_forgery_protection

            def index
                @apis = GraphicsApi.all
                respond_to do |f|
                    f.json
                end
            end

            def create
                @api = GraphicsApi.new(graphics_api_params)
                if GraphicsApi.find_by(name: @api.name)
                    @api = GraphicsApi.find_by(name: @api.name)
                else
                    @api.save
                end
                render json: @api
            end

            private
            def graphics_api_params
                params.require(:graphics_api).permit(:name, :version)
            end

        end
    end
end