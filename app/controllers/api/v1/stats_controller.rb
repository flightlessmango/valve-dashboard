module Api
    module V1
        class StatsController < ApplicationController
            before_action :authenticate_user!, except: [:blobs]
            before_action :admin, except: [:blobs]
            def frame_outputs
                @q = FrameOutput.ransack(params[:q])
                @frame_outputs = @q.result.includes(trace_exec: :gpu).includes(:deduped_frame_output, :trace_frame)
                @deduped_frames = DedupedFrameOutput.where(id: @frame_outputs.pluck(:deduped_frame_output_id))
                @trace_frames = TraceFrame.where(id: @frame_outputs.pluck(:trace_frame_id).uniq)
                @accepted = []
                @unaccepted = []
                @needs_review = []
                @deduped_frames.each do |frame|
                    if frame.is_acceptable == true
                        @accepted << frame
                    end
                    if frame.is_acceptable == false
                        @unaccepted << frame
                    end
                    if frame.is_acceptable == nil
                        @needs_review << frame
                    end
                end
                respond_to do |f|
                    f.json
                end
            end

            def trace_frames
                @trace_frame = TraceFrame.find(params[:id])
                @q = DedupedFrameOutput.ransack(params[:q])
                @deduped = @q.result.order(:created_at).where(trace_frame: @trace_frame).includes(:gpus, :frame_outputs, :blob, :jobs, :trace_execs, trace_execs: [:job], frame_outputs: [trace_exec: [:gpu]]).to_a.uniq
                respond_to do |f|
                    f.json
                end
            end

            def blobs
                @blob = ActiveStorage::Blob.find(params[:id])
                if @blob.attachments.pluck(:record_type).include? "FrameOutput"
                    respond_to do |f|
                        if Rails.env.development?
                            f.json {render json: {url: "/old.png"}}
                        else
                            f.json
                        end
                    end
                end
            end
        end
    end
end