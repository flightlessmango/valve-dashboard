module Api
    module V1
        class CommitsController < ApplicationController
            skip_forgery_protection
    
            def index
                @commits = Commit.all
                respond_to do |f|
                    f.json
                end
            end

            def create
                @commit = Commit.new(commit_params)
                @commit.save
                if @commit.errors.any?
                    render json: @commit.errors
                else
                    render json: @commit
                end
            end

            private
            def commit_params
                params.require(:commit).permit(:project_id, :version)
            end

        end
    end
end