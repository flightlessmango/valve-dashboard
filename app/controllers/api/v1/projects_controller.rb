module Api
    module V1
        class ProjectsController < ApplicationController
            skip_forgery_protection

            def index
                @projects = Project.all
                respond_to do |f|
                    f.json
                end
            end

            def create
                @project = Project.new(project_params)
                @project.save
                if @project.errors.any?
                    render json: @project.errors
                else
                    render json: @project
                end
            end

            private
            def project_params
                params.require(:project).permit(:name, :repo_url, :project_url, :base_url_for_commits)
            end

        end
    end
end