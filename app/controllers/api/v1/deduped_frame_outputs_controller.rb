module Api
    module V1
        class DedupedFrameOutputsController < ApplicationController
            before_action :authenticate_user!, except: [:show]
            before_action :admin, except: [:show]

            def show
                @dedup = DedupedFrameOutput.find(params[:id])
                respond_to do |f|
                    f.json
                end
            end

        end
    end
end