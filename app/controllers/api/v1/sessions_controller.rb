# frozen_string_literal: true
module Api
  module V1
    class SessionsController < DeviseController
        skip_forgery_protection
        prepend_before_action :require_no_authentication, only: [:new, :create]
        prepend_before_action :allow_params_authentication!, only: :create
        prepend_before_action :verify_signed_out_user, only: :destroy
        prepend_before_action(only: [:create, :destroy]) { request.env["devise.skip_timeout"] = true }
        before_action :admin, except: [:create]
        # POST /resource/sign_in
        def create
          self.resource = warden.authenticate!(auth_options)
          set_flash_message!(:notice, :signed_in)
          sign_in(resource_name, resource)
        end

        protected

        def auth_options
          { scope: resource_name, recall: "#{controller_path}#new" }
        end

    end
  end
end
