module Api
    module V1
        class JobsController < ApplicationController
            skip_forgery_protection
            before_action :authenticate_user!, except: [:index]
            before_action :admin, except: [:index]

            def index
                @jobs = Job.all
                if params[:name]
                    render json: Job.find_by(name: params[:name])
                else
                    respond_to do |f|
                        f.json
                    end
                end
            end

            def create
                @job = Job.new(job_params)
                @job.user = current_user
                @job_existing = Job.find_by(name: @job.name)
                if @job_existing
                    render json: @job_existing
                else
                # assign timeline
                    if params[:job_timeline]
                        @timeline = JobTimeLine.find_or_create_by(project: params[:job_timeline][:project],
                                                                branch: params[:job_timeline][:branch])
                        @timeline.update(project_url: params[:job_timeline][:project_url],
                                        base_url_for_commits: params[:job_timeline][:base_url_for_commits])
                        @job.timeline_id = @timeline.id
                    end
                    @job.save
                    if @job.errors.any?
                        render json: @job.errors
                    else
                        render json: @job
                    end
                end
            end

            private
            def job_params
                params.require(:job).permit(:gpu_id, :user_id, :name, :is_released_code, :commit_version, metadata: {})
            end

        end
    end
end
