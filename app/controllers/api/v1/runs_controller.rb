module Api
    module V1
        class RunsController < ApplicationController
            skip_forgery_protection

            def index
                @runs = Run.all
            end

            def create
                @run = Run.new(run_params)
                if @run.save!
                    if @run.upload.attached?
                        @run.parse_upload
                    end
                    render json: @run
                end
            end


            private
            def run_params
                params.require(:run).permit(:game_id, :bench_id, :upload, :name)
            end

        end
    end
end
