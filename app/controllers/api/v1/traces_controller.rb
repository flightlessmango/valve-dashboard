module Api
    module V1
        class TracesController < ApplicationController
            skip_forgery_protection
            before_action :can_download_trace, except: [:index, :show, :create, :update, :enrollment]
            before_action :can_upload_trace, only: [:create, :update], except: [:enrollment]
            before_action :authenticate_user!, except: [:show, :index]
            before_action :admin, only: [:enrollment]

            def index
                @traces = Trace.all.includes([:trace_frames, :graphics_api, :game, upload_attachment: :blob]).where(deleted: [nil, false])
                @pciid = params[:target_gpu_pciid]
                @gpu = Gpu.find_by(pciid: @pciid)
                @game = Game.where(appid: params[:steamid]).last if params[:steamid]
            end

            def create
                if current_user
                    @trace = Trace.new(trace_params)
                    @trace.user = current_user
                    if @trace.save
                        @trace.frames_to_capture.each do |i|
                            TraceFrame.create(frame_id: i, trace_id: @trace.id)
                        end
                    end
                    if @trace.errors.any?
                        render json: @trace.errors
                    else
                        render json: @trace
                    end
                end
            end

            def show
                @trace = Trace.find(params[:id])
                respond_to do |f|
                    f.json
                end
            end

            def update
                @trace = Trace.find(params[:id])
                @trace.update(trace_params)
                if @trace.errors.any?
                    render json: @trace.errors
                else
                    render json: @trace
                end
            end

            def enrollment
                @traces = Trace.where(graphics_api_id: nil)
                respond_to do |f|
                    f.json
                end
            end

            private
            def trace_params
                params.require(:trace).permit(:upload, :optimized, :gpuz, :game_id, :appid, :gpu_id, :optimize_lock, :graphics_api_id, metadata: {}, frames_to_capture: [])
            end

        end
    end
end
