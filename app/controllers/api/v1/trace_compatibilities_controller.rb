module Api
    module V1
        class TraceCompatibilitiesController < ApplicationController
            skip_forgery_protection

            def index
                @compats = TraceCompatibility.all.includes(:commit, commit: :project)
                if params[:pciid]
                    @gpu = Gpu.find_by(pciid: params[:pciid])
                    @compats = TraceCompatibility.where(gpu: @gpu)
                end
                respond_to do |f|
                    f.json
                end
            end

            def create
                @compat = TraceCompatibility.find_by(trace_compatibility_params)
                if @compat
                    TraceCompatibility.transaction do
                        @compat.lock!
                        @compat.agreement_count = @compat.agreement_count + 1
                        @compat.save
                    end
                else
                    @compat = TraceCompatibility.new(trace_compatibility_params)
                    @compat.agreement_count = 1
                    @compat.save
                end
                if @compat.errors.any?
                    render json: @compat.errors
                else
                    render json: @compat
                end
            end

            private
            def trace_compatibility_params
                params.require(:trace_compatibility).permit(:commit_id, :trace_id, :gpu_id, :is_working, :bug_id)
            end

        end
    end
end