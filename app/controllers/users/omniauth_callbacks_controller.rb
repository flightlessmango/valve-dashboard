class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController
  before_action :authenticate_user!, only: []
  before_action :admin, only: []
  skip_forgery_protection

  User::Providers.map(&:to_s).each do |meth|
    define_method(meth) do
      auth = request.env["omniauth.auth"]
      if current_user
        User.from_omniauth_logged_on(current_user, auth)
        redirect_to(profile_path(current_user))
      else
        @user = User.from_omniauth(auth)
        if @user
            sign_in_and_redirect @user, event: :authentication
        end
      end
    end
  end

  def failure
  end

end