class StaticController < ApplicationController
    skip_forgery_protection if Rails.env == "test"
    before_action :authenticate_user!, except: [:frames, :index, :get_ref_frames, :get_url]
    before_action :admin, only: [:stats]

    def index
        @colors = Bench::TWENTY
        @traces = Trace.all.order(created_at: :desc).limit(3)
        @bugs = Bug.all.order(created_at: :desc).limit(3)
        @filenames = Trace.filenames
    end

    def frames
        # Preload the mapping between a {gpu, driver, timeline}_id, and a display
        # string
        codenames = Gpu.codenames
        drivers = DriverVersion.all_drivers
        timelines = JobTimeLine.timelines

        last_dedup_of = Struct.new(:gpu_codename, :driver_name, :timeline_name)
        # Only show these specific ids if this param
        if params[:ids]
            @all = DedupedFrameOutput.where(id: params[:ids].split(","))
            @paginated = @all.includes(:game).page params[:page]
            paginated_dedups_h = @paginated.map {|d| [d.id, d]}.to_h
            @last_instances_for_dedups = Hash.new { |h, k| h[k] = Array.new() }
            @all.each do |dedup|
                if paginated_dedups_h.include? dedup.id
                    dedup = paginated_dedups_h[dedup.id]
                    @last_instances_for_dedups[dedup].append(
                        last_dedup_of.new(codenames[dedup.gpus.last.id],
                                        drivers[dedup.driver_versions.last.id],
                                        timelines[dedup.timelines.last.id]))
                end
            end
        else
            # The goal of this view is to prioritize reviewing the latest frames generated
            # for any GPU/Driver/Timeline combination rather than having to review every
            # single unique frame ever generated. It is however beneficial to know the
            # setting in which this frame was last seen, hence the complexity of this
            # query...

            # This query returns the list of deduped frame outputs that were last seen
            # for every GPU, driver version, timeline, and trace frame id, ordered from
            # oldest deduped frame output to newest. This is considered to be the list of
            # frames that need review, but they may contain duplicate deduped frame
            # outputs. We limit the results for performance and memory usage.
            limit = params.fetch('limit', '250').to_i

            last_dedup_seen_on = DedupedFrameOutput.needing_review(limit: limit, timeline_pk: params[:timeline_pk], trace_frame_pk: params[:trace_frame_pk],
                                                                driver_version_pk: params[:driver_version_pk], gpu_pk: params[:gpu_pk], trace_pk: params[:trace_pk])

            @is_all_an_understatement = last_dedup_seen_on.to_a.size == limit

            # This query will identify the list of unique deduped frame outputs found
            # in the list of frames needing review
            @all = DedupedFrameOutput.where(id: last_dedup_seen_on.map {|d| d.deduped_frame_output_id}).order(id: :asc)

            # This query will paginate the @all query, leading to performance
            # improvements. The result is also stored in a Hash form, with the key
            # being the deduped frame output ID and the value being the deduped frame
            # output
            @paginated = @all.includes(:game).page params[:page]
            paginated_dedups_h = @paginated.map {|d| [d.id, d]}.to_h

            # This section will go through the list of frames that need review, and will
            # associate to the deduped frame outputs their list of conditions where they
            # were last seen on (GPU, timeline, driver).
            @last_instances_for_dedups = Hash.new { |h, k| h[k] = Array.new() }
            last_dedup_seen_on.each do |frame_output|
                if paginated_dedups_h.include? frame_output.deduped_frame_output_id
                    dedup = paginated_dedups_h[frame_output.deduped_frame_output_id]
                    @last_instances_for_dedups[dedup].append(
                        last_dedup_of.new(codenames[frame_output.gpu_id],
                                        drivers[frame_output.driver_version_id],
                                        timelines[frame_output.timeline_id]))
                end
            end
        end
    end

    def stats
        @traces = Trace.all
    end

    def get_url
        @dedup = DedupedFrameOutput.find(params[:dedup])
        respond_to do |f|
            f.js
            f.json
        end
    end

    def get_ref_frames
        @frame = DedupedFrameOutput.find(params[:q][:dedup_id])
        @q = DedupedFrameOutput.includes(:gpus, :blob, :jobs, :trace_execs).
             where(trace_frame_id: @frame.trace_frame.id, jobs: {is_released_code: true}).
             where("jobs.id <= ?", @frame.jobs.first.id).where.not(id: @frame.id).
             ransack(params[:q])
        @candidate_dedups = @q.result(distinct: true).limit(1000)
        thread = Thread.new {
            @timelines_box = JobTimeLine.where(id: @candidate_dedups.pluck('jobs.timeline_id'))
            @drivers_box = DriverVersion.where(id: @candidate_dedups.pluck('trace_execs.driver_version_id')).where.not(name: [nil, ""])
            @codenames_box = Gpu.where(id: @candidate_dedups.pluck('trace_execs.gpu_id')).pluck(:name).uniq
            @codenames = Gpu.codenames
            @candidate_dedups = @candidate_dedups.sort_by {|d| d.id }.reverse!
        }
        thread.join
        respond_to do |f|
            f.html {}
            f.js
        end
    end

end

# acceptable = []
# dedups = DedupedFrameOutput.needing_review.map {|f| f.deduped_frame_output}
# threads = []
# dedups.each do |d|
#     puts d.id
#     threads << Thread.new {
#         if d.can_be_accepted
#             acceptable.push(d)
#         end
#     }
#     if threads.size > 3
#         threads.each(&:join)
#         threads = []
#     end
# end

# ds = []
# DedupedFrameOutput.all.each do |d|
#     if d.trace_frame == nil
#         ds.push(d)
#     end
# end