class TraceExecsController < ApplicationController
    before_action :authenticate_user!, except: [:show]

    def show
        @traceExec = TraceExec.find(params[:id])
        @trace_frames = @traceExec.trace_frames
        @gpus = [] << @traceExec.gpu
        @job = @traceExec.job
    end
end
