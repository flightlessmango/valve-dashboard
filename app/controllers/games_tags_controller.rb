class GamesTagsController < ApplicationController
    before_action :can_edit_game, only: [:destroy]
    before_action :admin, except: [:destroy]

    def destroy
        @tag = GamesTag.find_by(tag_id: params[:tag_id], game_id: params[:game_id])
        @tag.destroy
        redirect_back(fallback_location: root_path)
    end

end