class TracesController < ApplicationController
    before_action :authenticate_user!, except: [:line_chart_amount, :show, :index, :execution_time]
    before_action :can_upload_trace, only: [:create, :update, :edit]
    before_action :admin, only: [:destroy], except: [:update, :edit]

    def create
        @trace = Trace.new(trace_params)
        hash = {}
        hash["user"] = params[:user_metadata]
        @trace.metadata = hash
        @trace.user_id = current_user.id
        respond_to do |format|
            if @trace.save
                @trace.update(frames_to_capture: @trace.trace_frames.order(:frame_id).pluck(:frame_id))
                format.html { redirect_to @trace.game, notice: 'Trace was successfully created.' }
                format.json { render :edit, status: :created, location: @trace }
                format.js
            else
                format.html { redirect_to traces_path }
                format.json { render json: @trace.errors, status: :unprocessable_entity }
                format.js
            end
        end
    end

    def index
        @q = Trace.where(deleted: [nil, false]).ransack(params[:q])
        @traces = @q.result(distinct: true).page(params[:page]).per(10)
        @filenames = Trace.filenames
    end

    def destroy
        @trace = Trace.find(params[:id])
        @trace.update(deleted: true)
        # @trace.destroy
        redirect_to @trace.game
    end

    def edit
        @trace = Trace.find(params[:id])
    end

    def update
        @trace = Trace.find(params[:id])
        if @trace.update(trace_params)
            @trace.frames_to_capture.each do |i|
                TraceFrame.find_or_create_by(trace: @trace, frame_id: i)
            end
            @trace.trace_frames.each do |frame|
                if params[:trace]["frame_id_#{frame.frame_id}"] == "true"
                    array = @trace.frames_to_capture
                    array.delete(frame.frame_id)
                    @trace.update(frames_to_capture: array)
                end
            end
            redirect_to traces_path
        else
            redirect_to edit_trace_path(@trace)
        end
    end

    def show
        @trace = Trace.find(params[:id])
        @trace_frames = @trace.trace_frames.order(:frame_id)
        @gpus = Job.last.gpus.map {|gpu| gpu if !gpu.pciid.empty? }.compact
        @codenames = Gpu.codenames
        @timelines = @trace.timelines
    end

    def line_chart_amount
    end

    def execution_time
        @trace = Trace.find(params[:id])
        @timeline = JobTimeLine.find(params[:timeline]) if params[:timeline]
        @codenames = Gpu.codenames
        if @timeline
            @data = @trace.trace_execs.includes(:gpu, :timeline).
            where(status: 0, timeline: {id: @timeline.id}).where.not(
            execution_time: [0, nil, 0.0],
            gpu: {name: "{}"}).order(:id).group_by(&:gpu_id)
        else
            @data = @trace.trace_execs.includes(:gpu).
            where(status: 0).where.not(
            execution_time: [0, nil, 0.0],
            gpu: {name: "{}"}).order(:id).group_by(&:gpu_id)
        end
        respond_to do |format|
            format.json
        end
    end

    def frames
        @trace = Trace.find(params[:id])
        @frame = TraceFrame.find_by(frame_id: params[:frame_id])
        if @frame
            @q = @frame.latest_generated_frames.ransack(params[:q])
            @frames = @q.result(distinct: true).includes(:trace_exec)
            @timelines = JobTimeLine.where(id: @frames.pluck('jobs.timeline_id')).uniq
            @gpus = Gpu.where(id: @frames.pluck('trace_execs.gpu_id')).uniq
            @driver_branches = DriverBranch.where(id: @frames.map {|f| f.driver_branch.id}).uniq
        else
            render file: "#{Rails.root}/public/404.html", status: :not_found, layout: false
        end
    end

    private
    def trace_params
        params.require(:trace).permit(:upload, :gpuz, :game_id, :appid,
                                      :gpu_id, metadata: {}, frames_to_capture: [],
                                      trace_frames_attributes: [:id, :trace_id, :frame_id, :upload])
    end

end
