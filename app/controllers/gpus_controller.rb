class GpusController < ApplicationController
    before_action :admin

    def create
        @gpu = Gpu.new(gpu_params)
        hash = {}
        hash["user"] = params[:user_metadata]
        @gpu.metadata = hash
        respond_to do |format|
            if @gpu.save
                format.html { redirect_back(fallback_location:"/") }
                format.json { render :edit, status: :created, location: @gpu }
                format.js
            else
                # format.html { redirect_back(fallback_location:"/") }
                # format.json { render json: @gpu.errors, status: :unprocessable_entity }
                # format.js
            end
        end
    end

    def destroy
        @gpu = Gpu.find(params[:id])
        @gpu.destroy
        redirect_to gpus_path
    end

    def index
        @gpus = Gpu.all.where.not(name: [nil, "No codename", "{}"])
    end

    def show
        @gpu = Gpu.find(params[:id])
        @q = @gpu.jobs.ransack(params[:q])
        @jobs = @q.result.order(id: :desc).page(params[:page]).per(10)
        # @unaccepted_frames = FrameOutput.where("metadata @> ?", {machine_tags: [params[:id]]}.to_json).where(is_looking_acceptable: nil)
    end

    # def execution_time
    #     @trace = Trace.find(params[:id])
    #     @timeline = JobTimeLine.find(params[:timeline]) if params[:timeline]
    #     if @timeline
    #         @data = @trace.trace_execs.includes(:gpu, :timelines).
    #         where(status: 0, timelines: {id: @timeline.id}).where.not(
    #         execution_time: [0, nil, 0.0],
    #         gpu: {name: "{}"}).order(:id).group_by(&:gpu_id)
    #     else
    #         @data = @trace.trace_execs.includes(:gpu).
    #         where(status: 0).where.not(
    #         execution_time: [0, nil, 0.0],
    #         gpu: {name: "{}"}).order(:id).group_by(&:gpu_id)
    #     end
    #     respond_to do |format|
    #         format.json
    #     end
    # end
    
    private
    def gpu_params
        params.require(:gpu).permit(:name, :gpuz, metadata: {})
    end

end
