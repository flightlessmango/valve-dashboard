class RunsController < ApplicationController

    def index
        @runs = Run.all
    end

    def create
        @run = Run.new(run_params)
        respond_to do |format|
            if @run.save
                if @run.upload.attached?
                    if params[:file_type] == "HML"
                        @run.parse_hml
                    end
                    if params[:file_type] == "MANGO"
                        @run.parse_upload
                    end
                end
                format.html { redirect_to @run.bench, notice: 'Run was successfully created.' }
                format.json { render json: @run.bench }
                format.js
            else
                format.html { render :new }
                format.json { render json: @run.bench, status: :unprocessable_entity }
                format.js
            end
        end
    end
    
    def show
        @run = Run.find(params[:id])
    end

    def update
        @run = Run.find(params[:id])
        if @run.update(run_params)
            respond_to do |format|
                if params[:attachment]
                    if params[:file_type] == "HML"
                        @run.parse_hml
                    end
                    if params[:file_type] == "MANGO"
                        @run.parse_upload
                    end
                end
                format.html { redirect_to bench_path(@run) }
                format.json { render json: @run }
            end
        end
    end

    def destroy
        @run = Run.find(params[:id])
        @run.destroy
        redirect_to @run.bench
    end

    private
    def run_params
      params.require(:run).permit(:game_id, :bench_id, :upload, :name)
    end

end
